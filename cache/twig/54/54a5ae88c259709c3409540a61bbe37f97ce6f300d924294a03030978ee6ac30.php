<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @dashboard/components/macros.twig */
class __TwigTemplate_08b43c65fdcae4d9793fb89f8296c3ca9b4bd3f0cebf8a6043edb07f208fa5b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 10
        echo "
";
    }

    // line 7
    public function macro_dashboardHeading($__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 8
            echo "    ";
            $this->loadTemplate("@dashboard/components/dashboardHeading.twig", "@dashboard/components/macros.twig", 8)->display($context);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 11
    public function macro_dashboardSymbol($__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 12
            echo "    ";
            $this->loadTemplate("@dashboard/components/dashboardSymbol.twig", "@dashboard/components/macros.twig", 12)->display($context);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@dashboard/components/macros.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 12,  68 => 11,  58 => 8,  45 => 7,  40 => 10,  37 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * All dashboard twig components available as macros.
 */
#}

{% macro dashboardHeading(params) %}
    {% include \"@dashboard/components/dashboardHeading.twig\" %}
{% endmacro dashboardHeading %}

{% macro dashboardSymbol(params) %}
    {% include \"@dashboard/components/dashboardSymbol.twig\" %}
{% endmacro dashboardSymbol %}
", "@dashboard/components/macros.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\applications\\dashboard\\views\\components\\macros.twig");
    }
}
