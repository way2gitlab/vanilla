<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /applications/dashboard/views/utility/raw.twig */
class __TwigTemplate_68abee83a75de492a3aba465344c1f933612d8df834ef330e08906b6d395ad02 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo " ";
        if ((isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })())) {
            // line 2
            echo "    ";
            // line 3
            echo "    ";
            echo (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 3, $this->source); })());
            echo "
 ";
        }
    }

    public function getTemplateName()
    {
        return "/applications/dashboard/views/utility/raw.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" {% if data %}
    {# Make sure the content is already sanitized.#}
    {{ data|raw }}
 {% endif %}
", "/applications/dashboard/views/utility/raw.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\applications\\dashboard\\views\\utility\\raw.twig");
    }
}
