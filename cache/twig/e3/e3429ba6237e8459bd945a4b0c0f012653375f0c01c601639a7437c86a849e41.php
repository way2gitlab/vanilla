<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* library/Vanilla/Web/Asset/InlinePolyfillContent.js.twig */
class __TwigTemplate_d3e3a3a529531ab1238081b91fb098f4288a621cc0eef592c5bb330e49761158 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "var supportsAllFeatures =
    window.Promise &&
    window.Promise.prototype.finally &&
    window.fetch &&
    window.Symbol &&
    window.CustomEvent &&
    Array.prototype.includes &&
    Element.prototype.remove &&
    Element.prototype.closest &&
    Element.prototype.attachShadow &&
    window.NodeList &&
    NodeList.prototype.forEach
;

if (!supportsAllFeatures) {
    ";
        // line 17
        echo (isset($context["debugModeLiteral"]) || array_key_exists("debugModeLiteral", $context) ? $context["debugModeLiteral"] : (function () { throw new RuntimeError('Variable "debugModeLiteral" does not exist.', 17, $this->source); })());
        echo " && console.log(\"Older browser detected. Initiating polyfills.\");
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = \"";
        // line 20
        echo twig_get_attribute($this->env, $this->source, (isset($context["polyfillAsset"]) || array_key_exists("polyfillAsset", $context) ? $context["polyfillAsset"] : (function () { throw new RuntimeError('Variable "polyfillAsset" does not exist.', 20, $this->source); })()), "getWebPath", [], "method", false, false, false, 20);
        echo "\";

    ";
        // line 27
        echo "    script.async = false;
    // document.write has to be used instead of append child for edge & old safari compatibility.
    document.write(script.outerHTML);
} else {
    ";
        // line 31
        echo (isset($context["debugModeLiteral"]) || array_key_exists("debugModeLiteral", $context) ? $context["debugModeLiteral"] : (function () { throw new RuntimeError('Variable "debugModeLiteral" does not exist.', 31, $this->source); })());
        echo " && console.log(\"Modern browser detected. No polyfills necessary\");
}

if (!window.onVanillaReady) {
    window.onVanillaReady = function (handler) {
        if (typeof handler !== \"function\") {
            console.error(\"Cannot register a vanilla ready handler that is not a function.\");
            return;
        }
        document.addEventListener(\"X-DOMContentReady\", function () {
            if (!window.__VANILLA_INTERNAL_IS_READY__) {
                return;
            }
            handler(window.__VANILLA_GLOBALS_DO_NOT_USE_DIRECTLY__);
        })

        if (window.__VANILLA_INTERNAL_IS_READY__) {
            handler(window.__VANILLA_GLOBALS_DO_NOT_USE_DIRECTLY__);
        }
    }
}";
    }

    public function getTemplateName()
    {
        return "library/Vanilla/Web/Asset/InlinePolyfillContent.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 31,  65 => 27,  60 => 20,  54 => 17,  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{%- autoescape false -%}
var supportsAllFeatures =
    window.Promise &&
    window.Promise.prototype.finally &&
    window.fetch &&
    window.Symbol &&
    window.CustomEvent &&
    Array.prototype.includes &&
    Element.prototype.remove &&
    Element.prototype.closest &&
    Element.prototype.attachShadow &&
    window.NodeList &&
    NodeList.prototype.forEach
;

if (!supportsAllFeatures) {
    {{ debugModeLiteral }} && console.log(\"Older browser detected. Initiating polyfills.\");
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = \"{{ polyfillAsset.getWebPath() }}\";

    {#
    // Without this script execution order is inconsistent.
    // IE11 does not seem to respect https://html.spec.whatwg.org/multipage/scripting.html#script-processing-src-sync
    // Which means we HAVE to set the element, even if it should be defaulted.
    #}
    script.async = false;
    // document.write has to be used instead of append child for edge & old safari compatibility.
    document.write(script.outerHTML);
} else {
    {{ debugModeLiteral }} && console.log(\"Modern browser detected. No polyfills necessary\");
}

if (!window.onVanillaReady) {
    window.onVanillaReady = function (handler) {
        if (typeof handler !== \"function\") {
            console.error(\"Cannot register a vanilla ready handler that is not a function.\");
            return;
        }
        document.addEventListener(\"X-DOMContentReady\", function () {
            if (!window.__VANILLA_INTERNAL_IS_READY__) {
                return;
            }
            handler(window.__VANILLA_GLOBALS_DO_NOT_USE_DIRECTLY__);
        })

        if (window.__VANILLA_INTERNAL_IS_READY__) {
            handler(window.__VANILLA_GLOBALS_DO_NOT_USE_DIRECTLY__);
        }
    }
}
{%- endautoescape -%}
", "library/Vanilla/Web/Asset/InlinePolyfillContent.js.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\library\\Vanilla\\Web\\Asset\\InlinePolyfillContent.js.twig");
    }
}
