<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a819d632a52517da775408703d12a4dfeea860e03c29a95b3afd475dcee6f5fa */
class __TwigTemplate_ee8b5b654f2408801f20909af3637579c355fe15a348d9c7b9de224e1aca9c9c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "__string_template__a819d632a52517da775408703d12a4dfeea860e03c29a95b3afd475dcee6f5fa";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    public function getSourceContext()
    {
        return new Source("{#
    This template is empty by default.
    Any header HTML added here will render _above_ Vanilla's built-in header.
    See https://success.vanillaforums.com/kb/articles/171-a-themes-structure#header

    Styles from the styles.css asset will apply to this header.
 #}
", "__string_template__a819d632a52517da775408703d12a4dfeea860e03c29a95b3afd475dcee6f5fa", "");
    }
}
