<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /plugins/googlesignin/views/settings.twig */
class __TwigTemplate_40e573a639aa8d4c8edce17ae3f3fe9fe4dd14a929a6b0217cc257d625669c56 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<h1>";
        echo twig_escape_filter($this->env, (isset($context["Title"]) || array_key_exists("Title", $context) ? $context["Title"] : (function () { throw new RuntimeError('Variable "Title" does not exist.', 1, $this->source); })()), "html", null, true);
        echo " </h1>
<div class=\"padded alert alert-warning\">
    ";
        // line 3
        echo (isset($context["Instructions"]) || array_key_exists("Instructions", $context) ? $context["Instructions"] : (function () { throw new RuntimeError('Variable "Instructions" does not exist.', 3, $this->source); })());
        echo "
</div>
 ";
        // line 5
        echo twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "open", [], "method", false, false, false, 5);
        echo "
 ";
        // line 6
        echo twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), "errors", [], "method", false, false, false, 6);
        echo "
 ";
        // line 7
        echo twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "simple", [0 => (isset($context["formData"]) || array_key_exists("formData", $context) ? $context["formData"] : (function () { throw new RuntimeError('Variable "formData" does not exist.', 7, $this->source); })())], "method", false, false, false, 7);
        echo "
<div class=\"modal-footer js-modal-fixed\">
";
        // line 9
        echo twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "button", [0 => "Save"], "method", false, false, false, 9);
        echo "
</div>
 ";
        // line 11
        echo twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "close", [], "method", false, false, false, 11);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/plugins/googlesignin/views/settings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 11,  61 => 9,  56 => 7,  52 => 6,  48 => 5,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h1>{{ Title }} </h1>
<div class=\"padded alert alert-warning\">
    {{ Instructions | raw }}
</div>
 {{ form.open() | raw }}
 {{ form.errors() | raw }}
 {{ form.simple(formData) | raw }}
<div class=\"modal-footer js-modal-fixed\">
{{ form.button('Save') | raw }}
</div>
 {{ form.close() | raw }}
", "/plugins/googlesignin/views/settings.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\plugins\\googlesignin\\views\\settings.twig");
    }
}
