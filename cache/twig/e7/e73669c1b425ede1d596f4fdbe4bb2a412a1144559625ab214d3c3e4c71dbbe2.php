<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* \library\Vanilla\Web/PageHead.twig */
class __TwigTemplate_7220b084b935c2d00e7e2c35733024dc44844a4bb1f7e142ea9c3691fbc46373 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<title>";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>

";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["metaTags"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["meta"]) {
            // line 4
            echo "<meta";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["meta"]);
            foreach ($context['_seq'] as $context["attribute"] => $context["value"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attribute"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attribute'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['meta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["linkTags"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 8
            echo "<link";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["link"]);
            foreach ($context['_seq'] as $context["attribute"] => $context["value"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attribute"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attribute'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "
";
        // line 11
        if (($context["favIcon"] ?? null)) {
            // line 12
            echo "<link rel=\"shortcut icon\" href=\"";
            echo twig_escape_filter($this->env, ($context["favIcon"] ?? null), "html", null, true);
            echo "\" type=\"image/x-icon\" />
";
        }
        // line 14
        echo "
";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["inlineScripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 16
            echo "<script nonce=\"";
            echo twig_escape_filter($this->env, ($context["nonce"] ?? null), "html", null, true);
            echo "\">";
            // line 17
            echo $context["script"];
            // line 18
            echo "</script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 22
            echo "<script nonce=\"";
            echo twig_escape_filter($this->env, ($context["nonce"] ?? null), "html", null, true);
            echo "\" defer src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["script"], "getWebPath", [], "method", false, false, false, 22), "html", null, true);
            echo "\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        if (($context["jsonLD"] ?? null)) {
            // line 26
            echo "<script type=\"application/ld+json\">";
            // line 27
            echo ($context["jsonLD"] ?? null);
            // line 28
            echo "</script>";
        }
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
            // line 32
            echo "<link href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["stylesheet"], "getWebPath", [], "method", false, false, false, 32), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo twig_get_attribute($this->env, $this->source, ($context["preloadModel"] ?? null), "renderHtml", [], "method", false, false, false, 35);
        echo "
";
    }

    public function getTemplateName()
    {
        return "\\library\\Vanilla\\Web/PageHead.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 35,  156 => 32,  152 => 31,  149 => 28,  147 => 27,  145 => 26,  143 => 25,  132 => 22,  128 => 21,  125 => 20,  118 => 18,  116 => 17,  112 => 16,  108 => 15,  105 => 14,  99 => 12,  97 => 11,  94 => 10,  74 => 8,  70 => 7,  67 => 6,  47 => 4,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "\\library\\Vanilla\\Web/PageHead.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\library\\Vanilla\\Web\\PageHead.twig");
    }
}
