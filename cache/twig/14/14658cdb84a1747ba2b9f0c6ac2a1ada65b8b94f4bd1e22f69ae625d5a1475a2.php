<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /applications/dashboard/views/settings/labs.twig */
class __TwigTemplate_c0a9f6db0dd59e09ee4b50bb96a785d9a49a77be2667006e4bf380a9bd8d471d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div data-react=\"VanillaLabsPage\"></div>
";
    }

    public function getTemplateName()
    {
        return "/applications/dashboard/views/settings/labs.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div data-react=\"VanillaLabsPage\"></div>
", "/applications/dashboard/views/settings/labs.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\applications\\dashboard\\views\\settings\\labs.twig");
    }
}
