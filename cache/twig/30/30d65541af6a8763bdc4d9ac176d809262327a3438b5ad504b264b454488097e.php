<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__5758fa6ba03753ec711534cd956b02fcc73ad6dcd9df9c8d5b7e1a6f5e7e0a28 */
class __TwigTemplate_bdb65ef48eba1eb6d1929eb7b93cdf421669b5295b4d08ca3809d6e1ff844271 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"";
        echo twig_escape_filter($this->env, (isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 1, $this->source); })()), "html", null, true);
        echo "\" data-react=\"";
        echo twig_escape_filter($this->env, (isset($context["component"]) || array_key_exists("component", $context) ? $context["component"] : (function () { throw new RuntimeError('Variable "component" does not exist.', 1, $this->source); })()), "html", null, true);
        echo "\" data-props=\"";
        echo twig_escape_filter($this->env, (isset($context["props"]) || array_key_exists("props", $context) ? $context["props"] : (function () { throw new RuntimeError('Variable "props" does not exist.', 1, $this->source); })()), "html", null, true);
        echo "\"></div>";
    }

    public function getTemplateName()
    {
        return "__string_template__5758fa6ba03753ec711534cd956b02fcc73ad6dcd9df9c8d5b7e1a6f5e7e0a28";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"{{ class }}\" data-react=\"{{ component }}\" data-props=\"{{ props }}\"></div>", "__string_template__5758fa6ba03753ec711534cd956b02fcc73ad6dcd9df9c8d5b7e1a6f5e7e0a28", "");
    }
}
