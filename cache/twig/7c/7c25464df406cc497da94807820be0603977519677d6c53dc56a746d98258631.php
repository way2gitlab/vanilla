<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @dashboard/components/dashboardHeading.twig */
class __TwigTemplate_481bfd821f05ef6353b1661cc8c913dff348db753a1e8fb29f609eb6e736b98f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        $macros["__internal_c6baebb994f1307649667aa057194fa2a7a4672a1a230867b5efa5a140b017bc"] = $this->macros["__internal_c6baebb994f1307649667aa057194fa2a7a4672a1a230867b5efa5a140b017bc"] = $this->loadTemplate("@dashboard/components/macros.twig", "@dashboard/components/dashboardHeading.twig", 11)->unwrap();
        // line 12
        echo "<header class=\"header-block\">
    <div class=\"title-block\">
        ";
        // line 14
        if (((twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "returnUrl", [], "any", true, true, false, 14)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "returnUrl", [], "any", false, false, false, 14), false)) : (false))) {
            // line 15
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), [twig_get_attribute($this->env, $this->source, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new RuntimeError('Variable "params" does not exist.', 15, $this->source); })()), "returnUrl", [], "any", false, false, false, 15)]), "html", null, true);
            echo "\" class=\"btn btn-icon btn-return\" aria-label=\"Return\">
                ";
            // line 16
            echo twig_call_macro($macros["__internal_c6baebb994f1307649667aa057194fa2a7a4672a1a230867b5efa5a140b017bc"], "macro_dashboardSymbol", [["name" => "chevron-left"]], 16, $context, $this->getSourceContext());
            echo "
            </a>
        ";
        }
        // line 19
        echo "        <h1>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new RuntimeError('Variable "params" does not exist.', 19, $this->source); })()), "title", [], "any", false, false, false, 19), "html", null, true);
        echo "</h1>
    </div>
    ";
        // line 21
        if (((twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "buttons", [], "any", true, true, false, 21)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "buttons", [], "any", false, false, false, 21), false)) : (false))) {
            // line 22
            echo "        <div class=\"btn-container\">
            ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new RuntimeError('Variable "params" does not exist.', 23, $this->source); })()), "buttons", [], "any", false, false, false, 23), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 26
        echo "</header>
";
    }

    public function getTemplateName()
    {
        return "@dashboard/components/dashboardHeading.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 26,  67 => 23,  64 => 22,  62 => 21,  56 => 19,  50 => 16,  45 => 15,  43 => 14,  39 => 12,  37 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("{#
    /**
     * Formats a h1 heading block for the dashboard. Only to be used once on a page as the h1 header.
     * Handles url-ifying. Adds an optional button or return link.
     *
     * @param {string} title
     * @param {string} returnUrl (optional) A URL to use for a back button.
     * @param {\\Twig\\Markup} buttons (optional) Twig button content.
     */
#}
{% from \"@dashboard/components/macros.twig\" import dashboardSymbol %}
<header class=\"header-block\">
    <div class=\"title-block\">
        {% if params.returnUrl|default(false) %}
            <a href=\"{{ url(params.returnUrl) }}\" class=\"btn btn-icon btn-return\" aria-label=\"Return\">
                {{ dashboardSymbol({ name: 'chevron-left' }) }}
            </a>
        {% endif %}
        <h1>{{ params.title }}</h1>
    </div>
    {% if params.buttons|default(false) %}
        <div class=\"btn-container\">
            {{ params.buttons }}
        </div>
    {% endif %}
</header>
", "@dashboard/components/dashboardHeading.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\applications\\dashboard\\views\\components\\dashboardHeading.twig");
    }
}
