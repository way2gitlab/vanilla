<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* \library\Vanilla\EmbeddedContent\Embeds/ImageEmbed.twig */
class __TwigTemplate_b1f8629af33e45d822c1a6d0f24571139953f6b796b909f254584be543fe41e1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"embedExternal embedImage display-";
        echo twig_escape_filter($this->env, (isset($context["displaySize"]) || array_key_exists("displaySize", $context) ? $context["displaySize"] : (function () { throw new RuntimeError('Variable "displaySize" does not exist.', 1, $this->source); })()), "html", null, true);
        echo " float-";
        echo twig_escape_filter($this->env, (isset($context["float"]) || array_key_exists("float", $context) ? $context["float"] : (function () { throw new RuntimeError('Variable "float" does not exist.', 1, $this->source); })()), "html", null, true);
        echo "\">
    <div class=\"embedExternal-content\">
        <a class=\"embedImage-link\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, Gdn_Format::sanitizeUrl((isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 3, $this->source); })())), "html", null, true);
        echo "\" rel=\"nofollow noreferrer noopener ugc\" target=\"_blank\">
            <img class=\"embedImage-img\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\"  height=\"";
        echo twig_escape_filter($this->env, (isset($context["height"]) || array_key_exists("height", $context) ? $context["height"] : (function () { throw new RuntimeError('Variable "height" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\" width=\"";
        echo twig_escape_filter($this->env, (isset($context["width"]) || array_key_exists("width", $context) ? $context["width"] : (function () { throw new RuntimeError('Variable "width" does not exist.', 4, $this->source); })()), "html", null, true);
        echo "\" loading=\"lazy\"/>
        </a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "\\library\\Vanilla\\EmbeddedContent\\Embeds/ImageEmbed.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  45 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"embedExternal embedImage display-{{ displaySize }} float-{{ float }}\">
    <div class=\"embedExternal-content\">
        <a class=\"embedImage-link\" href=\"{{ sanitizeUrl(url) }}\" rel=\"nofollow noreferrer noopener ugc\" target=\"_blank\">
            <img class=\"embedImage-img\" src=\"{{ url }}\" alt=\"{{ name }}\"  height=\"{{ height }}\" width=\"{{ width }}\" loading=\"lazy\"/>
        </a>
    </div>
</div>
", "\\library\\Vanilla\\EmbeddedContent\\Embeds/ImageEmbed.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\library\\Vanilla\\EmbeddedContent\\Embeds\\ImageEmbed.twig");
    }
}
