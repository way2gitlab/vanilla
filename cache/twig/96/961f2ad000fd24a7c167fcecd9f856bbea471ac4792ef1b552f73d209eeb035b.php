<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* \library\Vanilla\Web/MasterView.twig */
class __TwigTemplate_72fa9ab5a3143f166fa546154b5644b4bcf4aa18a62ea169a82f7efc5bae910b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["locale"]) || array_key_exists("locale", $context) ? $context["locale"] : (function () { throw new RuntimeError('Variable "locale" does not exist.', 2, $this->source); })()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["pageHead"]) || array_key_exists("pageHead", $context) ? $context["pageHead"] : (function () { throw new RuntimeError('Variable "pageHead" does not exist.', 7, $this->source); })()), "html", null, true);
        echo "
        <noscript>
            <style>
                .fullPageLoader { display: none }

                body.isLoading .page {
                    max-height: initial;
                    height: initial;
                }
            </style>
        </noscript>
    </head>
    <body class=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["cssClasses"]) || array_key_exists("cssClasses", $context) ? $context["cssClasses"] : (function () { throw new RuntimeError('Variable "cssClasses" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "\">
        <div id=\"page\" class=\"page\">";
        // line 21
        if ((isset($context["themeHeader"]) || array_key_exists("themeHeader", $context) ? $context["themeHeader"] : (function () { throw new RuntimeError('Variable "themeHeader" does not exist.', 21, $this->source); })())) {
            // line 22
            echo "<noscript id=\"themeHeader\">
                    ";
            // line 23
            echo twig_escape_filter($this->env, (isset($context["themeHeader"]) || array_key_exists("themeHeader", $context) ? $context["themeHeader"] : (function () { throw new RuntimeError('Variable "themeHeader" does not exist.', 23, $this->source); })()), "html", null, true);
            echo "
                </noscript>";
        }
        // line 26
        if (((array_key_exists("bodyContent", $context)) ? (_twig_default_filter((isset($context["bodyContent"]) || array_key_exists("bodyContent", $context) ? $context["bodyContent"] : (function () { throw new RuntimeError('Variable "bodyContent" does not exist.', 26, $this->source); })()), false)) : (false))) {
            // line 27
            echo "<header id=\"titleBar\" data-react=\"title-bar-hamburger\"></header>
                ";
            // line 28
            echo twig_escape_filter($this->env, (isset($context["bodyContent"]) || array_key_exists("bodyContent", $context) ? $context["bodyContent"] : (function () { throw new RuntimeError('Variable "bodyContent" does not exist.', 28, $this->source); })()), "html", null, true);
        } else {
            // line 30
            echo "<header id=\"titleBar\"></header>
                <div id=\"app\" class=\"page-minHeight\">";
            // line 32
            if (((array_key_exists("seoContent", $context)) ? (_twig_default_filter((isset($context["seoContent"]) || array_key_exists("seoContent", $context) ? $context["seoContent"] : (function () { throw new RuntimeError('Variable "seoContent" does not exist.', 32, $this->source); })()), false)) : (false))) {
                // line 33
                echo "<noscript id=\"fallbackPageContent\">
                            <h1 class=\"heading heading-1 pageTitle\">";
                // line 35
                echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 35, $this->source); })()), "html", null, true);
                // line 36
                echo "</h1>";
                // line 37
                echo twig_escape_filter($this->env, (isset($context["seoContent"]) || array_key_exists("seoContent", $context) ? $context["seoContent"] : (function () { throw new RuntimeError('Variable "seoContent" does not exist.', 37, $this->source); })()), "html", null, true);
                // line 38
                echo "</noscript>";
            }
            // line 40
            echo "<div class=\"fullPageLoader\"></div>
                </div>";
        }
        // line 43
        if ((isset($context["themeFooter"]) || array_key_exists("themeFooter", $context) ? $context["themeFooter"] : (function () { throw new RuntimeError('Variable "themeFooter" does not exist.', 43, $this->source); })())) {
            // line 44
            echo "<noscript id=\"themeFooter\">";
            echo twig_escape_filter($this->env, (isset($context["themeFooter"]) || array_key_exists("themeFooter", $context) ? $context["themeFooter"] : (function () { throw new RuntimeError('Variable "themeFooter" does not exist.', 44, $this->source); })()), "html", null, true);
            echo "</noscript>";
        }
        // line 46
        echo "</div>
        <div id=\"modals\"></div>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "\\library\\Vanilla\\Web/MasterView.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 46,  108 => 44,  106 => 43,  102 => 40,  99 => 38,  97 => 37,  95 => 36,  93 => 35,  90 => 33,  88 => 32,  85 => 30,  82 => 28,  79 => 27,  77 => 26,  72 => 23,  69 => 22,  67 => 21,  63 => 19,  48 => 7,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"{{ locale }}\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        {{ pageHead }}
        <noscript>
            <style>
                .fullPageLoader { display: none }

                body.isLoading .page {
                    max-height: initial;
                    height: initial;
                }
            </style>
        </noscript>
    </head>
    <body class=\"{{ cssClasses }}\">
        <div id=\"page\" class=\"page\">
            {%- if themeHeader -%}
                <noscript id=\"themeHeader\">
                    {{ themeHeader }}
                </noscript>
            {%- endif -%}
            {%- if bodyContent|default(false) -%}
                <header id=\"titleBar\" data-react=\"title-bar-hamburger\"></header>
                {{ bodyContent }}
            {%- else -%}
                <header id=\"titleBar\"></header>
                <div id=\"app\" class=\"page-minHeight\">
                    {%- if seoContent|default(false) -%}
                        <noscript id=\"fallbackPageContent\">
                            <h1 class=\"heading heading-1 pageTitle\">
                                {{- title -}}
                            </h1>
                            {{- seoContent -}}
                        </noscript>
                    {%- endif -%}
                    <div class=\"fullPageLoader\"></div>
                </div>
            {%- endif -%}
            {%- if themeFooter -%}
                <noscript id=\"themeFooter\">{{ themeFooter }}</noscript>
            {%- endif -%}
        </div>
        <div id=\"modals\"></div>
    </body>
</html>
", "\\library\\Vanilla\\Web/MasterView.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\library\\Vanilla\\Web\\MasterView.twig");
    }
}
