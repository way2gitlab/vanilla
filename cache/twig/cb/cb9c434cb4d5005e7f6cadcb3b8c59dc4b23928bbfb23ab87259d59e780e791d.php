<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @dashboard/components/dashboardSymbol.twig */
class __TwigTemplate_348dcee8296496c13a59e15f73c8742018371999a8beba1d5410c189f083d098 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
<svg
    class=\"";
        // line 14
        echo twig_escape_filter($this->env, Vanilla\Utility\HtmlUtils::classNames("icon", "icon-svg", ((twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "class", [], "any", true, true, false, 14)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "class", [], "any", false, false, false, 14))) : (""))), "html", null, true);
        echo "\"
    alt=\"";
        // line 15
        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "alt", [], "any", true, true, false, 15) &&  !(null === twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "alt", [], "any", false, false, false, 15)))) ? (twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "alt", [], "any", false, false, false, 15)) : (twig_get_attribute($this->env, $this->source, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new RuntimeError('Variable "params" does not exist.', 15, $this->source); })()), "name", [], "any", false, false, false, 15))), "html", null, true);
        echo "\"
    ";
        // line 16
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "dangerousAttributeString", [], "any", true, true, false, 16)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["params"] ?? null), "dangerousAttributeString", [], "any", false, false, false, 16))) : ("")), "html", null, true);
        echo "
    viewBox=\"0 0 17 17\"
>
    <use xlink:href=\"#";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new RuntimeError('Variable "params" does not exist.', 19, $this->source); })()), "name", [], "any", false, false, false, 19), "html", null, true);
        echo "\" ></use>
</svg>
";
    }

    public function getTemplateName()
    {
        return "@dashboard/components/dashboardSymbol.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 19,  49 => 16,  45 => 15,  41 => 14,  37 => 12,);
    }

    public function getSourceContext()
    {
        return new Source("{#
    /**
    * Render SVG icons in the dashboard. Icon must exist in applications/dashboard/views/symbols.php
    *
    * @param string \$name The name of the icon to render. Must be set in applications/dashboard/views/symbols.php.
    * @param string \$class If set, overrides any 'class' attribute in the \$attr param.
    * @param string \$alt Alt text for the symbol The default 'alt' attribute will be set to \$name.
    * @param string dangerousAttributeString (optional) A set of HTML attribtues that will be inserted raw into the template.
                    These should be pre-sanitized. This is included only for backwards compatibility. Try not to use it.
    */
#}

<svg
    class=\"{{ classNames('icon', 'icon-svg', params.class|default) }}\"
    alt=\"{{ params.alt ?? params.name }}\"
    {{ params.dangerousAttributeString|default }}
    viewBox=\"0 0 17 17\"
>
    <use xlink:href=\"#{{ params.name }}\" ></use>
</svg>
", "@dashboard/components/dashboardSymbol.twig", "D:\\phpstudy_pro\\WWW\\vanilla\\applications\\dashboard\\views\\components\\dashboardSymbol.twig");
    }
}
