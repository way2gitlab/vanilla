<?php return array (
  'openapi' => '3.0.2',
  'info' => 
  array (
    'description' => 'API access to your community.',
    'title' => 'Vanilla API',
    'version' => '2.0',
  ),
  'paths' => 
  array (
    '/addons' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The type of addon.',
            'in' => 'query',
            'name' => 'type',
            'schema' => 
            array (
              'enum' => 
              array (
                0 => 'addon',
                1 => 'theme',
                2 => 'locale',
              ),
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter enabled or disabled addons.
',
            'in' => 'query',
            'name' => 'enabled',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
          ),
          2 => 
          array (
            'description' => 'Which theme to show the enabled status for.
',
            'in' => 'query',
            'name' => 'themeType',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'desktop',
              'enum' => 
              array (
                0 => 'desktop',
                1 => 'mobile',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Addon',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Addons',
        ),
        'summary' => 'List addons.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/addons/{addonID}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The ID of the addon.
',
            'in' => 'path',
            'name' => 'addonID',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'Which theme to show the enabled status for.
',
            'in' => 'query',
            'name' => 'themeType',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'desktop',
              'enum' => 
              array (
                0 => 'desktop',
                1 => 'mobile',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Addon',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Addons',
        ),
        'summary' => 'Get an addon.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The ID of the addon.',
            'in' => 'path',
            'name' => 'addonID',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Addon',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Addons',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'enabled' => 
                  array (
                    'description' => 'Enable or disable the addon.',
                    'type' => 'boolean',
                  ),
                  'themeType' => 
                  array (
                    'type' => 'string',
                    'default' => 'desktop',
                    'description' => 'Which theme type to set.',
                    'enum' => 
                    array (
                      0 => 'desktop',
                      1 => 'mobile',
                    ),
                  ),
                ),
                'required' => 
                array (
                  0 => 'enabled',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Enable or disable an addon.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/authenticators' => 
    array (
      'get' => 
      array (
        'summary' => 'List the authenticators.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'type',
            'description' => 'Filter by one or more authenticator types.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'string',
              ),
              'format' => 'form',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          2 => 
          array (
            'name' => 'limit',
            'in' => 'query',
            'description' => 'Maximum number of items to be included in the response. See [Pagination](https://docs.vanillaforums.com/apiv2/#pagination).
',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 10,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/AuthenticatorFragment',
                  ),
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/authenticators/oauth2' => 
    array (
      'post' => 
      array (
        'summary' => 'Add a new OAuth 2 connection.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/OAuth2AuthenticatorPost',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          201 => 
          array (
            'description' => 'Created',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/OAuth2Authenticator',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'oauth2',
      ),
    ),
    '/authenticators/oauth2/{id}' => 
    array (
      'parameters' => 
      array (
        0 => 
        array (
          'name' => 'id',
          'in' => 'path',
          'required' => true,
          'description' => 'The ID of the authenticator.',
          'schema' => 
          array (
            'type' => 'integer',
          ),
          'x-addon' => 'oauth2',
        ),
      ),
      'get' => 
      array (
        'summary' => 'Get an OAuth2 authenticator\'s information.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/OAuth2Authenticator',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'oauth2',
      ),
      'patch' => 
      array (
        'summary' => 'Update an OAuth2 authenticator\'s information.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/OAuth2AuthenticatorPatch',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/OAuth2Authenticator',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'oauth2',
      ),
    ),
    '/authenticators/{id}' => 
    array (
      'parameters' => 
      array (
        0 => 
        array (
          'name' => 'id',
          'in' => 'path',
          'description' => 'The ID of the authenticator to look up.',
          'required' => true,
          'schema' => 
          array (
            'type' => 'integer',
          ),
          'x-addon' => 'dashboard',
        ),
      ),
      'delete' => 
      array (
        'summary' => 'Delete an authenticator.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'summary' => 'Get a single authenticator.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/AuthenticatorFragment',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'summary' => 'Update an authenticator.',
        'tags' => 
        array (
          0 => 'Authenticators',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'type' => 'object',
                'properties' => 
                array (
                  'default' => 
                  array (
                    'type' => 'boolean',
                  ),
                  'active' => 
                  array (
                    'type' => 'boolean',
                  ),
                  'visible' => 
                  array (
                    'type' => 'boolean',
                  ),
                ),
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/AuthenticatorFragment',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/categories' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'categoryID',
            'description' => 'Filter by a range or CSV of category IDs.',
            'in' => 'query',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RangeExpression',
            ),
          ),
          1 => 
          array (
            'description' => 'Parent category ID.',
            'in' => 'query',
            'name' => 'parentCategoryID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            'description' => 'Parent category URL code.',
            'in' => 'query',
            'name' => 'parentCategoryCode',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          3 => 
          array (
            'description' => 'Only list categories followed by the current user.',
            'in' => 'query',
            'name' => 'followed',
            'required' => false,
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          4 => 
          array (
            'description' => 'The maximum tree depth to return when getting a tree structure.',
            'in' => 'query',
            'name' => 'maxDepth',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 2,
            ),
          ),
          5 => 
          array (
            'name' => 'archived',
            'description' => 'Filter by archived status of a category. True for archived only. False for no archived categories. Not compatible with followed filter.
',
            'in' => 'query',
            'required' => false,
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          6 => 
          array (
            'description' => 'Only list featured categories.',
            'in' => 'query',
            'name' => 'featured',
            'required' => false,
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          7 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          8 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'categoryID' => 
                      array (
                        'description' => 'The ID of the category.',
                        'type' => 'integer',
                      ),
                      'children' => 
                      array (
                        'items' => 
                        array (
                          'properties' => 
                          array (
                            'categoryID' => 
                            array (
                              'description' => 'The ID of the category.',
                              'type' => 'integer',
                            ),
                            'children' => 
                            array (
                              'items' => 
                              array (
                                'type' => 'string',
                              ),
                              'type' => 'array',
                            ),
                            'countAllComments' => 
                            array (
                              'description' => 'Total of all comments in a category and its children.',
                              'type' => 'integer',
                            ),
                            'countAllDiscussions' => 
                            array (
                              'description' => 'Total of all discussions in a category and its children.',
                              'type' => 'integer',
                            ),
                            'countCategories' => 
                            array (
                              'description' => 'Total number of child categories.',
                              'type' => 'integer',
                            ),
                            'countComments' => 
                            array (
                              'description' => 'Total comments in the category.',
                              'type' => 'integer',
                            ),
                            'countDiscussions' => 
                            array (
                              'description' => 'Total discussions in the category.',
                              'type' => 'integer',
                            ),
                            'customPermissions' => 
                            array (
                              'description' => 'Are custom permissions set for this category?',
                              'type' => 'boolean',
                            ),
                            'depth' => 
                            array (
                              'type' => 'integer',
                            ),
                            'description' => 
                            array (
                              'description' => 'The description of the category.',
                              'minLength' => 0,
                              'nullable' => true,
                              'type' => 'string',
                            ),
                            'displayAs' => 
                            array (
                              'type' => 'string',
                              'default' => 'discussions',
                              'description' => 'The display style of the category.',
                              'enum' => 
                              array (
                                0 => 'categories',
                                1 => 'discussions',
                                2 => 'flat',
                                3 => 'heading',
                              ),
                              'minLength' => 1,
                            ),
                            'followed' => 
                            array (
                              'description' => 'Is the category being followed by the current user?',
                              'type' => 'boolean',
                            ),
                            'isArchived' => 
                            array (
                              'description' => 'The archived state of this category.',
                              'type' => 'boolean',
                            ),
                            'name' => 
                            array (
                              'description' => 'The name of the category.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                            'parentCategoryID' => 
                            array (
                              'description' => 'Parent category ID.',
                              'nullable' => true,
                              'type' => 'integer',
                            ),
                            'url' => 
                            array (
                              'description' => 'The URL to the category.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                            'urlcode' => 
                            array (
                              'description' => 'The URL code of the category.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                            'allowedDiscussionTypes' => 
                            array (
                              'description' => 'Allowed category discussion types.',
                              'type' => 'array',
                            ),
                          ),
                          'required' => 
                          array (
                            0 => 'categoryID',
                            1 => 'name',
                            2 => 'description',
                            3 => 'parentCategoryID',
                            4 => 'customPermissions',
                            5 => 'isArchived',
                            6 => 'urlcode',
                            7 => 'url',
                            8 => 'displayAs',
                            9 => 'countCategories',
                            10 => 'countDiscussions',
                            11 => 'countComments',
                            12 => 'countAllDiscussions',
                            13 => 'countAllComments',
                            14 => 'depth',
                            15 => 'children',
                          ),
                          'type' => 'object',
                        ),
                        'type' => 'array',
                      ),
                      'countAllComments' => 
                      array (
                        'description' => 'Total of all comments in a category and its children.',
                        'type' => 'integer',
                      ),
                      'countAllDiscussions' => 
                      array (
                        'description' => 'Total of all discussions in a category and its children.',
                        'type' => 'integer',
                      ),
                      'countCategories' => 
                      array (
                        'description' => 'Total number of child categories.',
                        'type' => 'integer',
                      ),
                      'countComments' => 
                      array (
                        'description' => 'Total comments in the category.',
                        'type' => 'integer',
                      ),
                      'countDiscussions' => 
                      array (
                        'description' => 'Total discussions in the category.',
                        'type' => 'integer',
                      ),
                      'customPermissions' => 
                      array (
                        'description' => 'Are custom permissions set for this category?',
                        'type' => 'boolean',
                      ),
                      'depth' => 
                      array (
                        'type' => 'integer',
                      ),
                      'description' => 
                      array (
                        'description' => 'The description of the category.',
                        'minLength' => 0,
                        'nullable' => true,
                        'type' => 'string',
                      ),
                      'displayAs' => 
                      array (
                        'type' => 'string',
                        'default' => 'discussions',
                        'description' => 'The display style of the category.',
                        'enum' => 
                        array (
                          0 => 'categories',
                          1 => 'discussions',
                          2 => 'flat',
                          3 => 'heading',
                        ),
                        'minLength' => 1,
                      ),
                      'followed' => 
                      array (
                        'description' => 'Is the category being followed by the current user?',
                        'type' => 'boolean',
                      ),
                      'isArchived' => 
                      array (
                        'description' => 'The archived state of this category.',
                        'type' => 'boolean',
                      ),
                      'name' => 
                      array (
                        'description' => 'The name of the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'parentCategoryID' => 
                      array (
                        'description' => 'Parent category ID.',
                        'nullable' => true,
                        'type' => 'integer',
                      ),
                      'url' => 
                      array (
                        'description' => 'The URL to the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'urlcode' => 
                      array (
                        'description' => 'The URL code of the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'categoryID',
                      1 => 'name',
                      2 => 'description',
                      3 => 'parentCategoryID',
                      4 => 'customPermissions',
                      5 => 'isArchived',
                      6 => 'urlcode',
                      7 => 'url',
                      8 => 'displayAs',
                      9 => 'countCategories',
                      10 => 'countDiscussions',
                      11 => 'countComments',
                      12 => 'countAllDiscussions',
                      13 => 'countAllComments',
                      14 => 'depth',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'summary' => 'List categories.',
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'categoryID' => 
                    array (
                      'description' => 'The ID of the category.',
                      'type' => 'integer',
                    ),
                    'countAllComments' => 
                    array (
                      'description' => 'Total of all comments in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countAllDiscussions' => 
                    array (
                      'description' => 'Total of all discussions in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countCategories' => 
                    array (
                      'description' => 'Total number of child categories.',
                      'type' => 'integer',
                    ),
                    'countComments' => 
                    array (
                      'description' => 'Total comments in the category.',
                      'type' => 'integer',
                    ),
                    'countDiscussions' => 
                    array (
                      'description' => 'Total discussions in the category.',
                      'type' => 'integer',
                    ),
                    'customPermissions' => 
                    array (
                      'description' => 'Are custom permissions set for this category?',
                      'type' => 'boolean',
                    ),
                    'description' => 
                    array (
                      'description' => 'The description of the category.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'displayAs' => 
                    array (
                      'type' => 'string',
                      'default' => 'discussions',
                      'description' => 'The display style of the category.',
                      'enum' => 
                      array (
                        0 => 'categories',
                        1 => 'discussions',
                        2 => 'flat',
                        3 => 'heading',
                      ),
                      'minLength' => 1,
                    ),
                    'followed' => 
                    array (
                      'description' => 'Is the category being followed by the current user?',
                      'type' => 'boolean',
                    ),
                    'isArchived' => 
                    array (
                      'description' => 'The archived state of this category.',
                      'type' => 'boolean',
                    ),
                    'name' => 
                    array (
                      'description' => 'The name of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'parentCategoryID' => 
                    array (
                      'description' => 'Parent category ID.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL to the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'urlcode' => 
                    array (
                      'description' => 'The URL code of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'categoryID',
                    1 => 'name',
                    2 => 'description',
                    3 => 'parentCategoryID',
                    4 => 'customPermissions',
                    5 => 'isArchived',
                    6 => 'urlcode',
                    7 => 'url',
                    8 => 'displayAs',
                    9 => 'countCategories',
                    10 => 'countDiscussions',
                    11 => 'countComments',
                    12 => 'countAllDiscussions',
                    13 => 'countAllComments',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'requestBody' => 
        array (
          '$ref' => '#/components/requestBodies/CategoryPost',
        ),
        'summary' => 'Add a category.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/categories/search' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Category name filter.
',
            'in' => 'query',
            'name' => 'query',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'Page number. See [Pagination](https://docs.vanillaforums.com/apiv2/#pagination).
',
            'in' => 'query',
            'name' => 'page',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 1,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          2 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 200,
              'minimum' => 1,
            ),
          ),
          3 => 
          array (
            'description' => 'Expand associated records using one or more valid field names (all, parent, breadcrumbs).
A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'breadcrumbs',
                  1 => 'parent',
                  2 => 'all',
                ),
                'type' => 'string',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'categoryID' => 
                      array (
                        'description' => 'The ID of the category.',
                        'type' => 'integer',
                      ),
                      'countAllComments' => 
                      array (
                        'description' => 'Total of all comments in a category and its children.',
                        'type' => 'integer',
                      ),
                      'countAllDiscussions' => 
                      array (
                        'description' => 'Total of all discussions in a category and its children.',
                        'type' => 'integer',
                      ),
                      'countCategories' => 
                      array (
                        'description' => 'Total number of child categories.',
                        'type' => 'integer',
                      ),
                      'countComments' => 
                      array (
                        'description' => 'Total comments in the category.',
                        'type' => 'integer',
                      ),
                      'countDiscussions' => 
                      array (
                        'description' => 'Total discussions in the category.',
                        'type' => 'integer',
                      ),
                      'customPermissions' => 
                      array (
                        'description' => 'Are custom permissions set for this category?',
                        'type' => 'boolean',
                      ),
                      'description' => 
                      array (
                        'description' => 'The description of the category.',
                        'minLength' => 0,
                        'nullable' => true,
                        'type' => 'string',
                      ),
                      'displayAs' => 
                      array (
                        'type' => 'string',
                        'default' => 'discussions',
                        'description' => 'The display style of the category.',
                        'enum' => 
                        array (
                          0 => 'categories',
                          1 => 'discussions',
                          2 => 'flat',
                          3 => 'heading',
                        ),
                        'minLength' => 1,
                      ),
                      'followed' => 
                      array (
                        'description' => 'Is the category being followed by the current user?',
                        'type' => 'boolean',
                      ),
                      'isArchived' => 
                      array (
                        'description' => 'The archived state of this category.',
                        'type' => 'boolean',
                      ),
                      'name' => 
                      array (
                        'description' => 'The name of the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'parentCategoryID' => 
                      array (
                        'description' => 'Parent category ID.',
                        'nullable' => true,
                        'type' => 'integer',
                      ),
                      'url' => 
                      array (
                        'description' => 'The URL to the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'urlcode' => 
                      array (
                        'description' => 'The URL code of the category.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'breadcrumbs' => 
                      array (
                        'items' => 
                        array (
                          'properties' => 
                          array (
                            'name' => 
                            array (
                              'description' => 'Breadcrumb element name.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                            'url' => 
                            array (
                              'description' => 'Breadcrumb element url.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                          ),
                          'required' => 
                          array (
                            0 => 'name',
                            1 => 'url',
                          ),
                          'type' => 'object',
                        ),
                        'type' => 'array',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'categoryID',
                      1 => 'name',
                      2 => 'description',
                      3 => 'parentCategoryID',
                      4 => 'customPermissions',
                      5 => 'isArchived',
                      6 => 'urlcode',
                      7 => 'url',
                      8 => 'displayAs',
                      9 => 'countCategories',
                      10 => 'countDiscussions',
                      11 => 'countComments',
                      12 => 'countAllDiscussions',
                      13 => 'countAllComments',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'summary' => 'Search categories.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/categories/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The category ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'summary' => 'Delete a category.',
        'x-addon' => 'vanilla',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The category ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'categoryID' => 
                    array (
                      'description' => 'The ID of the category.',
                      'type' => 'integer',
                    ),
                    'countAllComments' => 
                    array (
                      'description' => 'Total of all comments in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countAllDiscussions' => 
                    array (
                      'description' => 'Total of all discussions in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countCategories' => 
                    array (
                      'description' => 'Total number of child categories.',
                      'type' => 'integer',
                    ),
                    'countComments' => 
                    array (
                      'description' => 'Total comments in the category.',
                      'type' => 'integer',
                    ),
                    'countDiscussions' => 
                    array (
                      'description' => 'Total discussions in the category.',
                      'type' => 'integer',
                    ),
                    'customPermissions' => 
                    array (
                      'description' => 'Are custom permissions set for this category?',
                      'type' => 'boolean',
                    ),
                    'description' => 
                    array (
                      'description' => 'The description of the category.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'displayAs' => 
                    array (
                      'type' => 'string',
                      'default' => 'discussions',
                      'description' => 'The display style of the category.',
                      'enum' => 
                      array (
                        0 => 'categories',
                        1 => 'discussions',
                        2 => 'flat',
                        3 => 'heading',
                      ),
                      'minLength' => 1,
                    ),
                    'followed' => 
                    array (
                      'description' => 'Is the category being followed by the current user?',
                      'type' => 'boolean',
                    ),
                    'isArchived' => 
                    array (
                      'description' => 'The archived state of this category.',
                      'type' => 'boolean',
                    ),
                    'name' => 
                    array (
                      'description' => 'The name of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'parentCategoryID' => 
                    array (
                      'description' => 'Parent category ID.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL to the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'urlcode' => 
                    array (
                      'description' => 'The URL code of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'categoryID',
                    1 => 'name',
                    2 => 'description',
                    3 => 'parentCategoryID',
                    4 => 'customPermissions',
                    5 => 'isArchived',
                    6 => 'urlcode',
                    7 => 'url',
                    8 => 'displayAs',
                    9 => 'countCategories',
                    10 => 'countDiscussions',
                    11 => 'countComments',
                    12 => 'countAllDiscussions',
                    13 => 'countAllComments',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'summary' => 'Get a category.',
        'x-addon' => 'vanilla',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The category ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'categoryID' => 
                    array (
                      'description' => 'The ID of the category.',
                      'type' => 'integer',
                    ),
                    'countAllComments' => 
                    array (
                      'description' => 'Total of all comments in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countAllDiscussions' => 
                    array (
                      'description' => 'Total of all discussions in a category and its children.',
                      'type' => 'integer',
                    ),
                    'countCategories' => 
                    array (
                      'description' => 'Total number of child categories.',
                      'type' => 'integer',
                    ),
                    'countComments' => 
                    array (
                      'description' => 'Total comments in the category.',
                      'type' => 'integer',
                    ),
                    'countDiscussions' => 
                    array (
                      'description' => 'Total discussions in the category.',
                      'type' => 'integer',
                    ),
                    'customPermissions' => 
                    array (
                      'description' => 'Are custom permissions set for this category?',
                      'type' => 'boolean',
                    ),
                    'description' => 
                    array (
                      'description' => 'The description of the category.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'displayAs' => 
                    array (
                      'type' => 'string',
                      'default' => 'discussions',
                      'description' => 'The display style of the category.',
                      'enum' => 
                      array (
                        0 => 'categories',
                        1 => 'discussions',
                        2 => 'flat',
                        3 => 'heading',
                      ),
                      'minLength' => 1,
                    ),
                    'followed' => 
                    array (
                      'description' => 'Is the category being followed by the current user?',
                      'type' => 'boolean',
                    ),
                    'isArchived' => 
                    array (
                      'description' => 'The archived state of this category.',
                      'type' => 'boolean',
                    ),
                    'name' => 
                    array (
                      'description' => 'The name of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'parentCategoryID' => 
                    array (
                      'description' => 'Parent category ID.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL to the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'urlcode' => 
                    array (
                      'description' => 'The URL code of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'categoryID',
                    1 => 'name',
                    2 => 'description',
                    3 => 'parentCategoryID',
                    4 => 'customPermissions',
                    5 => 'isArchived',
                    6 => 'urlcode',
                    7 => 'url',
                    8 => 'displayAs',
                    9 => 'countCategories',
                    10 => 'countDiscussions',
                    11 => 'countComments',
                    12 => 'countAllDiscussions',
                    13 => 'countAllComments',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/CategoryPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a category.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/categories/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The category ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'categoryID' => 
                    array (
                      'description' => 'The ID of the category.',
                      'type' => 'integer',
                    ),
                    'description' => 
                    array (
                      'description' => 'The description of the category.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'displayAs' => 
                    array (
                      'type' => 'string',
                      'default' => 'discussions',
                      'description' => 'The display style of the category.',
                      'enum' => 
                      array (
                        0 => 'categories',
                        1 => 'discussions',
                        2 => 'flat',
                        3 => 'heading',
                      ),
                      'minLength' => 1,
                    ),
                    'name' => 
                    array (
                      'description' => 'The name of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'parentCategoryID' => 
                    array (
                      'description' => 'Parent category ID.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'urlcode' => 
                    array (
                      'description' => 'The URL code of the category.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'categoryID',
                    1 => 'name',
                    2 => 'parentCategoryID',
                    3 => 'urlcode',
                    4 => 'description',
                    5 => 'displayAs',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'summary' => 'Get a category for editing.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/categories/{id}/follow' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'followed' => 
                    array (
                      'description' => 'The category-follow status for the current user.',
                      'type' => 'boolean',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'followed',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Categories',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'followed' => 
                  array (
                    'description' => 'The category-follow status for the current user.',
                    'type' => 'boolean',
                  ),
                ),
                'required' => 
                array (
                  0 => 'followed',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'commentID',
            'in' => 'query',
            'description' => 'Filter by a range or CSV of comment IDs.',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RangeExpression',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/DateInserted',
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/DateUpdated',
          ),
          3 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'query',
            'name' => 'discussionID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          4 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          5 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => '30',
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          6 => 
          array (
            'name' => 'sort',
            'description' => 'Sort the results.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
              'enum' => 
              array (
                0 => 'dateInserted',
                1 => 'commentID',
                2 => '-dateInserted',
                3 => '-commentID',
              ),
            ),
          ),
          7 => 
          array (
            'description' => 'Filter by author.
',
            'in' => 'query',
            'name' => 'insertUserID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          8 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'insertUser',
                  1 => 'all',
                  2 => 'reactions',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Comment',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'List comments.',
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Comment',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'requestBody' => 
        array (
          '$ref' => '#/components/requestBodies/CommentPost',
        ),
        'summary' => 'Add a comment.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/search' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The numeric ID of a category.
',
            'in' => 'query',
            'name' => 'categoryID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Search terms.
',
            'in' => 'query',
            'name' => 'query',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          4 => 
          array (
            'description' => 'Expand associated records.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Comment',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Search comments.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'reactions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Delete a comment.',
        'x-addon' => 'vanilla',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'reactions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Comment',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Get a comment.',
        'x-addon' => 'vanilla',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Comment',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/CommentPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a comment.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}/answer' => 
    array (
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Comment',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/CommentGet',
              ),
            ),
          ),
          'required' => true,
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'reactions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'body' => 
                    array (
                      'description' => 'The body of the comment.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'commentID' => 
                    array (
                      'description' => 'The ID of the comment.',
                      'type' => 'integer',
                    ),
                    'discussionID' => 
                    array (
                      'description' => 'The ID of the discussion.',
                      'type' => 'integer',
                    ),
                    'format' => 
                    array (
                      '$ref' => '#/components/schemas/Format',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'commentID',
                    1 => 'discussionID',
                    2 => 'body',
                    3 => 'format',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Get a comment for editing.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}/quote' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'reactions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'bodyRaw' => 
                    array (
                      'description' => 'The raw body of the comment. This can be an array of rich operations or a string for other formats',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'commentID' => 
                    array (
                      'description' => 'The ID of the comment.',
                      'type' => 'integer',
                    ),
                    'dateInserted' => 
                    array (
                      'description' => 'When the comment was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'dateUpdated' => 
                    array (
                      'description' => 'When the comment was last updated.',
                      'format' => 'date-time',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'format' => 
                    array (
                      '$ref' => '#/components/schemas/Format',
                    ),
                    'insertUser' => 
                    array (
                      '$ref' => '#/components/schemas/UserFragment',
                    ),
                    'url' => 
                    array (
                      'description' => 'The full URL to the comment.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'commentID',
                    1 => 'bodyRaw',
                    2 => 'dateInserted',
                    3 => 'dateUpdated',
                    4 => 'insertUser',
                    5 => 'url',
                    6 => 'format',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}/reactions' => 
    array (
      'x-addon' => 'reactions',
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter to a specific reaction type by using its URL code.
',
            'in' => 'query',
            'name' => 'type',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
            'allowEmptyValue' => true,
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'dateInserted' => 
                      array (
                        'format' => 'date-time',
                        'type' => 'string',
                      ),
                      'reactionType' => 
                      array (
                        'properties' => 
                        array (
                          'class' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                          'name' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                          'tagID' => 
                          array (
                            'type' => 'integer',
                          ),
                          'urlcode' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                        ),
                        'required' => 
                        array (
                          0 => 'tagID',
                          1 => 'urlcode',
                          2 => 'name',
                          3 => 'class',
                        ),
                        'type' => 'object',
                      ),
                      'recordID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'recordType' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'tagID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'user' => 
                      array (
                        '$ref' => '#/components/schemas/UserFragment',
                      ),
                      'userID' => 
                      array (
                        'type' => 'integer',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'recordType',
                      1 => 'recordID',
                      2 => 'tagID',
                      3 => 'userID',
                      4 => 'dateInserted',
                      5 => 'user',
                      6 => 'reactionType',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Get reactions to a comment.',
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'class' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'count' => 
                      array (
                        'type' => 'integer',
                      ),
                      'name' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'tagID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'urlcode' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'tagID',
                      1 => 'urlcode',
                      2 => 'name',
                      3 => 'class',
                      4 => 'count',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'reactionType' => 
                  array (
                    'description' => 'URL code of a reaction type.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'reactionType',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'React to a comment.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/comments/{id}/reactions/{userID}' => 
    array (
      'x-addon' => 'reactions',
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The comment ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'The target user ID.
',
            'in' => 'path',
            'name' => 'userID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'reactions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Comments',
        ),
        'summary' => 'Remove a user\'s reaction.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/config' => 
    array (
      'get' => 
      array (
        'summary' => 'Get the site\'s config.',
        'tags' => 
        array (
          0 => 'Config',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'select',
            'in' => 'query',
            'description' => 'A CSV of config key names to select a sparse set of items. Use * as a wildcard to match multiple keys.
',
            'style' => 'form',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'string',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Config',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'summary' => 'Update one or more config settings.',
        'tags' => 
        array (
          0 => 'Config',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/Config',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/conversations' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Filter by author.
',
            'in' => 'query',
            'name' => 'insertUserID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter by participating user. (Has no effect if insertUserID is used)
',
            'in' => 'query',
            'name' => 'participantUserID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 50,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          4 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'insertUser',
                  1 => 'lastInsertUser',
                  2 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Conversation',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'summary' => 'List user conversations.',
        'x-addon' => 'conversations',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Conversation',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'requestBody' => 
        array (
          '$ref' => '#/components/requestBodies/ConversationPost',
        ),
        'summary' => 'Add a conversation.',
        'x-addon' => 'conversations',
      ),
    ),
    '/conversations/{id}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The conversation ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Conversation',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'summary' => 'Get a conversation.',
        'x-addon' => 'conversations',
      ),
    ),
    '/conversations/{id}/leave' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The conversation ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'summary' => 'Leave a conversation.',
        'x-addon' => 'conversations',
      ),
    ),
    '/conversations/{id}/participants' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The conversation ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter by participant status.
',
            'in' => 'query',
            'name' => 'status',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'participating',
              'enum' => 
              array (
                0 => 'all',
                1 => 'participating',
                2 => 'deleted',
              ),
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 5,
              'maximum' => 100,
              'minimum' => 5,
            ),
          ),
          4 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'user',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ConversationParticipants',
                ),
              ),
            ),
            'description' => 'List of participants.',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'summary' => 'Get participants of a conversation.',
        'x-addon' => 'conversations',
      ),
      'post' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The conversation ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ConversationParticipants',
                ),
              ),
            ),
            'description' => 'List of participants.',
          ),
        ),
        'tags' => 
        array (
          0 => 'Conversations',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/ConversationPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add participants to a conversation.',
        'x-addon' => 'conversations',
      ),
    ),
    '/dashboard/menus' => 
    array (
      'get' => 
      array (
        'summary' => 'List the dashboard menus.',
        'description' => 'This endpoint returns the entire navigation structure of the dashboard. It has three levels:

1. The first level represents main menu sections.
2. Each section can be seperated into groups.
3. Groups contain the actual menu links.
',
        'tags' => 
        array (
          0 => 'Dashboard',
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'description' => 'Represents a main menu item.',
                    'type' => 'object',
                    'properties' => 
                    array (
                      'description' => 
                      array (
                        'description' => 'The menu description.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'groups' => 
                      array (
                        'items' => 
                        array (
                          'properties' => 
                          array (
                            'key' => 
                            array (
                              'description' => 'The key of the group.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                            'links' => 
                            array (
                              'items' => 
                              array (
                                'properties' => 
                                array (
                                  'badge' => 
                                  array (
                                    'description' => 'Information about a badge to display beside the link.',
                                    'properties' => 
                                    array (
                                      'text' => 
                                      array (
                                        'description' => 'Literal text for the badge.',
                                        'minLength' => 1,
                                        'type' => 'string',
                                      ),
                                      'type' => 
                                      array (
                                        'description' => 'The type of badge.',
                                        'enum' => 
                                        array (
                                          0 => 'view',
                                          1 => 'text',
                                        ),
                                        'minLength' => 1,
                                        'type' => 'string',
                                      ),
                                      'url' => 
                                      array (
                                        'description' => 'The URL of a view.',
                                        'type' => 'string',
                                      ),
                                    ),
                                    'type' => 'object',
                                  ),
                                  'key' => 
                                  array (
                                    'description' => 'The key of the link.',
                                    'minLength' => 1,
                                    'type' => 'string',
                                  ),
                                  'name' => 
                                  array (
                                    'description' => 'The title of the link.',
                                    'minLength' => 1,
                                    'type' => 'string',
                                  ),
                                  'react' => 
                                  array (
                                    'description' => 'Whether or not the link represents a React component.',
                                    'type' => 'boolean',
                                  ),
                                  'url' => 
                                  array (
                                    'description' => 'The URL of the link.',
                                    'minLength' => 1,
                                    'type' => 'string',
                                  ),
                                ),
                                'required' => 
                                array (
                                  0 => 'name',
                                  1 => 'key',
                                  2 => 'url',
                                  3 => 'react',
                                ),
                                'type' => 'object',
                              ),
                              'type' => 'array',
                            ),
                            'name' => 
                            array (
                              'description' => 'The title of the group.',
                              'minLength' => 1,
                              'type' => 'string',
                            ),
                          ),
                          'required' => 
                          array (
                            0 => 'name',
                            1 => 'key',
                            2 => 'links',
                          ),
                          'type' => 'object',
                        ),
                        'type' => 'array',
                      ),
                      'key' => 
                      array (
                        'description' => 'The ID of the menu.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'name' => 
                      array (
                        'description' => 'The title of the menu.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'url' => 
                      array (
                        'description' => 'The URL to the menu if it doesn\'t have a submenu.',
                        'type' => 'string',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'name',
                      1 => 'key',
                      2 => 'description',
                      3 => 'groups',
                    ),
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/discussions' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'discussionID',
            'description' => 'Filter by a range or CSV of discussion IDs.',
            'in' => 'query',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RangeExpression',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter by a category.',
            'in' => 'query',
            'name' => 'categoryID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/DateInserted',
          ),
          3 => 
          array (
            '$ref' => '#/components/parameters/DateUpdated',
          ),
          4 => 
          array (
            '$ref' => '#/components/parameters/DateLastComment',
          ),
          5 => 
          array (
            'description' => 'Filter discussions by site-section-id (subcommunity). The subcommunityID or folder can be used. The query looks like: siteSectionID=$SubcommunityID:{id} ie. 1 siteSectionID=$SubcommunityID:{folder} ie. en
',
            'in' => 'query',
            'name' => 'siteSectionID',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          6 => 
          array (
            'description' => 'Filter by discussion type.',
            'in' => 'query',
            'name' => 'type',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-filter' => 
            array (
              'field' => 'd.Type',
            ),
          ),
          7 => 
          array (
            'description' => 'Filter questions by status (accepted, answered, unanswered).',
            'in' => 'query',
            'name' => 'status',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-filter' => 
            array (
              'field' => 'd.QnA',
            ),
          ),
          8 => 
          array (
            'description' => 'Only fetch discussions from followed categories. Pinned discussions are mixed in.
',
            'in' => 'query',
            'name' => 'followed',
            'required' => true,
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          9 => 
          array (
            'description' => 'Whether or not to include pinned discussions. If true, only return pinned discussions. Cannot be used with the pinOrder parameter.
',
            'in' => 'query',
            'name' => 'pinned',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
          ),
          10 => 
          array (
            'description' => 'If including pinned posts, in what order should they be integrated? When "first", discussions pinned to a specific category will only be affected if the discussion\'s category is passed as the categoryID parameter. Cannot be used with the pinned parameter.
Must be one of: "first", "mixed".
',
            'in' => 'query',
            'name' => 'pinOrder',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'first',
              'enum' => 
              array (
                0 => 'first',
                1 => 'mixed',
              ),
            ),
          ),
          11 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          12 => 
          array (
            'description' => 'Desired number of items per page.',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          13 => 
          array (
            'name' => 'sort',
            'description' => 'Sort the results.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
              'enum' => 
              array (
                0 => 'dateLastComment',
                1 => 'dateInserted',
                2 => 'discussionID',
                3 => '-dateLastComment',
                4 => '-dateInserted',
                5 => '-discussionID',
              ),
            ),
          ),
          14 => 
          array (
            'description' => 'Filter by author.
',
            'in' => 'query',
            'name' => 'insertUserID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'x-filter' => 
            array (
              'field' => 'd.InsertUserID',
            ),
          ),
          15 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
          16 => 
          array (
            'description' => 'The group the discussion is in.',
            'in' => 'query',
            'name' => 'groupID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'allowEmptyValue' => true,
          ),
          17 => 
          array (
            'description' => 'Filter by resolved status.',
            'in' => 'query',
            'name' => 'resolved',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
            'x-filter' => 
            array (
              'field' => 'd.Resolved',
            ),
          ),
          18 => 
          array (
            'name' => 'reactionType',
            'description' => 'Get all discussions you have given a specific reaction to.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Discussion',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'List discussions.',
        'x-addon' => 'reactions',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DiscussionPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/bookmarked' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          1 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Discussion',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Get a list of the current user\'s bookmarked discussions.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/idea' => 
    array (
      'x-addon' => 'ideation',
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DiscussionPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add an idea.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/question' => 
    array (
      'x-addon' => 'qna',
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DiscussionPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/search' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The numeric ID of a category to limit search results to.
',
            'in' => 'query',
            'name' => 'categoryID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Limit results to those in followed categories. Cannot be used with the categoryID parameter.
',
            'in' => 'query',
            'name' => 'followed',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
          ),
          2 => 
          array (
            'description' => 'Search terms.
',
            'in' => 'query',
            'name' => 'query',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
          3 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          4 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          5 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Discussion',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Search discussions.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Delete a discussion.',
        'x-addon' => 'vanilla',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Get a discussion.',
        'x-addon' => 'vanilla',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DiscussionPatch',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/bookmark' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'bookmarked' => 
                    array (
                      'description' => 'The current bookmark value.',
                      'type' => 'boolean',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'bookmarked',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'bookmarked' => 
                  array (
                    'description' => 'Pass true to bookmark or false to remove bookmark.',
                    'type' => 'boolean',
                  ),
                ),
                'required' => 
                array (
                  0 => 'bookmarked',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Bookmark a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/canonical-url' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'Not Found',
          ),
          400 => 
          array (
            'description' => 'Bad Request',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'canonicalUrl' => 
                  array (
                    'description' => 'Canonical url for discussion.',
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'canonicalUrl',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Set custom canonical url for a discussion.',
        'x-addon' => 'vanilla',
      ),
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'Not Found',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Remove custom canonical url for a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/DiscussionGetEdit',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Get a discussion for editing.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/idea' => 
    array (
      'x-addon' => 'ideation',
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'statusID' => 
                    array (
                      'description' => 'Idea status ID.',
                      'type' => 'integer',
                    ),
                    'statusNotes' => 
                    array (
                      'description' => 'Notes on a status change. Notes will persist until overwritten.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'statusID',
                    1 => 'statusNotes',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'statusID' => 
                  array (
                    'description' => 'Idea status ID.',
                    'type' => 'integer',
                  ),
                  'statusNotes' => 
                  array (
                    'description' => 'Notes on a status change. Notes will persist until overwritten.',
                    'minLength' => 1,
                    'nullable' => true,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'statusID',
                  1 => 'statusNotes',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update idea metadata on a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/quote' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'bodyRaw' => 
                    array (
                      'description' => 'The raw body of the discussion. This can be an array of rich operations or a string for other formats',
                      'minLength' => 1,
                      'type' => 'string',
                      'x-todo' => 'tpye: array',
                    ),
                    'dateInserted' => 
                    array (
                      'description' => 'When the discussion was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'dateUpdated' => 
                    array (
                      'description' => 'When the discussion was last updated.',
                      'format' => 'date-time',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'discussionID' => 
                    array (
                      'description' => 'The ID of the discussion.',
                      'type' => 'integer',
                    ),
                    'format' => 
                    array (
                      '$ref' => '#/components/schemas/Format',
                    ),
                    'insertUser' => 
                    array (
                      '$ref' => '#/components/schemas/UserFragment',
                    ),
                    'name' => 
                    array (
                      'description' => 'The title of the discussion',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The full URL to the discussion.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'discussionID',
                    1 => 'name',
                    2 => 'bodyRaw',
                    3 => 'dateInserted',
                    4 => 'dateUpdated',
                    5 => 'insertUser',
                    6 => 'url',
                    7 => 'format',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/reactions' => 
    array (
      'x-addon' => 'reactions',
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter to a specific reaction type by using its URL code.',
            'in' => 'query',
            'name' => 'type',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
            'allowEmptyValue' => true,
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'dateInserted' => 
                      array (
                        'format' => 'date-time',
                        'type' => 'string',
                      ),
                      'reactionType' => 
                      array (
                        'properties' => 
                        array (
                          'class' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                          'name' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                          'tagID' => 
                          array (
                            'type' => 'integer',
                          ),
                          'urlcode' => 
                          array (
                            'minLength' => 1,
                            'type' => 'string',
                          ),
                        ),
                        'required' => 
                        array (
                          0 => 'tagID',
                          1 => 'urlcode',
                          2 => 'name',
                          3 => 'class',
                        ),
                        'type' => 'object',
                      ),
                      'recordID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'recordType' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'tagID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'user' => 
                      array (
                        '$ref' => '#/components/schemas/UserFragment',
                      ),
                      'userID' => 
                      array (
                        'type' => 'integer',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'recordType',
                      1 => 'recordID',
                      2 => 'tagID',
                      3 => 'userID',
                      4 => 'dateInserted',
                      5 => 'user',
                      6 => 'reactionType',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Get reactions to a discussion.',
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'class' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'count' => 
                      array (
                        'type' => 'integer',
                      ),
                      'name' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'tagID' => 
                      array (
                        'type' => 'integer',
                      ),
                      'urlcode' => 
                      array (
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'tagID',
                      1 => 'urlcode',
                      2 => 'name',
                      3 => 'class',
                      4 => 'count',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'reactionType' => 
                  array (
                    'description' => 'URL code of a reaction type.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'reactionType',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'React to a discussion.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/reactions/' => 
    array (
      'x-addon' => 'reactions',
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Remove user\'s own reaction.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/reactions/{userID}' => 
    array (
      'x-addon' => 'reactions',
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'The target user ID.
',
            'in' => 'path',
            'name' => 'userID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/DiscussionExpand',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'summary' => 'Remove a user\'s reaction.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/tags' => 
    array (
      'parameters' => 
      array (
        0 => 
        array (
          'name' => 'id',
          'description' => 'The discussion ID',
          'in' => 'path',
          'required' => true,
          'schema' => 
          array (
            'type' => 'integer',
          ),
          'x-addon' => 'vanilla',
        ),
      ),
      'put' => 
      array (
        'summary' => 'Set the tags on a discussion.',
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/TagReference',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/TagFragment',
                  ),
                ),
              ),
            ),
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'summary' => 'Add tags to a discussion.',
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/TagReference',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/TagFragment',
                  ),
                ),
              ),
            ),
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/discussions/{id}/type' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The discussion ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Discussion',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Discussions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'type' => 
                  array (
                    'description' => 'record type to convert to.',
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'recordType',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Change a discussion type.',
        'x-addon' => 'vanilla',
      ),
    ),
    '/drafts' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Filter drafts by record type.
',
            'in' => 'query',
            'name' => 'recordType',
            'schema' => 
            array (
              'enum' => 
              array (
                0 => 'comment',
                1 => 'discussion',
              ),
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter by the unique ID of the parent for a draft. Used with recordType.
',
            'in' => 'query',
            'name' => 'parentRecordID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'allowEmptyValue' => true,
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    'properties' => 
                    array (
                      'attributes' => 
                      array (
                        'description' => 'A free-form object containing all custom data for this draft.',
                        'type' => 'object',
                      ),
                      'dateInserted' => 
                      array (
                        'description' => 'When the draft was created.',
                        'format' => 'date-time',
                        'type' => 'string',
                      ),
                      'dateUpdated' => 
                      array (
                        'description' => 'When the draft was updated.',
                        'format' => 'date-time',
                        'nullable' => true,
                        'type' => 'string',
                      ),
                      'draftID' => 
                      array (
                        'description' => 'The unique ID of the draft.',
                        'type' => 'integer',
                      ),
                      'insertUserID' => 
                      array (
                        'description' => 'The unique ID of the user who created this draft.',
                        'type' => 'integer',
                      ),
                      'parentRecordID' => 
                      array (
                        'description' => 'The unique ID of the intended parent to this record.',
                        'nullable' => true,
                        'type' => 'integer',
                      ),
                      'recordType' => 
                      array (
                        'description' => 'The type of record associated with this draft.',
                        'enum' => 
                        array (
                          0 => 'comment',
                          1 => 'discussion',
                        ),
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'updateUserID' => 
                      array (
                        'description' => 'The unique ID of the user who updated this draft.',
                        'nullable' => true,
                        'type' => 'integer',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'draftID',
                      1 => 'recordType',
                      2 => 'parentRecordID',
                      3 => 'attributes',
                      4 => 'insertUserID',
                      5 => 'dateInserted',
                      6 => 'updateUserID',
                      7 => 'dateUpdated',
                    ),
                    'type' => 'object',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'summary' => 'List drafts created by the current user.',
        'x-addon' => 'dashboard',
      ),
      'post' => 
      array (
        'summary' => 'Create a draft.',
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'attributes' => 
                    array (
                      'description' => 'A free-form object containing all custom data for this draft.
',
                      'type' => 'object',
                    ),
                    'dateInserted' => 
                    array (
                      'description' => 'When the draft was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'dateUpdated' => 
                    array (
                      'description' => 'When the draft was updated.',
                      'format' => 'date-time',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'draftID' => 
                    array (
                      'description' => 'The unique ID of the draft.',
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The unique ID of the user who created this draft.',
                      'type' => 'integer',
                    ),
                    'parentRecordID' => 
                    array (
                      'description' => 'The unique ID of the intended parent to this record.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'recordType' => 
                    array (
                      'description' => 'The type of record associated with this draft.',
                      'enum' => 
                      array (
                        0 => 'comment',
                        1 => 'discussion',
                      ),
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'updateUserID' => 
                    array (
                      'description' => 'The unique ID of the user who updated this draft.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'draftID',
                    1 => 'recordType',
                    2 => 'parentRecordID',
                    3 => 'attributes',
                    4 => 'insertUserID',
                    5 => 'dateInserted',
                    6 => 'updateUserID',
                    7 => 'dateUpdated',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DraftPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/drafts/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The draft ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'summary' => 'Delete a draft.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The draft ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'attributes' => 
                    array (
                      'description' => 'A free-form object containing all custom data for this draft.
',
                      'type' => 'object',
                    ),
                    'dateInserted' => 
                    array (
                      'description' => 'When the draft was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'dateUpdated' => 
                    array (
                      'description' => 'When the draft was updated.',
                      'format' => 'date-time',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'draftID' => 
                    array (
                      'description' => 'The unique ID of the draft.',
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The unique ID of the user who created this draft.',
                      'type' => 'integer',
                    ),
                    'parentRecordID' => 
                    array (
                      'description' => 'The unique ID of the intended parent to this record.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'recordType' => 
                    array (
                      'description' => 'The type of record associated with this draft.',
                      'enum' => 
                      array (
                        0 => 'comment',
                        1 => 'discussion',
                      ),
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'updateUserID' => 
                    array (
                      'description' => 'The unique ID of the user who updated this draft.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'draftID',
                    1 => 'recordType',
                    2 => 'parentRecordID',
                    3 => 'attributes',
                    4 => 'insertUserID',
                    5 => 'dateInserted',
                    6 => 'updateUserID',
                    7 => 'dateUpdated',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'summary' => 'Get a draft.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The draft ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'attributes' => 
                    array (
                      'description' => 'A free-form object containing all custom data for this draft.
',
                      'type' => 'object',
                    ),
                    'draftID' => 
                    array (
                      'description' => 'The unique ID of the draft.',
                      'type' => 'integer',
                    ),
                    'parentRecordID' => 
                    array (
                      'description' => 'The unique ID of the intended parent to this record.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'draftID',
                    1 => 'parentRecordID',
                    2 => 'attributes',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/DraftPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a draft.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/drafts/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The draft ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'attributes' => 
                    array (
                      'description' => 'A free-form object containing all custom data for this draft.
',
                      'type' => 'object',
                    ),
                    'draftID' => 
                    array (
                      'description' => 'The unique ID of the draft.',
                      'type' => 'integer',
                    ),
                    'parentRecordID' => 
                    array (
                      'description' => 'The unique ID of the intended parent to this record.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'draftID',
                    1 => 'parentRecordID',
                    2 => 'attributes',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Drafts',
        ),
        'summary' => 'Get a draft for editing.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/locales' => 
    array (
      'get' => 
      array (
        'summary' => 'Get all enabled locales on the site.',
        'tags' => 
        array (
          0 => 'Locales',
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'A list of enabled locales',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Locale',
                  ),
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/locales/translations/{locale}' => 
    array (
      'get' => 
      array (
        'summary' => 'Get all of the application\'s translation strings.',
        'tags' => 
        array (
          0 => 'Translations',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/LocaleCodeParameter',
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/CacheBusterParameter',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'description' => 'A map of string keys to translation strings.',
                  'type' => 'object',
                  'additionalProperties' => 
                  array (
                    'type' => 'string',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/locales/translations/{locale}.js' => 
    array (
      'get' => 
      array (
        'summary' => 'Get the application\'s translations strings as a javascrpt file.',
        'description' => 'This endpoint is intended for application optimization where translations are requested within a `<script>` tag
rather than as an external API call.
',
        'tags' => 
        array (
          0 => 'Translations',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/LocaleCodeParameter',
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/CacheBusterParameter',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/javascript' => 
              array (
                'schema' => 
                array (
                  'type' => 'string',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/media' => 
    array (
      'post' => 
      array (
        'summary' => 'Upload a media file.',
        'tags' => 
        array (
          0 => 'Media',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'multipart/form-data' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'file' => 
                  array (
                    '$ref' => '#/components/schemas/UploadedFile',
                  ),
                ),
                'required' => 
                array (
                  0 => 'file',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          201 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'dateInserted' => 
                    array (
                      'description' => 'When the media item was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'foreignID' => 
                    array (
                      'description' => 'The ID of the table.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'foreignType' => 
                    array (
                      'description' => 'Table the media is linked to.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'Image height',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The user that created the media item.',
                      'type' => 'integer',
                    ),
                    'mediaID' => 
                    array (
                      'description' => 'The ID of the record.',
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The original filename of the upload.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'size' => 
                    array (
                      'description' => 'File size in bytes',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'MIME type',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL of the file.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'Image width',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'mediaID',
                    1 => 'url',
                    2 => 'name',
                    3 => 'type',
                    4 => 'size',
                    5 => 'dateInserted',
                    6 => 'insertUserID',
                    7 => 'foreignType',
                    8 => 'foreignID',
                  ),
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/media/by-url' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Full URL to the item.
',
            'in' => 'query',
            'name' => 'url',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'summary' => 'Delete a media item, using its URL.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Full URL to the item.
',
            'in' => 'query',
            'name' => 'url',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'dateInserted' => 
                    array (
                      'description' => 'When the media item was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'foreignID' => 
                    array (
                      'description' => 'The ID of the table.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'foreignType' => 
                    array (
                      'description' => 'Table the media is linked to.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'Image height',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The user that created the media item.',
                      'type' => 'integer',
                    ),
                    'mediaID' => 
                    array (
                      'description' => 'The ID of the record.',
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The original filename of the upload.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'size' => 
                    array (
                      'description' => 'File size in bytes',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'MIME type',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL of the file.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'Image width',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'mediaID',
                    1 => 'url',
                    2 => 'name',
                    3 => 'type',
                    4 => 'size',
                    5 => 'dateInserted',
                    6 => 'insertUserID',
                    7 => 'foreignType',
                    8 => 'foreignID',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'summary' => 'Get a media item, using its URL.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/media/scrape' => 
    array (
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'attributes' => 
                    array (
                      'description' => 'Any additional attributes required by the the specific embed.',
                      'nullable' => true,
                      'type' => 'object',
                    ),
                    'body' => 
                    array (
                      'description' => 'A paragraph summarizing the content, if any. This is not what is what gets rendered to the page.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'The height of the image/video/etc. if applicable. This may be the photoUrl, but might exist even when there is no photoUrl in the case of a video without preview image.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The title of the page/item/etc. if any.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'photoUrl' => 
                    array (
                      'description' => 'A photo that goes with the content.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'type' => 
                    array (
                      'description' => 'The type of site. This determines how the embed is rendered.',
                      'enum' => 
                      array (
                        0 => 'quote',
                        1 => 'twitter',
                        2 => 'youtube',
                        3 => 'vimeo',
                        4 => 'instagram',
                        5 => 'soundcloud',
                        6 => 'imgur',
                        7 => 'twitch',
                        8 => 'getty',
                        9 => 'giphy',
                        10 => 'wistia',
                        11 => 'codepen',
                        12 => 'image',
                        13 => 'link',
                      ),
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL that was scraped.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'The width of the image/video/etc. if applicable.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'url',
                    1 => 'type',
                    2 => 'name',
                    3 => 'body',
                    4 => 'photoUrl',
                    5 => 'height',
                    6 => 'width',
                    7 => 'attributes',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'force' => 
                  array (
                    'default' => false,
                    'description' => 'Force the scrape even if the result is cached.',
                    'type' => 'boolean',
                  ),
                  'url' => 
                  array (
                    'description' => 'The URL to scrape.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'url',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Scrape information from a URL.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/media/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The media ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'dateInserted' => 
                    array (
                      'description' => 'When the media item was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'foreignID' => 
                    array (
                      'description' => 'The ID of the table.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'foreignType' => 
                    array (
                      'description' => 'Table the media is linked to.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'Image height',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The user that created the media item.',
                      'type' => 'integer',
                    ),
                    'mediaID' => 
                    array (
                      'description' => 'The ID of the record.',
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The original filename of the upload.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'size' => 
                    array (
                      'description' => 'File size in bytes',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'MIME type',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL of the file.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'Image width',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'mediaID',
                    1 => 'url',
                    2 => 'name',
                    3 => 'type',
                    4 => 'size',
                    5 => 'dateInserted',
                    6 => 'insertUserID',
                    7 => 'foreignType',
                    8 => 'foreignID',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'summary' => 'Delete a media item.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The media ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'dateInserted' => 
                    array (
                      'description' => 'When the media item was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'foreignID' => 
                    array (
                      'description' => 'The ID of the table.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'foreignType' => 
                    array (
                      'description' => 'Table the media is linked to.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'Image height',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The user that created the media item.',
                      'type' => 'integer',
                    ),
                    'mediaID' => 
                    array (
                      'description' => 'The ID of the record.',
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The original filename of the upload.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'size' => 
                    array (
                      'description' => 'File size in bytes',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'MIME type',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL of the file.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'Image width',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'mediaID',
                    1 => 'url',
                    2 => 'name',
                    3 => 'type',
                    4 => 'size',
                    5 => 'dateInserted',
                    6 => 'insertUserID',
                    7 => 'foreignType',
                    8 => 'foreignID',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'summary' => 'Get a media item.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/media/{id}/attachment' => 
    array (
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The media ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'dateInserted' => 
                    array (
                      'description' => 'When the media item was created.',
                      'format' => 'date-time',
                      'type' => 'string',
                    ),
                    'foreignID' => 
                    array (
                      'description' => 'The ID of the table.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'foreignType' => 
                    array (
                      'description' => 'Table the media is linked to.',
                      'minLength' => 1,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'height' => 
                    array (
                      'description' => 'Image height',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                    'insertUserID' => 
                    array (
                      'description' => 'The user that created the media item.',
                      'type' => 'integer',
                    ),
                    'mediaID' => 
                    array (
                      'description' => 'The ID of the record.',
                      'type' => 'integer',
                    ),
                    'name' => 
                    array (
                      'description' => 'The original filename of the upload.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'size' => 
                    array (
                      'description' => 'File size in bytes',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'MIME type',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'url' => 
                    array (
                      'description' => 'The URL of the file.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'width' => 
                    array (
                      'description' => 'Image width',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'mediaID',
                    1 => 'url',
                    2 => 'name',
                    3 => 'type',
                    4 => 'size',
                    5 => 'dateInserted',
                    6 => 'insertUserID',
                    7 => 'foreignType',
                    8 => 'foreignID',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Media',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/MediaItemPatch',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a media item\'s attachment to another record.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/messages' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Filter by conversation.
',
            'in' => 'query',
            'name' => 'conversationID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Filter by author.
',
            'in' => 'query',
            'name' => 'insertUserID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 50,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          4 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'insertUser',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Message',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Messages',
        ),
        'summary' => 'List user messages.',
        'x-addon' => 'conversations',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Message',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Messages',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/MessagePost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add a message.',
        'x-addon' => 'conversations',
      ),
    ),
    '/messages/{id}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The message ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Message',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Messages',
        ),
        'summary' => 'Get a message.',
        'x-addon' => 'conversations',
      ),
    ),
    '/notifications' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/NotificationSchema',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Notifications',
        ),
        'summary' => 'List notifications for the current user.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'summary' => 'Mark all notifications read.',
        'tags' => 
        array (
          0 => 'Notifications',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/NotificationPatchSchema',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/notifications/{id}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The notification ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/NotificationSchema',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Notifications',
        ),
        'summary' => 'Get a single notification.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'summary' => 'Mark a notification read.',
        'tags' => 
        array (
          0 => 'Notifications',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/NotificationPatchSchema',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/NotificationSchema',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/notifications/{id}/read' => 
    array (
      'put' => 
      array (
        'summary' => 'Mark a notification read.',
        'tags' => 
        array (
          0 => 'Notifications',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/NotificationSchema',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/pockets/' => 
    array (
      'x-hidden' => true,
      'get' => 
      array (
        'summary' => 'Get a list of pockets.',
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The expand.',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/fullPocketSchema',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'pockets',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/fullPocketSchema',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/PocketPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add a pocket.',
        'x-addon' => 'pockets',
      ),
    ),
    '/pockets/{id}' => 
    array (
      'x-hidden' => true,
      'get' => 
      array (
        'summary' => 'Get a pocket by id.',
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Pocket ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'The expand.',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/fullPocketSchema',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'pockets',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The pocket ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/PocketPatch',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/PocketPatch',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a pocket.',
        'x-addon' => 'pockets',
      ),
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The pocket ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'summary' => 'Delete a Pocket.',
        'x-addon' => 'pockets',
      ),
    ),
    '/pockets/{id}/edit' => 
    array (
      'x-hidden' => true,
      'get' => 
      array (
        'summary' => 'Get a pocket for editing.',
        'tags' => 
        array (
          0 => 'Pockets',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Pocket ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'The expand.',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'pocketID' => 
                    array (
                      'description' => 'The pocket ID',
                      'type' => 'integer',
                    ),
                    'widgetID' => 
                    array (
                      'description' => 'The widget type.',
                      'type' => 'string',
                    ),
                    'name' => 
                    array (
                      'description' => 'The name of the pocket',
                      'type' => 'string',
                    ),
                    'page' => 
                    array (
                      'description' => 'Which page to display the Pocket on.',
                      'type' => 'string',
                    ),
                    'body' => 
                    array (
                      'description' => 'The body of the pocket.',
                      'type' => 'string',
                    ),
                    'format' => 
                    array (
                      'enum' => 
                      array (
                        0 => 'raw',
                        1 => 'widget',
                      ),
                      'type' => 'string',
                    ),
                    'repeatType' => 
                    array (
                      'enum' => 
                      array (
                        0 => 'once',
                        1 => 'after',
                        2 => 'before',
                        3 => 'every',
                        4 => 'index',
                      ),
                      'type' => 'string',
                      'description' => 'Pocket repeat type.',
                    ),
                    'repeatIndexes' => 
                    array (
                      'description' => 'Repeat indexes',
                      'type' => 'integer',
                    ),
                    'mobileType' => 
                    array (
                      'enum' => 
                      array (
                        0 => 'only',
                        1 => 'never',
                        2 => 'default',
                      ),
                      'type' => 'string',
                      'description' => 'Whether or not pocket is active on mobile.',
                    ),
                    'sort' => 
                    array (
                      'description' => 'Pocket sort order.',
                      'type' => 'integer',
                      'default' => 0,
                    ),
                    'isEmbeddable' => 
                    array (
                      'description' => 'Pocket can be embedded.',
                      'type' => 'boolean',
                      'default' => false,
                    ),
                    'location' => 
                    array (
                      'description' => 'Location of the pocket on the page.',
                      'type' => 'string',
                    ),
                    'isAd' => 
                    array (
                      'description' => 'If the pocket is an ad.',
                      'type' => 'boolean',
                      'default' => false,
                    ),
                    'enabled' => 
                    array (
                      'description' => 'Pocket enabled/disabled.',
                      'type' => 'boolean',
                      'default' => false,
                    ),
                    'categoryID' => 
                    array (
                      'description' => 'Pocket active in this category.',
                      'type' => 'integer',
                    ),
                    'includeChildCategories' => 
                    array (
                      'description' => 'Incldue category child categories.',
                      'type' => 'integer',
                    ),
                    'roleIDs' => 
                    array (
                      'description' => 'RoleIDs that the pocket will apply to.',
                      'type' => 'array',
                      'items' => 
                      array (
                        'type' => 'integer',
                      ),
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'pocketID',
                    1 => 'name',
                    2 => 'repeatType',
                    3 => 'body',
                    4 => 'format',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'pockets',
      ),
    ),
    '/reactions' => 
    array (
      'get' => 
      array (
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/ReactionType',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Reactions',
        ),
        'summary' => 'Get a list of reaction types.',
        'x-addon' => 'reactions',
      ),
    ),
    '/reactions/edit/{urlCode}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'urlCode',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'The reaction type ID.
',
            'in' => 'query',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ReactionType',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Reactions',
        ),
        'summary' => 'Get a reaction type for editing.',
        'x-addon' => 'reactions',
      ),
    ),
    '/reactions/{urlCode}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'urlCode',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'The reaction type ID.
',
            'in' => 'query',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ReactionType',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Reactions',
        ),
        'summary' => 'Get a single reaction type.',
        'x-addon' => 'reactions',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'urlCode',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ReactionType',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Reactions',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/ReactionType',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a reaction type.',
        'x-addon' => 'reactions',
      ),
    ),
    '/resources' => 
    array (
      'get' => 
      array (
        'summary' => 'List the resources available on the site.',
        'tags' => 
        array (
          0 => 'Resources',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'crawlable',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'boolean',
              'description' => 'Filter by resources that have crawling information.',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/ResourceFragment',
                  ),
                ),
                'example' => 
                array (
                  0 => 
                  array (
                    'recordType' => 'user',
                    'url' => 'https://example.com/api/v2/resources/user',
                    'crawlable' => true,
                  ),
                  1 => 
                  array (
                    'recordType' => 'discussion',
                    'url' => 'https://example.com/api/v2/resources/discussion',
                    'crawlable' => true,
                  ),
                  2 => 
                  array (
                    'recordType' => 'comment',
                    'url' => 'https://example.com/api/v2/resources/comment',
                    'crawlable' => true,
                  ),
                ),
              ),
            ),
          ),
          403 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/resources/dirty-records/{recordType}' => 
    array (
      'delete' => 
      array (
        'summary' => 'Delete resources dirty records.',
        'tags' => 
        array (
          0 => 'Resources',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'recordType',
            'description' => 'The record type slug of the resource.',
            'in' => 'path',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
              'example' => 'discussion',
            ),
          ),
          1 => 
          array (
            'name' => 'dateInserted',
            'description' => 'Date to filter records by.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/resources/{recordType}' => 
    array (
      'get' => 
      array (
        'summary' => 'Get the details of a resource.',
        'tags' => 
        array (
          0 => 'Resources',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'recordType',
            'description' => 'The record type slug of the resource.',
            'in' => 'path',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
              'example' => 'discussion',
            ),
          ),
          1 => 
          array (
            'name' => 'expand',
            'description' => 'Expand fields on the result.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'string',
                'enum' => 
                array (
                  0 => 'all',
                  1 => 'crawl',
                ),
              ),
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Resource',
                ),
              ),
            ),
          ),
          403 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/rich/quote' => 
    array (
      'post' => 
      array (
        'summary' => 'Create a rich-compatible HTML representation of a string for quoting.',
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'body' => 
                  array (
                    'description' => 'Raw post text to render as a rich post quote.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                  'format' => 
                  array (
                    '$ref' => '#/components/schemas/Format',
                  ),
                ),
                'required' => 
                array (
                  0 => 'body',
                  1 => 'format',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'quote' => 
                    array (
                      'description' => 'A quoted representation of the text, rendered as HTML.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'quote',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Rich',
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/role-requests/applications' => 
    array (
      'post' => 
      array (
        'summary' => 'Apply to a role.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'type' => 'object',
                'properties' => 
                array (
                  'roleID' => 
                  array (
                    'type' => 'integer',
                    'description' => 'The role to apply to.',
                  ),
                  'attributes' => 
                  array (
                    'type' => 'object',
                    'description' => 'Custom fields for the request.',
                  ),
                ),
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          201 => 
          array (
            'description' => 'Created.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/RoleApplication',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'summary' => 'List role applicants.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'roleID',
            'in' => 'query',
            'description' => 'Filter by role.',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'name' => 'status',
            'in' => 'query',
            'description' => 'Filter by status.',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RoleRequestStatus',
            ),
          ),
          2 => 
          array (
            'name' => 'userID',
            'in' => 'query',
            'description' => 'Filter by user.',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          3 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          4 => 
          array (
            '$ref' => '#/components/parameters/Offset',
          ),
          5 => 
          array (
            'name' => 'limit',
            'in' => 'query',
            'description' => 'Desired number of items per page.',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          6 => 
          array (
            'name' => 'sort',
            'in' => 'query',
            'description' => 'Sort the results.',
            'schema' => 
            array (
              'type' => 'string',
              'enum' => 
              array (
                0 => 'dateInserted',
                1 => '-dateInserted',
              ),
            ),
          ),
          7 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'OK.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    'allOf' => 
                    array (
                      0 => 
                      array (
                        '$ref' => '#/components/schemas/RoleApplication',
                      ),
                      1 => 
                      array (
                        '$ref' => '#/components/schemas/RoleRequestExpansions',
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/role-requests/applications/{id}' => 
    array (
      'get' => 
      array (
        'summary' => 'Get a single application.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestID',
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'allOf' => 
                  array (
                    0 => 
                    array (
                      '$ref' => '#/components/schemas/RoleApplication',
                    ),
                    1 => 
                    array (
                      '$ref' => '#/components/schemas/RoleRequestExpansions',
                    ),
                  ),
                ),
              ),
            ),
          ),
          403 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'summary' => 'Update a role application.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestID',
          ),
          1 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestExpand',
          ),
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'type' => 'object',
                'properties' => 
                array (
                  'status' => 
                  array (
                    '$ref' => '#/components/schemas/RoleRequestStatus',
                  ),
                ),
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Updated.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/RoleApplication',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'delete' => 
      array (
        'summary' => 'Delete a role application.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/RoleRequestID',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/role-requests/metas' => 
    array (
      'put' => 
      array (
        'summary' => 'Add or update a role request meta definition.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'type' => 'object',
                'properties' => 
                array (
                  'roleID' => 
                  array (
                    'type' => 'integer',
                    'description' => 'The role to apply to.',
                  ),
                  'type' => 
                  array (
                    '$ref' => '#/components/schemas/RoleRequestType',
                  ),
                  'name' => 
                  array (
                    'type' => 'string',
                    'description' => 'The title displayed to to the user.',
                  ),
                  'body' => 
                  array (
                    'type' => 'string',
                    'description' => 'The default text to display to the user.',
                  ),
                  'format' => 
                  array (
                    '$ref' => '#/components/schemas/Format',
                  ),
                  'attributesSchema' => 
                  array (
                    '$ref' => '#/components/schemas/BasicSchema',
                  ),
                ),
                'required' => 
                array (
                  0 => 'roleID',
                  1 => 'type',
                  2 => 'body',
                  3 => 'format',
                  4 => 'attributesSchema',
                ),
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/RoleRequestMeta',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'summary' => 'List role request meta information',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'type',
            'in' => 'query',
            'description' => 'Filter on type.',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RoleRequestType',
            ),
          ),
          1 => 
          array (
            'name' => 'roleID',
            'in' => 'query',
            'description' => 'Filter on role.',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          2 => 
          array (
            'name' => 'hasRole',
            'in' => 'query',
            'description' => 'Filter on roles the user already has or doesn\'t have.',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
          ),
          3 => 
          array (
            'name' => 'expand',
            'in' => 'query',
            'description' => 'Expand the result.',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'string',
                'enum' => 
                array (
                  0 => 'role',
                  1 => 'roleRequest',
                ),
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/RoleRequestMeta',
                  ),
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/role-requests/metas/{type}/{roleID}' => 
    array (
      'get' => 
      array (
        'summary' => 'Get a meta item.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'type',
            'in' => 'path',
            'required' => true,
            'description' => 'The type of request.',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RoleRequestType',
            ),
          ),
          1 => 
          array (
            'name' => 'roleID',
            'in' => 'path',
            'required' => true,
            'description' => 'The role ID.',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/RoleRequestMeta',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'delete' => 
      array (
        'summary' => 'Delete a meta item.',
        'tags' => 
        array (
          0 => 'Role Requests',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'type',
            'in' => 'path',
            'required' => true,
            'description' => 'The type of request.',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RoleRequestType',
            ),
          ),
          1 => 
          array (
            'name' => 'roleID',
            'in' => 'path',
            'required' => true,
            'description' => 'The role ID.',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success.',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/roles' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'permissions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Role',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'summary' => 'List roles.',
        'x-addon' => 'dashboard',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Role',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'requestBody' => 
        array (
          '$ref' => '#/components/requestBodies/RolePost',
        ),
        'summary' => 'Add a role.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/roles/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The role ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'summary' => 'Delete a role.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The role ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'permissions',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Role',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'summary' => 'Get a role.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The role ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Role',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/RolePost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a role.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/roles/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The role ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'canSession' => 
                    array (
                      'description' => 'Can users in this role start a session?',
                      'type' => 'boolean',
                    ),
                    'deletable' => 
                    array (
                      'description' => 'Is the role deletable?',
                      'type' => 'boolean',
                    ),
                    'description' => 
                    array (
                      'description' => 'Description of the role.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'name' => 
                    array (
                      'description' => 'Name of the role.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'personalInfo' => 
                    array (
                      'description' => 'Is membership in this role personal information?',
                      'type' => 'boolean',
                    ),
                    'roleID' => 
                    array (
                      'description' => 'ID of the role.',
                      'type' => 'integer',
                    ),
                    'type' => 
                    array (
                      'description' => 'Default type of this role.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'roleID',
                    1 => 'name',
                    2 => 'description',
                    3 => 'type',
                    4 => 'deletable',
                    5 => 'canSession',
                    6 => 'personalInfo',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'summary' => 'Get a role for editing.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/roles/{id}/permissions' => 
    array (
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/PermissionFragment',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'items' => 
                array (
                  '$ref' => '#/components/schemas/PermissionFragment',
                ),
                'type' => 'array',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update permissions on a role',
        'x-addon' => 'dashboard',
      ),
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/PermissionFragment',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Roles',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'items' => 
                array (
                  '$ref' => '#/components/schemas/PermissionFragment',
                ),
                'type' => 'array',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Overwrite all permissions for a role.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/search' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Filter the records using the supplied terms.
',
            'in' => 'query',
            'name' => 'query',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-search-filter' => true,
          ),
          1 => 
          array (
            'description' => 'Restrict the search to the specified main type(s) of records.
',
            'in' => 'query',
            'name' => 'recordTypes',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'article',
                  1 => 'discussion',
                  2 => 'comment',
                  3 => 'group',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
          2 => 
          array (
            'description' => 'Restrict the search to the specified type(s) of records.
',
            'in' => 'query',
            'name' => 'types',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'article',
                  1 => 'discussion',
                  2 => 'comment',
                  3 => 'question',
                  4 => 'answer',
                  5 => 'group',
                  6 => 'poll',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
          3 => 
          array (
            'description' => 'Set the scope of the search to the comments of a discussion. Incompatible with recordType and type.
',
            'in' => 'query',
            'name' => 'discussionID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'x-search-scope' => true,
          ),
          4 => 
          array (
            'description' => 'Set the scope of the search to a specific category.
',
            'in' => 'query',
            'name' => 'categoryID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'x-search-scope' => true,
          ),
          5 => 
          array (
            'description' => 'Set the scope of the search to followed categories only.
',
            'in' => 'query',
            'name' => 'followedCategories',
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
            'x-search-scope' => true,
          ),
          6 => 
          array (
            'description' => 'Search the specified category\'s subtree. Works with categoryID
',
            'in' => 'query',
            'name' => 'includeChildCategories',
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          7 => 
          array (
            'description' => 'Allow search in archived categories.
',
            'in' => 'query',
            'name' => 'includeArchivedCategories',
            'schema' => 
            array (
              'default' => false,
              'type' => 'boolean',
            ),
          ),
          8 => 
          array (
            'description' => 'Filter the records by KnowledgeBase ID
',
            'in' => 'query',
            'name' => 'knowledgeBaseID',
            'schema' => 
            array (
              'type' => 'integer',
            ),
            'x-search-scope' => true,
          ),
          9 => 
          array (
            'description' => 'Filter the records by KnowledgeCategory ID
',
            'in' => 'query',
            'name' => 'knowledgeCategoryIDs',
            'schema' => 
            array (
              'items' => 
              array (
                'type' => 'integer',
              ),
              'type' => 'array',
            ),
            'x-search-scope' => true,
          ),
          10 => 
          array (
            'description' => 'Filter the records by matching part of their name.
',
            'in' => 'query',
            'name' => 'name',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-search-filter' => true,
          ),
          11 => 
          array (
            'description' => 'Filter the records by their featured status.
',
            'in' => 'query',
            'name' => 'featured',
            'schema' => 
            array (
              'type' => 'boolean',
            ),
            'x-search-filter' => true,
          ),
          12 => 
          array (
            'description' => 'Filter the records by their locale.
',
            'in' => 'query',
            'name' => 'locale',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-search-filter' => true,
          ),
          13 => 
          array (
            'description' => 'Filter the records by their site-section-group.
',
            'in' => 'query',
            'name' => 'siteSiteSectionGroup',
            'schema' => 
            array (
              'type' => 'string',
            ),
            'x-search-filter' => true,
          ),
          14 => 
          array (
            'description' => 'Filter the records by inserted user names.
',
            'in' => 'query',
            'name' => 'insertUserNames',
            'schema' => 
            array (
              'items' => 
              array (
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
            'x-search-filter' => true,
          ),
          15 => 
          array (
            'description' => 'Filter the records by inserted userIDs.
',
            'in' => 'query',
            'name' => 'insertUserIDs',
            'schema' => 
            array (
              'items' => 
              array (
                'type' => 'integer',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
            'x-search-filter' => true,
          ),
          16 => 
          array (
            '$ref' => '#/components/parameters/DateInserted',
          ),
          17 => 
          array (
            'description' => 'Filter discussions by matching tags.
',
            'in' => 'query',
            'name' => 'tags',
            'schema' => 
            array (
              'items' => 
              array (
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
            'x-search-filter' => true,
          ),
          18 => 
          array (
            'description' => 'Tags search condition.
Must be one of: "and", "or".
',
            'in' => 'query',
            'name' => 'tagOperator',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'or',
              'enum' => 
              array (
                0 => 'and',
                1 => 'or',
              ),
            ),
          ),
          19 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          20 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          21 => 
          array (
            'description' => 'Expand the results to include a rendered body field.
',
            'in' => 'query',
            'name' => 'expandBody',
            'schema' => 
            array (
              'type' => 'boolean',
              'default' => true,
            ),
          ),
          22 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'insertUser',
                  1 => 'breadcrumbs',
                  2 => 'excerpt',
                  3 => 'image',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/SearchResult',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Search',
        ),
        'summary' => 'Search for records matching specific criteria.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/tags' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'name' => 'query',
            'description' => 'The search term to find a tag.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'name' => 'type',
            'description' => 'The type of tags to list.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'string',
              ),
            ),
            'style' => 'form',
          ),
          2 => 
          array (
            'name' => 'parentID',
            'description' => 'ID of parent to filter results.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'array',
              'items' => 
              array (
                'type' => 'integer',
              ),
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/LegacyTag',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Tags',
        ),
        'summary' => 'List tags.',
        'x-addon' => 'vanilla',
      ),
      'post' => 
      array (
        'summary' => 'Add a tag.',
        'tags' => 
        array (
          0 => 'Tags',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/Tag',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Tag',
                ),
              ),
            ),
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/tags/{id}' => 
    array (
      'parameters' => 
      array (
        0 => 
        array (
          'name' => 'id',
          'description' => 'The ID of the tag.',
          'in' => 'path',
          'required' => true,
          'schema' => 
          array (
            'type' => 'integer',
          ),
          'x-addon' => 'vanilla',
        ),
      ),
      'get' => 
      array (
        'summary' => 'Get a single tag.',
        'tags' => 
        array (
          0 => 'Tags',
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Tag',
                ),
              ),
            ),
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'patch' => 
      array (
        'summary' => 'Update a tag.',
        'tags' => 
        array (
          0 => 'Tags',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/Tag',
              ),
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Tag',
                ),
              ),
            ),
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'delete' => 
      array (
        'summary' => 'Delete a tag.',
        'tags' => 
        array (
          0 => 'Tags',
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success.',
          ),
          401 => 
          array (
            '$ref' => '#/components/responses/PermissionError',
          ),
          404 => 
          array (
            '$ref' => '#/components/responses/NotFound',
          ),
          409 => 
          array (
            'description' => 'Tag isn\'t empty.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/BasicError',
                ),
              ),
            ),
          ),
        ),
        'x-addon' => 'vanilla',
      ),
    ),
    '/themes' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/ExpandAssetsParam',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Themes',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Themes',
        ),
        'summary' => 'Get all themes available.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/current' => 
    array (
      'get' => 
      array (
        'summary' => 'Get the current theme.',
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/ExpandAssetsParam',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Theme',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Themes',
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/parameters/ExpandAssetsParam',
          ),
          1 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Theme',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Themes',
        ),
        'summary' => 'Get a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/footer' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeFooterAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get the footer HTML of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/footer.html' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'text/html' => 
              array (
                'schema' => 
                array (
                  'type' => 'string',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get the footer HTML of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/header' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeHeaderAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get the header HTML of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/header.html' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'text/html' => 
              array (
                'schema' => 
                array (
                  'type' => 'string',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get the header HTML of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/javascript' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeJavascriptAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get JavaScript to be used as part of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/javascript.js' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/javascript' => 
              array (
                'schema' => 
                array (
                  'type' => 'string',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get JavaScript to be used as part of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/scripts' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeScriptsAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get additional script files of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/scripts.json' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/ThemeScript',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get additional script files of a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/styles' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeCssAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get theme styles.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/styles.css' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'text/css' => 
              array (
                'schema' => 
                array (
                  'type' => 'string',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get theme styles.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/variables' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ThemeVariablesAsset',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get theme variables.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/assets/variables.json' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique theme slug.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
          404 => 
          array (
            'description' => 'JavaScript could not be found.',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'description' => 
                    array (
                      'description' => 'Verbose description of the error.',
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'message' => 
                    array (
                      'description' => 'Short description of the error.',
                      'type' => 'string',
                    ),
                    'status' => 
                    array (
                      'description' => 'Status code of the error response.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'description',
                    1 => 'message',
                    2 => 'status',
                  ),
                ),
              ),
            ),
          ),
        ),
        'tags' => 
        array (
          0 => 'Theme Assets',
        ),
        'summary' => 'Get theme variables.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/themes/{themeID}/revisions' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Unique themeID.',
            'in' => 'path',
            'name' => 'themeID',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Theme',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Themes',
        ),
        'summary' => 'Get all revisions for a theme.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/tick' => 
    array (
      'post' => 
      array (
        'summary' => 'Register a page view for statistical and analytical purpose.',
        'responses' => 
        array (
          201 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Statistics',
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/tokens' => 
    array (
      'get' => 
      array (
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Token',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Tokens',
        ),
        'summary' => 'Get a list of access token IDs for the current user.',
        'x-addon' => 'dashboard',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Token',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Tokens',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/Token',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Issue a new access token for the current user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/tokens/oauth' => 
    array (
      'post' => 
      array (
        'summary' => 'Exchange an OAuth access token for a Vanilla access token.',
        'description' => 'This endpoint takes an access token from your OAuth 2 provider and exchanges it for a Vanilla access token. If no matching user exists in Vanilla, one will be created and an access token will be issued for them.

In order to use this endpoint, the access token calls out to the API defined in the OAuth connection so it must have a scope that has access to the user\'s profile or else it will fail.

The access tokens provided by this endpoint are relatively short lived, so make sure you take note of the `dateExpires` in the response and have a strategy for refreshing the access token for long lived client sessions.',
        'tags' => 
        array (
          0 => 'Tokens',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'type' => 'object',
                'properties' => 
                array (
                  'clientID' => 
                  array (
                    'type' => 'string',
                    'description' => 'Your OAuth client ID used to identify the specific OAuth connection.',
                  ),
                  'oauthAccessToken' => 
                  array (
                    'type' => 'string',
                    'description' => 'Your OAuth access token with scope to access the user\'s profile information.',
                  ),
                ),
                'required' => 
                array (
                  0 => 'clientID',
                  1 => 'oauthAccessToken',
                ),
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          200 => 
          array (
            'description' => 'Success',
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'object',
                  'properties' => 
                  array (
                    'accessToken' => 
                    array (
                      'type' => 'string',
                      'description' => 'The access token for Vanilla.',
                    ),
                    'dateExpires' => 
                    array (
                      'type' => 'string',
                      'format' => 'date-time',
                      'description' => 'The date/time that the access token will expire.',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'accessToken',
                    1 => 'dateExpires',
                  ),
                ),
                'example' => 
                array (
                  'accessToken' => 'va.MtVMRtHgB5Hipdb3HI86VqrdBlI4qWMz.QCw5cA.WkNNEV_',
                  'dateExpires' => '2019-08-30T14:37:52Z',
                ),
              ),
            ),
          ),
          400 => 
          array (
            'description' => 'The user profile was successfully fetched, but was malformed in some way.',
          ),
          403 => 
          array (
            'description' => 'Forbidden. The OAuth access token didn\'t have permission to access the user information or failed in some other way.

If you get this error then make sure that the profile endpoint on **your** site returns a profile the access tokens you are providing to this site.
',
          ),
          404 => 
          array (
            'description' => 'The provided client ID did not match the client ID that was configured within Vanilla.',
          ),
          500 => 
          array (
            'description' => 'OAuth is either not enabled or is not configured on Vanilla.',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/tokens/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The numeric ID of a token.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Tokens',
        ),
        'summary' => 'Revoke an access token.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The numeric ID of a token.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'A valid CSRF token for the current user.
',
            'in' => 'query',
            'name' => 'transientKey',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Token',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Tokens',
        ),
        'summary' => 'Reveal a usable access token.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'When the user was created.
This filter receive a string that can take two forms.
A single date that matches \'{Operator}{DateTime}\' where {Operator} can be =, &lt;, &gt;, &lt;=, &gt;=  and, if omitted, defaults to =.
A date range that matches \'{Opening}{DateTime},{DateTime}{Closing}\' where {Opening} can be \'[\' or \'(\' and {Closing} can be \']\' or \')\'. \'[]\' are inclusive and \'()\' are exclusive.
',
            'in' => 'query',
            'name' => 'dateInserted',
            'schema' => 
            array (
              'format' => 'date-filter',
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'When the user was updated.
This filter receive a string that can take two forms.
A single date that matches \'{Operator}{DateTime}\' where {Operator} can be =, &lt;, &gt;, &lt;=, &gt;=  and, if omitted, defaults to =.
A date range that matches \'{Opening}{DateTime},{DateTime}{Closing}\' where {Opening} can be \'[\' or \'(\' and {Closing} can be \']\' or \')\'. \'[]\' are inclusive and \'()\' are exclusive.
',
            'in' => 'query',
            'name' => 'dateUpdated',
            'schema' => 
            array (
              'format' => 'date-filter',
              'type' => 'string',
            ),
          ),
          2 => 
          array (
            'name' => 'dateLastActive',
            'description' => 'When the user was last active on the community.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'string',
              'format' => 'date-filter',
            ),
          ),
          3 => 
          array (
            'name' => 'roleID',
            'description' => 'Filter by the role ID of a user.',
            'in' => 'query',
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          4 => 
          array (
            'name' => 'userID',
            'description' => 'Filter by a range or CSV of user IDs.',
            'in' => 'query',
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RangeExpression',
            ),
          ),
          5 => 
          array (
            '$ref' => '#/components/parameters/Page',
          ),
          6 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 500,
              'minimum' => 1,
            ),
          ),
          7 => 
          array (
            'name' => 'sort',
            'in' => 'query',
            'description' => 'Sort the results.',
            'schema' => 
            array (
              'type' => 'string',
              'enum' => 
              array (
                0 => 'dateInserted',
                1 => 'dateLastActive',
                2 => 'name',
                3 => 'userID',
                4 => '-dateInserted',
                5 => '-dateLastActive',
                6 => '-name',
                7 => '-userID',
              ),
            ),
          ),
          8 => 
          array (
            '$ref' => '#/components/parameters/UserExpand',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/User',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'List users.',
        'x-addon' => 'dashboard',
      ),
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/User',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/UserPost',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Add a user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/by-names' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Filter for username. Supports full or partial matching with appended wildcard (e.g. User*).
',
            'in' => 'query',
            'name' => 'name',
            'required' => true,
            'schema' => 
            array (
              'minLength' => 1,
              'type' => 'string',
            ),
          ),
          1 => 
          array (
            'description' => 'Sort method for results.
Must be one of: "countComments", "dateLastActive", "name", "mention".
',
            'in' => 'query',
            'name' => 'order',
            'schema' => 
            array (
              'type' => 'string',
              'default' => 'name',
              'enum' => 
              array (
                0 => 'countComments',
                1 => 'dateLastActive',
                2 => 'name',
                3 => 'mention',
              ),
            ),
          ),
          2 => 
          array (
            'description' => 'Page number. See [Pagination](https://docs.vanillaforums.com/apiv2/#pagination).
',
            'in' => 'query',
            'name' => 'page',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 1,
              'minimum' => 1,
            ),
          ),
          3 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/UserFragment',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Search for users by full or partial name matching.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/me' => 
    array (
      'get' => 
      array (
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'allOf' => 
                  array (
                    0 => 
                    array (
                      '$ref' => '#/components/schemas/UserFragment',
                    ),
                    1 => 
                    array (
                      'type' => 'object',
                      'properties' => 
                      array (
                        'email' => 
                        array (
                          'description' => 'The current user\'s email address.',
                          'type' => 'string',
                          'format' => 'email',
                        ),
                        'ssoID' => 
                        array (
                          'description' => 'The unique ID of the default SSO connection. This will be YOUR user ID.',
                          'type' => 'string',
                        ),
                        'isAdmin' => 
                        array (
                          'description' => 'Whether or not the user is a global admin.',
                          'type' => 'boolean',
                        ),
                        'permissions' => 
                        array (
                          'description' => 'Global permissions available to the current user.',
                          'type' => 'array',
                          'items' => 
                          array (
                            'type' => 'string',
                          ),
                        ),
                        'countUnreadNotifications' => 
                        array (
                          'description' => 'Total number of unread notifications for the current user.',
                          'type' => 'integer',
                        ),
                        'countUnreadConversations' => 
                        array (
                          'description' => 'Total number of unread conversations for the current user.',
                          'type' => 'integer',
                        ),
                      ),
                      'required' => 
                      array (
                        0 => 'isAdmin',
                        1 => 'permissions',
                      ),
                    ),
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Get information about the current user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/me-counts' => 
    array (
      'get' => 
      array (
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'counts' => 
                    array (
                      'type' => 'array',
                      'items' => 
                      array (
                        'type' => 'object',
                        'properties' => 
                        array (
                          'name' => 
                          array (
                            'description' => 'Menu counter name',
                            'type' => 'string',
                          ),
                          'count' => 
                          array (
                            'description' => 'Counter value',
                            'type' => 'integer',
                          ),
                        ),
                      ),
                      'example' => 
                      array (
                        0 => 
                        array (
                          'name' => 'UnreadNotifications',
                          'count' => 2,
                        ),
                        1 => 
                        array (
                          'name' => 'Bookmarks',
                          'count' => 3,
                        ),
                      ),
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'counts',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Get information about menu counts for current user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/register' => 
    array (
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'email' => 
                    array (
                      'description' => 'Email address of the user.',
                      'minLength' => 0,
                      'type' => 'string',
                    ),
                    'name' => 
                    array (
                      'description' => 'Name of the user.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'userID' => 
                    array (
                      'description' => 'ID of the user.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'userID',
                    1 => 'name',
                    2 => 'email',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'discoveryText' => 
                  array (
                    'description' => 'Why does the user wish to join? Only used when the registration is flagged as SPAM (response code: 202).',
                    'type' => 'string',
                  ),
                  'email' => 
                  array (
                    'description' => 'An email address for this user.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                  'name' => 
                  array (
                    'description' => 'The username.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                  'password' => 
                  array (
                    'description' => 'A password for this user.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'email',
                  1 => 'name',
                  2 => 'password',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Submit a new user registration.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/request-password' => 
    array (
      'post' => 
      array (
        'responses' => 
        array (
          201 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'email' => 
                  array (
                    'description' => 'The email/username of the user.',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'email',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'deleteMethod' => 
                  array (
                    'type' => 'string',
                    'default' => 'delete',
                    'description' => 'The deletion method / strategy.',
                    'enum' => 
                    array (
                      0 => 'keep',
                      1 => 'wipe',
                      2 => 'delete',
                    ),
                  ),
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Delete a user.',
        'x-addon' => 'dashboard',
      ),
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'rank',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/User',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Get a user.',
        'x-addon' => 'dashboard',
      ),
      'patch' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/User',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/UserPatch',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Update a user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/badges' => 
    array (
      'x-addon' => 'badges',
      'get' => 
      array (
        'summary' => 'Get user badges.',
        'tags' => 
        array (
          0 => 'Users',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Page number. See [Pagination](https://docs.vanillaforums.com/apiv2/#pagination).
',
            'in' => 'query',
            'name' => 'page',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 1,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
          2 => 
          array (
            'description' => 'Desired number of items per page.
',
            'in' => 'query',
            'name' => 'limit',
            'schema' => 
            array (
              'type' => 'integer',
              'default' => 30,
              'maximum' => 100,
              'minimum' => 1,
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/UserBadges',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/ban' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'banned' => 
                    array (
                      'description' => 'The current banned value.',
                      'type' => 'boolean',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'banned',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'banned' => 
                  array (
                    'description' => 'Pass true to ban or false to unban.',
                    'type' => 'boolean',
                  ),
                ),
                'required' => 
                array (
                  0 => 'banned',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Ban a user.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/confirm-email' => 
    array (
      'post' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'email' => 
                    array (
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'emailConfirmed' => 
                    array (
                      'type' => 'boolean',
                    ),
                    'userID' => 
                    array (
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'userID',
                    1 => 'email',
                    2 => 'emailConfirmed',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'confirmationCode' => 
                  array (
                    'description' => 'Email confirmation code',
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'confirmationCode',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Confirm a users current email address by using a confirmation code',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/edit' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'rank',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'bypassSpam' => 
                    array (
                      'description' => 'Should submissions from this user bypass SPAM checks?',
                      'type' => 'boolean',
                    ),
                    'email' => 
                    array (
                      'description' => 'Email address of the user.',
                      'minLength' => 0,
                      'type' => 'string',
                    ),
                    'emailConfirmed' => 
                    array (
                      'description' => 'Has the email address for this user been confirmed?',
                      'type' => 'boolean',
                    ),
                    'name' => 
                    array (
                      'description' => 'Name of the user.',
                      'minLength' => 1,
                      'type' => 'string',
                    ),
                    'photo' => 
                    array (
                      'description' => 'Raw photo field value from the user record.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                    'userID' => 
                    array (
                      'description' => 'ID of the user.',
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'userID',
                    1 => 'name',
                    2 => 'email',
                    3 => 'photo',
                    4 => 'emailConfirmed',
                    5 => 'bypassSpam',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Get a user for editing.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/extended' => 
    array (
      'patch' => 
      array (
        'summary' => 'Update a user\'s extended fields.',
        'tags' => 
        array (
          0 => 'Users',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                '$ref' => '#/components/schemas/ExtendedUserFields',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/ExtendedUserFields',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'profileextender',
      ),
    ),
    '/users/{id}/hidden' => 
    array (
      'put' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'hidden' => 
                    array (
                      'description' => 'Whether not the user is hidden from Online status.',
                      'type' => 'boolean',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'hidden',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'hidden' => 
                  array (
                    'description' => 'Whether not the user should be hidden from Online status.',
                    'type' => 'boolean',
                  ),
                ),
                'required' => 
                array (
                  0 => 'hidden',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'summary' => 'Adjust a user’s Online privacy.',
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/photo' => 
    array (
      'delete' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.
',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'rank',
                  1 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          204 => 
          array (
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'summary' => 'Delete a user photo.',
        'x-addon' => 'dashboard',
      ),
      'post' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'photoUrl' => 
                    array (
                      'description' => 'URL to the user photo.',
                      'minLength' => 0,
                      'nullable' => true,
                      'type' => 'string',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'photoUrl',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Users',
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'photo' => 
                  array (
                    'type' => 'string',
                    'format' => 'binary',
                  ),
                ),
                'required' => 
                array (
                  0 => 'photo',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/rank' => 
    array (
      'x-addon' => 'ranks',
      'put' => 
      array (
        'summary' => 'Update the rank of a user.',
        'tags' => 
        array (
          0 => 'Users',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'requestBody' => 
        array (
          'content' => 
          array (
            'application/json' => 
            array (
              'schema' => 
              array (
                'properties' => 
                array (
                  'rankID' => 
                  array (
                    'description' => 'ID of the user rank.',
                    'nullable' => true,
                    'type' => 'integer',
                  ),
                ),
                'required' => 
                array (
                  0 => 'rankID',
                ),
                'type' => 'object',
              ),
            ),
          ),
          'required' => true,
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'properties' => 
                  array (
                    'rankID' => 
                    array (
                      'description' => 'ID of the user rank.',
                      'nullable' => true,
                      'type' => 'integer',
                    ),
                  ),
                  'required' => 
                  array (
                    0 => 'rankID',
                  ),
                  'type' => 'object',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
    '/users/{id}/reacted' => 
    array (
      'get' => 
      array (
        'summary' => 'Get a user\'s posts that have received a certain reaction.',
        'tags' => 
        array (
          0 => 'Users',
        ),
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'The user ID.',
            'in' => 'path',
            'name' => 'id',
            'required' => true,
            'schema' => 
            array (
              'type' => 'integer',
            ),
          ),
          1 => 
          array (
            'description' => 'The reaction to filter by.',
            'in' => 'query',
            'name' => 'reactionUrlcode',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
          2 => 
          array (
            'description' => 'expand parameters',
            'in' => 'query',
            'name' => 'expand',
            'schema' => 
            array (
              'items' => 
              array (
                'enum' => 
                array (
                  0 => 'insertUser',
                  1 => 'updateUser',
                  2 => 'reactions',
                  3 => 'all',
                ),
                'type' => 'string',
              ),
              'type' => 'array',
            ),
            'style' => 'form',
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'type' => 'array',
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/ReactedRecord',
                  ),
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'x-addon' => 'reactions',
      ),
    ),
    '/widgets' => 
    array (
      'get' => 
      array (
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  'items' => 
                  array (
                    '$ref' => '#/components/schemas/Widget',
                  ),
                  'type' => 'array',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Widgets',
        ),
        'summary' => 'Get all widgets registered',
        'x-addon' => 'vanilla',
      ),
    ),
    '/widgets/{name}' => 
    array (
      'get' => 
      array (
        'parameters' => 
        array (
          0 => 
          array (
            'description' => 'Name of the widget.',
            'name' => 'name',
            'in' => 'path',
            'required' => true,
            'schema' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'responses' => 
        array (
          200 => 
          array (
            'content' => 
            array (
              'application/json' => 
              array (
                'schema' => 
                array (
                  '$ref' => '#/components/schemas/Widget',
                ),
              ),
            ),
            'description' => 'Success',
          ),
        ),
        'tags' => 
        array (
          0 => 'Widgets',
        ),
        'summary' => 'Get widget by name.',
        'x-addon' => 'vanilla',
      ),
    ),
  ),
  'components' => 
  array (
    'requestBodies' => 
    array (
      'CategoryPost' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              '$ref' => '#/components/schemas/CategoryPost',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'vanilla',
      ),
      'CommentPost' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              '$ref' => '#/components/schemas/CommentPost',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'vanilla',
      ),
      'ConversationPost' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              '$ref' => '#/components/schemas/ConversationPost',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'conversations',
      ),
      'NotificationSchema' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              '$ref' => '#/components/schemas/NotificationSchema',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'dashboard',
      ),
      'PermissionFragmentArray' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              'items' => 
              array (
                '$ref' => '#/components/schemas/PermissionFragment',
              ),
              'type' => 'array',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'dashboard',
      ),
      'RolePost' => 
      array (
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              '$ref' => '#/components/schemas/RolePost',
            ),
          ),
        ),
        'required' => true,
        'x-addon' => 'dashboard',
      ),
    ),
    'schemas' => 
    array (
      'Addon' => 
      array (
        'properties' => 
        array (
          'addonID' => 
          array (
            'description' => 'The ID of the addon used for API calls.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'conflict' => 
          array (
            'description' => 'An array of addons that conflict with this addon.',
            'items' => 
            array (
              'properties' => 
              array (
                'addonID' => 
                array (
                  'description' => 'The ID of the addon used for API calls.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'constraint' => 
                array (
                  'description' => 'The version requirement.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'key' => 
                array (
                  'description' => 'The unique key that identifies the addon',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'name' => 
                array (
                  'description' => 'The name of the addon.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'type' => 
                array (
                  'description' => 'The type of addon.',
                  'enum' => 
                  array (
                    0 => 'addon',
                    1 => 'theme',
                    2 => 'locale',
                  ),
                  'minLength' => 1,
                  'type' => 'string',
                ),
              ),
              'required' => 
              array (
                0 => 'addonID',
                1 => 'name',
                2 => 'key',
                3 => 'type',
                4 => 'constraint',
              ),
              'type' => 'object',
            ),
            'type' => 'array',
          ),
          'description' => 
          array (
            'description' => 'The addon\'s description',
            'type' => 'string',
          ),
          'enabled' => 
          array (
            'description' => 'Whether or not the addon is enabled.',
            'type' => 'boolean',
          ),
          'iconUrl' => 
          array (
            'description' => 'The addon\'s icon.',
            'format' => 'uri',
            'minLength' => 1,
            'type' => 'string',
          ),
          'key' => 
          array (
            'description' => 'The unique key that identifies the addon',
            'minLength' => 1,
            'type' => 'string',
          ),
          'name' => 
          array (
            'description' => 'The name of the addon.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'require' => 
          array (
            'description' => 'An array of addons that are required to enable the addon.',
            'items' => 
            array (
              'properties' => 
              array (
                'addonID' => 
                array (
                  'description' => 'The ID of the addon used for API calls.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'constraint' => 
                array (
                  'description' => 'The version requirement.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'key' => 
                array (
                  'description' => 'The unique key that identifies the addon',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'name' => 
                array (
                  'description' => 'The name of the addon.',
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'type' => 
                array (
                  'description' => 'The type of addon.',
                  'enum' => 
                  array (
                    0 => 'addon',
                    1 => 'theme',
                    2 => 'locale',
                  ),
                  'minLength' => 1,
                  'type' => 'string',
                ),
              ),
              'required' => 
              array (
                0 => 'addonID',
                1 => 'name',
                2 => 'key',
                3 => 'type',
                4 => 'constraint',
              ),
              'type' => 'object',
            ),
            'type' => 'array',
          ),
          'type' => 
          array (
            'description' => 'The type of addon.',
            'enum' => 
            array (
              0 => 'addon',
              1 => 'theme',
              2 => 'locale',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
          'version' => 
          array (
            'description' => 'The addon\'s version.',
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'addonID',
          1 => 'name',
          2 => 'key',
          3 => 'type',
          4 => 'iconUrl',
          5 => 'version',
          6 => 'enabled',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'AuthenticatorFragment' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'authenticatorID' => 
          array (
            'type' => 'integer',
            'description' => 'The authenticator\'s ID.',
          ),
          'name' => 
          array (
            'type' => 'string',
            'description' => 'A human readible name for the authenticator.',
          ),
          'type' => 
          array (
            'type' => 'string',
            'description' => 'The type of authenticator',
            'example' => 'oauth2',
          ),
          'clientID' => 
          array (
            'type' => 'string',
          ),
          'default' => 
          array (
            'type' => 'boolean',
          ),
          'active' => 
          array (
            'type' => 'boolean',
            'example' => true,
          ),
          'visible' => 
          array (
            'type' => 'boolean',
          ),
          'urls' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'signInUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
              'signOutUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
              'authenticateUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
              'registerUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
              'passwordUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
              'profileUrl' => 
              array (
                'nullable' => true,
                'format' => 'uri',
                'type' => 'string',
              ),
            ),
          ),
        ),
        'required' => 
        array (
          0 => 'authenticatorID',
          1 => 'name',
          2 => 'type',
          3 => 'clientID',
          4 => 'default',
          5 => 'active',
          6 => 'visible',
        ),
        'x-addon' => 'dashboard',
      ),
      'BasicError' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'message' => 
          array (
            'description' => 'Verbose description of the error.',
            'type' => 'string',
          ),
          'status' => 
          array (
            'description' => 'Response status code.',
            'type' => 'integer',
          ),
          'errorType' => 
          array (
            'description' => 'A system error code that further breaks down the error.',
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'message',
          1 => 'status',
        ),
        'x-addon' => 'dashboard',
      ),
      'BasicSchema' => 
      array (
        'description' => 'A basic OpenAPI schema validator so that schemas can be submitted as data.',
        'type' => 'object',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'Must be an object.',
            'type' => 'string',
            'enum' => 
            array (
              0 => 'object',
            ),
          ),
          'properties' => 
          array (
            'description' => 'A map of property names to property definitions.',
            'type' => 'object',
            'additionalProperties' => 
            array (
              '$ref' => '#/components/schemas/BasicSchemaProperty',
            ),
          ),
          'required' => 
          array (
            'description' => 'The names of required properties.',
            'type' => 'array',
            'items' => 
            array (
              'type' => 'string',
            ),
          ),
        ),
        'example' => 
        array (
          'type' => 'object',
          'properties' => 
          array (
            'license' => 
            array (
              'type' => 'string',
              'description' => 'Find your license number in the help menu.',
            ),
            'notes' => 
            array (
              'type' => 'string',
              'x-control' => 'textarea',
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'BasicSchemaProperty' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The data type of the property.',
            'type' => 'string',
            'enum' => 
            array (
              0 => 'string',
              1 => 'number',
              2 => 'integer',
              3 => 'boolean',
            ),
          ),
          'description' => 
          array (
            'description' => 'A description to help the user enter the right information.',
            'type' => 'string',
          ),
          'x-label' => 
          array (
            'description' => 'The control label.',
            'type' => 'string',
          ),
          'x-control' => 
          array (
            'description' => 'The type of control used to collect the input.',
            'type' => 'string',
            'enum' => 
            array (
              0 => 'textbox',
              1 => 'textarea',
              2 => 'checkbox',
            ),
          ),
          'default' => 
          array (
            'description' => 'The default value if none is specified.',
            'oneOf' => 
            array (
              0 => 
              array (
                'type' => 'string',
              ),
              1 => 
              array (
                'type' => 'number',
              ),
              2 => 
              array (
                'type' => 'integer',
              ),
              3 => 
              array (
                'type' => 'boolean',
              ),
            ),
            'nullable' => true,
          ),
          'enum' => 
          array (
            'description' => 'Limit input to a set of values.',
            'type' => 'array',
            'items' => 
            array (
              'oneOf' => 
              array (
                0 => 
                array (
                  'type' => 'string',
                ),
                1 => 
                array (
                  'type' => 'number',
                ),
                2 => 
                array (
                  'type' => 'integer',
                ),
                3 => 
                array (
                  'type' => 'boolean',
                ),
              ),
            ),
          ),
          'minLength' => 
          array (
            'description' => 'Minimum string length.',
            'type' => 'integer',
            'minimum' => 0,
          ),
          'maxLength' => 
          array (
            'description' => 'Maximum string length.',
            'type' => 'integer',
            'minimum' => 1,
          ),
          'minimum' => 
          array (
            'description' => 'Minimum numeric value.',
            'type' => 'number',
          ),
          'maximum' => 
          array (
            'description' => 'Maximum numeric value.',
            'type' => 'number',
          ),
        ),
        'required' => 
        array (
          0 => 'type',
        ),
        'x-addon' => 'dashboard',
      ),
      'CategoryFragment' => 
      array (
        'properties' => 
        array (
          'categoryID' => 
          array (
            'description' => 'The ID of the category.',
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'The name of the category.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'url' => 
          array (
            'description' => 'Full URL to the category.',
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'categoryID',
          1 => 'name',
          2 => 'url',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'CategoryPost' => 
      array (
        'properties' => 
        array (
          'customPermissions' => 
          array (
            'description' => 'Are custom permissions set for this category?',
            'type' => 'boolean',
          ),
          'displayAs' => 
          array (
            'type' => 'string',
            'default' => 'discussions',
            'description' => 'The display style of the category.',
            'enum' => 
            array (
              0 => 'categories',
              1 => 'discussions',
              2 => 'flat',
              3 => 'heading',
            ),
            'minLength' => 1,
          ),
          'name' => 
          array (
            'description' => 'The name of the category.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'parentCategoryID' => 
          array (
            'description' => 'Parent category ID.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'urlcode' => 
          array (
            'description' => 'The URL code of the category.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'featured' => 
          array (
            'description' => 'Category is featured',
            'type' => 'boolean',
            'default' => false,
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'urlcode',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'Comment' => 
      array (
        'properties' => 
        array (
          'attributes' => 
          array (
            'nullable' => true,
            'properties' => 
            array (
              'hootsuite' => 
              array (
                'description' => 'Hootsuite metadata.',
                'type' => 'object',
                'x-addon' => 'hootsuite',
              ),
            ),
            'type' => 'object',
          ),
          'name' => 
          array (
            'description' => 'Name of the comment (based on the discussion).',
            'type' => 'string',
          ),
          'body' => 
          array (
            'description' => 'The body of the comment.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'commentID' => 
          array (
            'description' => 'The ID of the comment.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the comment was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'groupID' => 
          array (
            'description' => 'GroupID of the comment if applicable.',
            'type' => 'integer',
            'nullable' => true,
          ),
          'dateUpdated' => 
          array (
            'description' => 'When the comment was last updated.',
            'format' => 'date-time',
            'nullable' => true,
            'type' => 'string',
          ),
          'categoryID' => 
          array (
            'description' => 'The category of the comment.',
            'type' => 'integer',
          ),
          'discussionID' => 
          array (
            'description' => 'The ID of the discussion.',
            'type' => 'integer',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'insertUserID' => 
          array (
            'description' => 'The user that created the comment.',
            'type' => 'integer',
          ),
          'reactions' => 
          array (
            'type' => 'array',
            'items' => 
            array (
              'properties' => 
              array (
                'class' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'count' => 
                array (
                  'type' => 'integer',
                ),
                'name' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'tagID' => 
                array (
                  'type' => 'integer',
                ),
                'urlcode' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
              ),
              'required' => 
              array (
                0 => 'tagID',
                1 => 'urlcode',
                2 => 'name',
                3 => 'class',
                4 => 'count',
              ),
              'type' => 'object',
            ),
            'x-addon' => 'reactions',
          ),
          'score' => 
          array (
            'description' => 'Total points associated with this post.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'url' => 
          array (
            'description' => 'The full URL to the comment.',
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'commentID',
          1 => 'discussionID',
          2 => 'categoryID',
          3 => 'name',
          4 => 'body',
          5 => 'dateInserted',
          6 => 'dateUpdated',
          7 => 'insertUserID',
          8 => 'score',
          9 => 'attributes',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'CommentGet' => 
      array (
        'properties' => 
        array (
          'expand' => 
          array (
            'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.',
            'items' => 
            array (
              'enum' => 
              array (
                0 => 'reactions',
                1 => 'all',
              ),
              'type' => 'string',
            ),
            'type' => 'array',
            'x-collectionFormat' => 'csv',
          ),
          'id' => 
          array (
            'description' => 'The comment ID.',
            'type' => 'integer',
          ),
        ),
        'required' => 
        array (
          0 => 'id',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'CommentPost' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the comment.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'discussionID' => 
          array (
            'description' => 'The ID of the discussion.',
            'type' => 'integer',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
        ),
        'required' => 
        array (
          0 => 'body',
          1 => 'format',
          2 => 'discussionID',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'Config' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'garden.description' => 
          array (
            'description' => 'The site description usually appears in search engines. You should try having a description that is 100–150
characters long.
',
            'type' => 'string',
            'default' => '',
            'maxLength' => 350,
            'x-key' => 'Garden.Description',
            'x-read' => 'public',
            'x-write' => 'community.manage',
          ),
          'garden.externalUrlFormat' => 
          array (
            'description' => 'The format used to generate URLs to pages from external sources, typically emails. Set this config setting
your site is embedded or uses a reverse proxy. Place a "%s" in the URL and it will be replaced with the path
being generated.
',
            'type' => 'string',
            'default' => '',
            'example' => 'https://example.com/community/%s',
            'pattern' => '(^$)|(^https?://.+%s)',
            'x-key' => 'Garden.ExternalUrlFormat',
            'x-read' => 'community.manage',
          ),
          'garden.homepageTitle' => 
          array (
            'description' => 'The homepage title is displayed on your home page. Pick a title that you would want to see appear in search
engines.
',
            'type' => 'string',
            'default' => '',
            'example' => 'Welcome To Our Support Community',
            'maxLength' => 100,
            'x-key' => 'Garden.HomepageTitle',
            'x-read' => 'public',
            'x-write' => 'community.manage',
          ),
          'garden.orgName' => 
          array (
            'description' => 'Your organization name is used for SEO microdata and JSON+LD.
',
            'type' => 'string',
            'default' => '',
            'maxLength' => 50,
            'x-key' => 'Garden.OrgName',
            'x-read' => 'public',
            'x-write' => 'community.manage',
          ),
          'garden.privacy.ips' => 
          array (
            'description' => 'Anonymize IP addresses on users so they aren\'t tracked. You can specify "partial" to remove the last octet or full to anonymize the entire IP address.',
            'type' => 'string',
            'default' => '',
            'enum' => 
            array (
              0 => '',
              1 => 'partial',
              2 => 'full',
            ),
            'x-key' => 'Garden.Privacy.IPs',
            'x-read' => 'community.manage',
          ),
          'garden.title' => 
          array (
            'description' => 'The banner title appears on your site\'s banner and in your browser\'s title bar.
',
            'type' => 'string',
            'default' => '',
            'example' => 'Support Community',
            'maxLength' => 50,
            'x-key' => 'Garden.Title',
            'x-read' => 'public',
            'x-write' => 'community.manage',
          ),
          'labs.userCards' => 
          array (
            'description' => 'Enable user cards on older themes.
',
            'type' => 'boolean',
            'default' => false,
            'example' => true,
            'x-key' => 'Feature.UserCards.Enabled',
            'x-read' => 'public',
            'x-write' => 'site.manage',
          ),
          'labs.newSearchPage' => 
          array (
            'description' => 'Enable the new search UI on older themes.
',
            'type' => 'boolean',
            'default' => false,
            'example' => true,
            'x-key' => 'Feature.useNewSearchPage.Enabled',
            'x-read' => 'public',
            'x-write' => 'site.manage',
          ),
          'labs.newQuickLinks' => 
          array (
            'description' => 'Enable the new quick links UI on older themes.
',
            'type' => 'boolean',
            'default' => false,
            'example' => true,
            'x-key' => 'Feature.NewQuickLinks.Enabled',
            'x-read' => 'public',
            'x-write' => 'site.manage',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'Conversation' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The most recent unread message in the conversation.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'conversationID' => 
          array (
            'description' => 'The ID of the conversation.',
            'type' => 'integer',
          ),
          'countMessages' => 
          array (
            'description' => 'The number of messages on the conversation.',
            'type' => 'integer',
          ),
          'countParticipants' => 
          array (
            'description' => 'The number of participants on the conversation.',
            'type' => 'integer',
          ),
          'countUnread' => 
          array (
            'description' => 'The number of unread messages.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the conversation was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'insertUserID' => 
          array (
            'description' => 'The user that created the conversation.',
            'type' => 'integer',
          ),
          'lastMessage' => 
          array (
            'properties' => 
            array (
              'dateInserted' => 
              array (
                'description' => 'The date of the message.',
                'format' => 'date-time',
                'type' => 'string',
              ),
              'insertUser' => 
              array (
                '$ref' => '#/components/schemas/UserFragment',
              ),
              'insertUserID' => 
              array (
                'description' => 'The author of the your most recent message.',
                'type' => 'integer',
              ),
            ),
            'required' => 
            array (
              0 => 'insertUserID',
              1 => 'dateInserted',
              2 => 'insertUser',
            ),
            'type' => 'object',
          ),
          'name' => 
          array (
            'description' => 'The name of the conversation.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'participants' => 
          array (
            '$ref' => '#/components/schemas/ConversationParticipants',
          ),
          'unread' => 
          array (
            'description' => 'Whether the conversation has an unread indicator.',
            'type' => 'boolean',
          ),
          'url' => 
          array (
            'description' => 'The URL of the conversation.',
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'conversationID',
          1 => 'name',
          2 => 'body',
          3 => 'url',
          4 => 'dateInserted',
          5 => 'insertUserID',
          6 => 'countParticipants',
          7 => 'countMessages',
        ),
        'type' => 'object',
        'x-addon' => 'conversations',
      ),
      'ConversationParticipants' => 
      array (
        'description' => 'List of participants.',
        'items' => 
        array (
          'properties' => 
          array (
            'userID' => 
            array (
              'description' => 'The userID of the participant.',
              'type' => 'integer',
            ),
            'status' => 
            array (
              'description' => 'Participation status of the user.',
              'enum' => 
              array (
                0 => 'participating',
                1 => 'deleted',
              ),
              'type' => 'string',
            ),
            'user' => 
            array (
              '$ref' => '#/components/schemas/UserFragment',
            ),
          ),
          'required' => 
          array (
            0 => 'userID',
            1 => 'status',
          ),
          'type' => 'object',
        ),
        'type' => 'array',
        'x-addon' => 'conversations',
      ),
      'ConversationPost' => 
      array (
        'properties' => 
        array (
          'participantUserIDs' => 
          array (
            'description' => 'List of userID of the participants.',
            'items' => 
            array (
              'type' => 'integer',
            ),
            'type' => 'array',
          ),
        ),
        'required' => 
        array (
          0 => 'participantUserIDs',
          1 => 'name',
        ),
        'type' => 'object',
        'x-addon' => 'conversations',
      ),
      'Discussion' => 
      array (
        'properties' => 
        array (
          'attributes' => 
          array (
            'properties' => 
            array (
              'idea' => 
              array (
                'properties' => 
                array (
                  'status' => 
                  array (
                    'properties' => 
                    array (
                      'name' => 
                      array (
                        'description' => 'Label for the status.',
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                      'state' => 
                      array (
                        'description' => 'The open/closed state of an idea.',
                        'enum' => 
                        array (
                          0 => 'open',
                          1 => 'closed',
                        ),
                        'minLength' => 1,
                        'type' => 'string',
                      ),
                    ),
                    'required' => 
                    array (
                      0 => 'name',
                      1 => 'state',
                    ),
                    'type' => 'object',
                  ),
                  'statusID' => 
                  array (
                    'description' => 'Unique numeric ID of a status.',
                    'type' => 'integer',
                  ),
                  'statusNotes' => 
                  array (
                    'description' => 'Status update notes.',
                    'minLength' => 1,
                    'nullable' => true,
                    'type' => 'string',
                  ),
                  'type' => 
                  array (
                    'description' => 'Voting type for this idea: up-only or up and down.',
                    'enum' => 
                    array (
                      0 => 'up',
                      1 => 'up-down',
                    ),
                    'minLength' => 1,
                    'type' => 'string',
                  ),
                ),
                'required' => 
                array (
                  0 => 'statusNotes',
                  1 => 'statusID',
                  2 => 'status',
                  3 => 'type',
                ),
                'type' => 'object',
                'x-addon' => 'ideation',
              ),
            ),
            'type' => 'object',
          ),
          'body' => 
          array (
            'description' => 'The body of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'bookmarked' => 
          array (
            'description' => 'Whether or not the discussion is bookmarked by the current user.',
            'type' => 'boolean',
          ),
          'category' => 
          array (
            '$ref' => '#/components/schemas/CategoryFragment',
          ),
          'categoryID' => 
          array (
            'description' => 'The category the discussion is in.',
            'type' => 'integer',
          ),
          'closed' => 
          array (
            'description' => 'Whether the discussion is closed or open.',
            'type' => 'boolean',
          ),
          'countComments' => 
          array (
            'description' => 'The number of comments on the discussion.',
            'type' => 'integer',
          ),
          'countUnread' => 
          array (
            'description' => 'The number of unread comments.',
            'type' => 'integer',
          ),
          'countViews' => 
          array (
            'description' => 'The number of views on the discussion.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the discussion was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'dateUpdated' => 
          array (
            'description' => 'When the discussion was last updated.',
            'format' => 'date-time',
            'nullable' => true,
            'type' => 'string',
          ),
          'dateLastComment' => 
          array (
            'description' => 'The date of the last comment or the original discussion date if it has no comments.',
            'type' => 'string',
            'format' => 'date-time',
          ),
          'discussionID' => 
          array (
            'description' => 'The ID of the discussion.',
            'type' => 'integer',
          ),
          'groupID' => 
          array (
            'x-addon' => 'groups',
            'description' => 'The group the discussion is in.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'insertUserID' => 
          array (
            'description' => 'The user that created the discussion.',
            'type' => 'integer',
          ),
          'lastPost' => 
          array (
            '$ref' => '#/components/schemas/PostFragment',
          ),
          'lastUserID' => 
          array (
            'type' => 'integer',
            'description' => 'The last user to post in the discussion.',
          ),
          'lastUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'name' => 
          array (
            'description' => 'The title of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'pinLocation' => 
          array (
            'description' => 'The location for the discussion, if pinned. "category" are pinned to their own category. "recent" are pinned to the recent discussions list, as well as their own category.',
            'enum' => 
            array (
              0 => 'category',
              1 => 'recent',
            ),
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'pinned' => 
          array (
            'description' => 'Whether or not the discussion has been pinned.',
            'type' => 'boolean',
          ),
          'reactions' => 
          array (
            'items' => 
            array (
              'properties' => 
              array (
                'class' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'count' => 
                array (
                  'type' => 'integer',
                ),
                'name' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
                'tagID' => 
                array (
                  'type' => 'integer',
                ),
                'urlcode' => 
                array (
                  'minLength' => 1,
                  'type' => 'string',
                ),
              ),
              'required' => 
              array (
                0 => 'tagID',
                1 => 'urlcode',
                2 => 'name',
                3 => 'class',
                4 => 'count',
              ),
              'type' => 'object',
            ),
            'type' => 'array',
            'x-addon' => 'reactions',
          ),
          'score' => 
          array (
            'description' => 'Total points associated with this post.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'sink' => 
          array (
            'description' => 'Whether or not the discussion has been sunk.',
            'type' => 'boolean',
          ),
          'type' => 
          array (
            'description' => 'The type of this discussion if any.',
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'unread' => 
          array (
            'description' => 'Whether or not the discussion should have an unread indicator.',
            'type' => 'boolean',
          ),
          'url' => 
          array (
            'description' => 'The full URL to the discussion.',
            'type' => 'string',
          ),
          'resolved' => 
          array (
            'x-addon' => 'resolved2',
            'description' => 'Whether or not the discussion should has been resolved.',
            'type' => 'boolean',
          ),
        ),
        'required' => 
        array (
          0 => 'discussionID',
          1 => 'type',
          2 => 'name',
          3 => 'body',
          4 => 'categoryID',
          5 => 'dateInserted',
          6 => 'dateUpdated',
          7 => 'insertUserID',
          8 => 'pinLocation',
          9 => 'closed',
          10 => 'sink',
          11 => 'countComments',
          12 => 'countViews',
          13 => 'score',
          14 => 'bookmarked',
          15 => 'unread',
          16 => 'attributes',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'DiscussionGetEdit' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'categoryID' => 
          array (
            'description' => 'The category the discussion is in.',
            'type' => 'integer',
          ),
          'closed' => 
          array (
            'description' => 'Whether the discussion is closed or open.',
            'type' => 'boolean',
          ),
          'discussionID' => 
          array (
            'description' => 'The ID of the discussion.',
            'type' => 'integer',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
          'groupID' => 
          array (
            'x-addon' => 'groups',
            'description' => 'The group the discussion is in.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'The title of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'pinLocation' => 
          array (
            'description' => 'The location for the discussion, if pinned. "category" are pinned to their own category.
"recent" are pinned to the recent discussions list, as well as their own category.
',
            'enum' => 
            array (
              0 => 'category',
              1 => 'recent',
            ),
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'pinned' => 
          array (
            'description' => 'Whether or not the discussion has been pinned.',
            'type' => 'boolean',
          ),
          'sink' => 
          array (
            'description' => 'Whether or not the discussion has been sunk.',
            'type' => 'boolean',
          ),
        ),
        'required' => 
        array (
          0 => 'discussionID',
          1 => 'name',
          2 => 'body',
          3 => 'format',
          4 => 'categoryID',
          5 => 'sink',
          6 => 'closed',
          7 => 'pinned',
          8 => 'pinLocation',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'DiscussionPatch' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'categoryID' => 
          array (
            'description' => 'The category the discussion is in.',
            'type' => 'integer',
          ),
          'closed' => 
          array (
            'description' => 'Whether the discussion is closed or open.',
            'type' => 'boolean',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
          'groupID' => 
          array (
            'x-addon' => 'groups',
            'description' => 'The group the discussion is in.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'The title of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'pinLocation' => 
          array (
            'description' => 'The location for the discussion, if pinned. "category" are pinned to their own category. "recent" are pinned to the recent discussions list, as well as their own category.',
            'enum' => 
            array (
              0 => 'category',
              1 => 'recent',
            ),
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'pinned' => 
          array (
            'description' => 'Whether or not the discussion has been pinned.',
            'type' => 'boolean',
          ),
          'sink' => 
          array (
            'description' => 'Whether or not the discussion has been sunk.',
            'type' => 'boolean',
          ),
          'resolved' => 
          array (
            'x-addon' => 'resolved2',
            'description' => 'Whether or not the discussion should has been resolved.',
            'type' => 'boolean',
          ),
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'DiscussionPost' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'categoryID' => 
          array (
            'description' => 'The category the discussion is in.',
            'type' => 'integer',
          ),
          'closed' => 
          array (
            'description' => 'Whether the discussion is closed or open.',
            'type' => 'boolean',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
          'groupID' => 
          array (
            'x-addon' => 'groups',
            'description' => 'The group the discussion is in.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'The title of the discussion.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'pinLocation' => 
          array (
            'description' => 'The location for the discussion, if pinned. "category" are pinned to their own category. "recent" are pinned to the recent discussions list, as well as their own category.',
            'enum' => 
            array (
              0 => 'category',
              1 => 'recent',
            ),
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'pinned' => 
          array (
            'description' => 'Whether or not the discussion has been pinned.',
            'type' => 'boolean',
          ),
          'sink' => 
          array (
            'description' => 'Whether or not the discussion has been sunk.',
            'type' => 'boolean',
          ),
          'resolved' => 
          array (
            'x-addon' => 'resolved2',
            'description' => 'Whether or not the discussion should has been resolved.',
            'type' => 'boolean',
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'body',
          2 => 'format',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'DraftPatch' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'attributes' => 
          array (
            'description' => 'A free-form object containing all custom data for this draft.',
            'type' => 'object',
          ),
          'parentRecordID' => 
          array (
            'description' => 'The unique ID of the intended parent to this record.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'recordType' => 
          array (
            'description' => 'The type of record associated with this draft.',
            'enum' => 
            array (
              0 => 'comment',
              1 => 'discussion',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'DraftPost' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'attributes' => 
          array (
            'description' => 'A free-form object containing all custom data for this draft.',
            'type' => 'object',
          ),
          'parentRecordID' => 
          array (
            'description' => 'The unique ID of the intended parent to this record.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'recordType' => 
          array (
            'description' => 'The type of record associated with this draft.',
            'enum' => 
            array (
              0 => 'comment',
              1 => 'discussion',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'recordType',
          1 => 'attributes',
        ),
        'x-addon' => 'dashboard',
      ),
      'ExpandAssetsValues' => 
      array (
        'type' => 'array',
        'items' => 
        array (
          'type' => 'string',
          'enum' => 
          array (
            0 => 'all',
            1 => 'javascript.data',
            2 => 'css.data',
            3 => 'variables.data',
            4 => 'header.data',
            5 => 'footer.data',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'ExtendedUserFields' => 
      array (
        'type' => 'object',
        'description' => 'The user\'s profile extender fields.',
        'x-addon' => 'profileextender',
      ),
      'Format' => 
      array (
        'description' => 'The format of the body used to convert it to HTML.',
        'type' => 'string',
        'enum' => 
        array (
          0 => 'rich',
          1 => 'markdown',
          2 => 'text',
          3 => 'textex',
          4 => 'wysiwyg',
          5 => 'bbcode',
        ),
        'example' => 'markdown',
        'x-addon' => 'dashboard',
      ),
      'InsertInfo' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'dateInserted' => 
          array (
            'type' => 'string',
            'format' => 'date-time',
            'description' => 'The date the record was inserted.',
            'readOnly' => true,
          ),
          'insertUserID' => 
          array (
            'type' => 'integer',
            'description' => 'The user that inserted the record.',
            'readOnly' => true,
          ),
          'insertIPAddress' => 
          array (
            'type' => 'string',
            'format' => 'ipv4',
            'description' => 'The IP address the record was inserted from.',
            'readOnly' => true,
          ),
        ),
        'required' => 
        array (
          0 => 'dateInserted',
          1 => 'insertUserID',
          2 => 'insertIPAddress',
        ),
        'x-addon' => 'dashboard',
      ),
      'LegacyTag' => 
      array (
        'description' => 'A backwards compatible tag.',
        'allOf' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/schemas/TagFragment',
          ),
          1 => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'id' => 
              array (
                'type' => 'integer',
                'description' => 'The ID of the tag.',
                'deprecated' => true,
              ),
              'urlCode' => 
              array (
                'description' => 'The url-code of the tag.',
                'type' => 'string',
                'deprecated' => true,
              ),
            ),
            'required' => 
            array (
              0 => 'id',
              1 => 'urlCode',
            ),
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'Locale' => 
      array (
        'properties' => 
        array (
          'localeID' => 
          array (
            'description' => 'The key of the locale addon.',
            'minLength' => 1,
            'type' => 'string',
            'example' => 'vf_fr_CA,',
          ),
          'localeKey' => 
          array (
            'description' => 'The normalized key of the locale without any regional modifier.',
            'type' => 'string',
            'example' => 'fr',
          ),
          'regionalKey' => 
          array (
            'description' => 'The normalized key of the locale with a regional modifier if it exists.',
            'type' => 'string',
            'example' => 'fr_CA',
          ),
          'displayNames' => 
          array (
            'type' => 'object',
            'description' => 'Translatable names of the',
            'example' => 
            array (
              'en' => 'French',
              'fr' => 'Français',
              'de' => 'Französisch',
            ),
          ),
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'MediaItemPatch' => 
      array (
        'properties' => 
        array (
          'foreignID' => 
          array (
            'description' => 'Unique ID of the resource this media item will be attached to.',
            'type' => 'integer',
          ),
          'foreignType' => 
          array (
            'description' => 'Type of resource the media item will be attached to (e.g. comment).',
            'enum' => 
            array (
              0 => 'embed',
              1 => 'comment',
              2 => 'discussion',
            ),
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'foreignType',
          1 => 'foreignID',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'Message' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the message.',
            'maxLength' => 2000,
            'minLength' => 1,
            'type' => 'string',
          ),
          'conversationID' => 
          array (
            'description' => 'The ID of the conversation.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the message was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'insertUserID' => 
          array (
            'description' => 'The user that created the message.',
            'type' => 'integer',
          ),
          'messageID' => 
          array (
            'description' => 'The ID of the message.',
            'type' => 'integer',
          ),
        ),
        'required' => 
        array (
          0 => 'messageID',
          1 => 'conversationID',
          2 => 'body',
          3 => 'insertUserID',
          4 => 'dateInserted',
        ),
        'type' => 'object',
        'x-addon' => 'conversations',
      ),
      'MessagePost' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The body of the message.',
            'maxLength' => 2000,
            'minLength' => 1,
            'type' => 'string',
          ),
          'conversationID' => 
          array (
            'description' => 'The ID of the conversation.',
            'type' => 'integer',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
        ),
        'required' => 
        array (
          0 => 'conversationID',
          1 => 'body',
        ),
        'type' => 'object',
        'x-addon' => 'conversations',
      ),
      'NotificationPatchSchema' => 
      array (
        'description' => 'Fields for patching a notification.',
        'type' => 'object',
        'properties' => 
        array (
          'read' => 
          array (
            'type' => 'boolean',
            'enum' => 
            array (
              0 => true,
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'NotificationSchema' => 
      array (
        'properties' => 
        array (
          'body' => 
          array (
            'description' => 'The notification text. This contains some HTML, but only <b> tags.',
            'type' => 'string',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the notification was first made.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'dateUpdated' => 
          array (
            'description' => 'When the notification was last updated.

Notifications on the same record will group together into a single notification, updating just the dateUpdated property.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'notificationID' => 
          array (
            'description' => 'A unique ID to identify the notification.',
            'type' => 'integer',
          ),
          'photoUrl' => 
          array (
            'description' => 'An avatar or thumbnail associated with the notification.',
            'nullable' => true,
            'type' => 'string',
          ),
          'read' => 
          array (
            'description' => 'Whether or not the notification has been seen.',
            'type' => 'boolean',
          ),
          'url' => 
          array (
            'description' => 'The target of the notification.',
            'type' => 'string',
          ),
          'readUrl' => 
          array (
            'description' => 'Url to read the notification.',
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'notificationID',
          1 => 'body',
          2 => 'photoUrl',
          3 => 'url',
          4 => 'dateInserted',
          5 => 'dateUpdated',
          6 => 'read',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'OAuth2Authenticator' => 
      array (
        'allOf' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/schemas/AuthenticatorFragment',
          ),
          1 => 
          array (
            '$ref' => '#/components/schemas/OAuth2AuthenticatorPost',
          ),
        ),
        'x-addon' => 'oauth2',
      ),
      'OAuth2AuthenticatorPatch' => 
      array (
        '$ref' => '#/components/schemas/OAuth2AuthenticatorPost',
        'x-addon' => 'oauth2',
      ),
      'OAuth2AuthenticatorPost' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'secret' => 
          array (
            'type' => 'string',
          ),
          'urls' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'tokenUrl' => 
              array (
                'description' => 'The URL on the OAuth server used to get the access token.',
                'type' => 'string',
                'format' => 'uri',
              ),
              'profileUrl' => 
              array (
                'description' => 'The URL on tge OAuth server used to get the user\'s profile information.',
                'type' => 'string',
                'format' => 'uri',
              ),
            ),
          ),
          'authenticationRequest' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'scope' => 
              array (
                'type' => 'string',
                'description' => 'The scope to request from the OAuth server.',
                'x-column' => 'AcceptedScope',
              ),
              'prompt' => 
              array (
                'type' => 'string',
                'description' => 'Prompt Parameter to append to Authorize Url',
                'example' => 'consent login',
              ),
            ),
          ),
          'useBearerToken' => 
          array (
            'type' => 'boolean',
            'description' => 'When requesting the profile, pass the access token in the HTTP header. i.e Authorization: Bearer [accesstoken]
',
            'default' => true,
            'x-column' => 'BearerToken',
          ),
          'allowAccessTokens' => 
          array (
            'type' => 'boolean',
            'description' => 'Allow this connection to issue Vanilla API access tokens.
',
            'default' => false,
          ),
          'userMappings' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'uniqueID' => 
              array (
                'description' => 'The Key in the JSON array to designate UserID.',
                'type' => 'string',
                'x-column' => 'ProfileKeyUniqueID',
              ),
              'email' => 
              array (
                'description' => 'The Key in the JSON array to designate Emails.',
                'type' => 'string',
                'x-column' => 'ProfileKeyEmail',
              ),
              'name' => 
              array (
                'description' => 'The Key in the JSON array to designate Display Name.',
                'type' => 'string',
                'x-column' => 'ProfileKeyName',
              ),
              'photoUrl' => 
              array (
                'description' => 'The Key in the JSON array to designate Photo.',
                'type' => 'string',
                'x-column' => 'ProfileKeyPhoto',
              ),
              'fullName' => 
              array (
                'description' => 'The Key in the JSON array to designate Full Name.',
                'type' => 'string',
                'x-column' => 'ProfileKeyFullName',
              ),
              'roles' => 
              array (
                'description' => 'The Key in the JSON array to designate Roles.',
                'type' => 'string',
                'x-column' => 'ProfileKeyRoles',
              ),
            ),
          ),
        ),
        'x-addon' => 'oauth2',
      ),
      'PermissionFragment' => 
      array (
        'properties' => 
        array (
          'id' => 
          array (
            'type' => 'integer',
          ),
          'permissions' => 
          array (
            'type' => 'object',
          ),
          'type' => 
          array (
            'enum' => 
            array (
              0 => 'global',
              1 => 'category',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'type',
          1 => 'permissions',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'PocketPatch' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'name' => 
          array (
            'description' => 'The name of the pocket',
            'type' => 'string',
          ),
          'body' => 
          array (
            'type' => 'string',
            'description' => 'The body of the pocket.',
          ),
          'repeatType' => 
          array (
            'enum' => 
            array (
              0 => 'once',
              1 => 'after',
              2 => 'before',
              3 => 'every',
              4 => 'index',
            ),
          ),
          'widgetID' => 
          array (
            'type' => 'string',
            'description' => 'The widget type.',
          ),
          'page' => 
          array (
            'description' => 'Which page to display the Pocket on.',
            'type' => 'string',
          ),
          'repeatEvery' => 
          array (
            'description' => 'Repeat frequency.',
            'type' => 'integer',
          ),
          'repeatIndexes' => 
          array (
            'description' => 'Repeat indexes',
            'type' => 'integer',
          ),
          'mobileType' => 
          array (
            'enum' => 
            array (
              0 => 'only',
              1 => 'never',
              2 => 'default',
            ),
            'type' => 'string',
          ),
          'isDashboard' => 
          array (
            'description' => 'Pocket active in the dasboard.',
            'type' => 'boolean',
            'default' => false,
          ),
          'sort' => 
          array (
            'description' => 'Pocket sort order.',
            'type' => 'integer',
            'default' => 0,
          ),
          'isEmbeddable' => 
          array (
            'description' => 'Pocket can be embedded.',
            'type' => 'boolean',
            'default' => false,
          ),
          'location' => 
          array (
            'description' => 'Location of the pocket on the page.',
            'type' => 'string',
          ),
          'isAd' => 
          array (
            'description' => 'If the pocket is an ad.',
            'type' => 'boolean',
            'default' => false,
          ),
          'enabled' => 
          array (
            'description' => 'Pocket enabled/disabled.',
            'type' => 'boolean',
            'default' => false,
          ),
          'categoryID' => 
          array (
            'description' => 'Pocket active in this category.',
            'type' => 'integer',
          ),
          'includeChildCategories' => 
          array (
            'description' => 'Incldue category child categories.',
            'type' => 'integer',
          ),
          'format' => 
          array (
            'description' => 'Format of the pocket',
            'enum' => 
            array (
              0 => 'raw',
              1 => 'widget',
            ),
            'type' => 'string',
          ),
          'roleIDs' => 
          array (
            'description' => 'RoleIDs that the pocket will apply to.',
            'type' => 'array',
            'items' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'x-addon' => 'pockets',
      ),
      'PocketPost' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'name' => 
          array (
            'type' => 'string',
            'description' => 'The name of the pocket',
          ),
          'body' => 
          array (
            'type' => 'string',
            'description' => 'The body of the pocket.',
          ),
          'repeatType' => 
          array (
            'enum' => 
            array (
              0 => 'once',
              1 => 'after',
              2 => 'before',
              3 => 'every',
              4 => 'index',
            ),
          ),
          'widgetID' => 
          array (
            'type' => 'string',
            'description' => 'The widget type.',
          ),
          'page' => 
          array (
            'description' => 'Which page to display the Pocket on.',
            'type' => 'string',
          ),
          'repeatEvery' => 
          array (
            'description' => 'Repeat frequency.',
            'type' => 'integer',
          ),
          'repeatIndexes' => 
          array (
            'description' => 'Repeat indexes',
            'type' => 'integer',
          ),
          'mobileType' => 
          array (
            'enum' => 
            array (
              0 => 'only',
              1 => 'never',
              2 => 'default',
            ),
          ),
          'isDashboard' => 
          array (
            'description' => 'Pocket active in the dasboard.',
            'type' => 'boolean',
            'default' => false,
          ),
          'sort' => 
          array (
            'description' => 'Pocket sort order.',
            'type' => 'integer',
            'default' => 0,
          ),
          'isEmbeddable' => 
          array (
            'description' => 'Pocket can be embedded.',
            'type' => 'boolean',
            'default' => false,
          ),
          'location' => 
          array (
            'description' => 'Location of the pocket on the page.',
            'type' => 'string',
          ),
          'isAd' => 
          array (
            'description' => 'If the pocket is an ad.',
            'type' => 'boolean',
            'default' => false,
          ),
          'enabled' => 
          array (
            'description' => 'Pocket enabled/disabled.',
            'type' => 'boolean',
            'default' => false,
          ),
          'categoryID' => 
          array (
            'description' => 'Pocket active in this category.',
            'type' => 'integer',
          ),
          'includeChildCategories' => 
          array (
            'description' => 'Incldue category child categories.',
            'type' => 'integer',
          ),
          'format' => 
          array (
            'description' => 'Format of the pocket',
            'enum' => 
            array (
              0 => 'raw',
              1 => 'widget',
            ),
            'type' => 'string',
          ),
          'roleIDs' => 
          array (
            'description' => 'RoleIDs that the pocket will apply to.',
            'type' => 'array',
            'items' => 
            array (
              'type' => 'integer',
            ),
          ),
        ),
        'required' => 
        array (
          0 => 'pocketID',
          1 => 'widgetID',
          2 => 'name',
          3 => 'repeatType',
        ),
        'x-addon' => 'pockets',
      ),
      'PostFragment' => 
      array (
        'properties' => 
        array (
          'discussionID' => 
          array (
            'description' => 'The discussion ID of the post.',
            'type' => 'integer',
          ),
          'commentID' => 
          array (
            'description' => 'The comment ID of the post, if any.',
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'The title of the post.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'body' => 
          array (
            'description' => 'The HTML formatted body of the post.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'description' => 'The URL of the post.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'dateInserted' => 
          array (
            'description' => 'The date of the post.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'insertUserID' => 
          array (
            'description' => 'The author of the post.',
            'type' => 'integer',
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'url',
          2 => 'dateInserted',
          3 => 'insertUserID',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'PreviewAssets' => 
      array (
        'description' => 'A collections of variables to generate a preview.',
        'properties' => 
        array (
          'preset' => 
          array (
            'type' => 'string',
          ),
          'globalPrimary' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
          'globalBg' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
          'globalFg' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
          'titleBarBg' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
          'titleBarFg' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
          'backgroundImage' => 
          array (
            'type' => 'string',
            'nullable' => true,
          ),
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'RangeExpression' => 
      array (
        'description' => 'Specify a range or CSV of values.',
        'type' => 'string',
        'format' => 'range-expression',
        'externalDocs' => 
        array (
          'url' => 'https://success.vanillaforums.com/kb/articles/308-range-expressions',
        ),
        'x-addon' => 'dashboard',
      ),
      'ReactedRecord' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'name' => 
          array (
            'description' => 'The name of the record',
            'type' => 'string',
          ),
          'body' => 
          array (
            'description' => 'The body of the record',
            'type' => 'string',
          ),
          'insertUserID' => 
          array (
            'description' => 'The author of the record',
            'type' => 'integer',
          ),
          'dateUpdated' => 
          array (
            'format' => 'date-time',
            'type' => 'string',
          ),
          'recordID' => 
          array (
            'type' => 'integer',
          ),
          'recordType' => 
          array (
            'type' => 'string',
            'enum' => 
            array (
              0 => 'Comment',
              1 => 'Discussion',
            ),
          ),
          'discussionID' => 
          array (
            'type' => 'integer',
          ),
          'commentID' => 
          array (
            'type' => 'integer',
          ),
          'url' => 
          array (
            'type' => 'string',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'updateUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'reactions' => 
          array (
            'type' => 'array',
            'items' => 
            array (
              '$ref' => '#/components/schemas/ReactionFragment',
            ),
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'body',
          2 => 'insertUserID',
          3 => 'recordID',
          4 => 'recordType',
          5 => 'discussionID',
          6 => 'url',
        ),
        'x-addon' => 'reactions',
      ),
      'ReactionFragment' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'tagID' => 
          array (
            'type' => 'integer',
          ),
          'urlcode' => 
          array (
            'type' => 'string',
          ),
          'name' => 
          array (
            'type' => 'string',
          ),
          'class' => 
          array (
            'type' => 'string',
          ),
          'count' => 
          array (
            'type' => 'integer',
          ),
        ),
        'x-addon' => 'reactions',
      ),
      'ReactionType' => 
      array (
        'properties' => 
        array (
          'active' => 
          array (
            'description' => 'Is this type available for use?',
            'type' => 'boolean',
          ),
          'attributes' => 
          array (
            'description' => 'Metadata.',
            'nullable' => true,
            'type' => 'object',
          ),
          'class' => 
          array (
            'description' => 'The classification of the type. Directly maps to permissions.',
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'custom' => 
          array (
            'description' => 'Is this a non-standard type?',
            'type' => 'boolean',
          ),
          'description' => 
          array (
            'description' => 'A user-friendly description.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'hidden' => 
          array (
            'description' => 'Should this type be hidden from the UI?',
            'type' => 'boolean',
          ),
          'name' => 
          array (
            'description' => 'A user-friendly name.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'points' => 
          array (
            'description' => 'Reputation points to be applied along with this reaction.',
            'type' => 'integer',
          ),
          'sort' => 
          array (
            'description' => 'Display order when listing types.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'tagID' => 
          array (
            'description' => 'The numeric ID of the tag associated with the type.',
            'type' => 'integer',
          ),
          'urlCode' => 
          array (
            'description' => 'A URL-safe identifier.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'reactionValue' => 
          array (
            'description' => 'The reaction\'s value.',
            'type' => 'integer',
          ),
        ),
        'required' => 
        array (
          0 => 'urlCode',
          1 => 'name',
          2 => 'description',
          3 => 'points',
          4 => 'class',
          5 => 'tagID',
          6 => 'attributes',
          7 => 'sort',
          8 => 'active',
          9 => 'custom',
          10 => 'hidden',
        ),
        'type' => 'object',
        'x-addon' => 'reactions',
      ),
      'Resource' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'recordType' => 
          array (
            'type' => 'string',
            'description' => 'The record the resource represents. Also the unique ID of the resource.',
          ),
          'crawl' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'url' => 
              array (
                'type' => 'string',
                'description' => 'The URL template used to crawl the resource.',
                'format' => 'uri',
              ),
              'min' => 
              array (
                'description' => 'The minimum parameter value for crawling the resource.',
                'oneOf' => 
                array (
                  0 => 
                  array (
                    'type' => 'number',
                    'format' => 'int32',
                  ),
                  1 => 
                  array (
                    'type' => 'string',
                    'format' => 'date-time',
                  ),
                ),
              ),
              'max' => 
              array (
                'description' => 'The maximum parameter value for crawling the resource.',
                'oneOf' => 
                array (
                  0 => 
                  array (
                    'type' => 'number',
                    'format' => 'int32',
                  ),
                  1 => 
                  array (
                    'type' => 'string',
                    'format' => 'date-time',
                  ),
                ),
              ),
              'count' => 
              array (
                'type' => 'number',
                'format' => 'int32',
                'description' => 'The approximate number of rows the resource has.',
              ),
              'parameter' => 
              array (
                'type' => 'string',
                'description' => 'The name of the parameter you need to pass to the crawl URL.',
              ),
            ),
            'required' => 
            array (
              0 => 'url',
              1 => 'min',
              2 => 'max',
              3 => 'count',
              4 => 'parameter',
            ),
          ),
        ),
        'required' => 
        array (
          0 => 'recordType',
        ),
        'example' => 
        array (
          'recordType' => 'discussion',
          'crawl' => 
          array (
            'url' => 'https://example.com/api/v2/discussions?expand=crawl?order=-discussionID',
            'min' => 1.0,
            'max' => 500000.0,
            'count' => 499560.0,
            'parameter' => 'discussionID',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'ResourceFragment' => 
      array (
        'description' => 'Meta information about a resource.',
        'type' => 'object',
        'properties' => 
        array (
          'recordType' => 
          array (
            'type' => 'string',
            'description' => 'The record the resource represents. Also the unique ID of the resource.',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'The URL of the resource meta information.',
            'format' => 'uri',
          ),
          'crawlable' => 
          array (
            'type' => 'boolean',
            'description' => 'Whether or not the resource has crawl information.',
          ),
        ),
        'required' => 
        array (
          0 => 'recordType',
          1 => 'url',
          2 => 'crawlable',
        ),
        'x-addon' => 'dashboard',
      ),
      'Role' => 
      array (
        'properties' => 
        array (
          'canSession' => 
          array (
            'description' => 'Can users in this role start a session?',
            'type' => 'boolean',
          ),
          'deletable' => 
          array (
            'description' => 'Is the role deletable?',
            'type' => 'boolean',
          ),
          'description' => 
          array (
            'description' => 'Description of the role.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
          'name' => 
          array (
            'description' => 'Name of the role.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'permissions' => 
          array (
            'items' => 
            array (
              '$ref' => '#/components/schemas/PermissionFragment',
            ),
            'type' => 'array',
          ),
          'personalInfo' => 
          array (
            'description' => 'Is membership in this role personal information?',
            'type' => 'boolean',
          ),
          'roleID' => 
          array (
            'description' => 'ID of the role.',
            'type' => 'integer',
          ),
          'type' => 
          array (
            'description' => 'Default type of this role.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'roleID',
          1 => 'name',
          2 => 'description',
          3 => 'type',
          4 => 'deletable',
          5 => 'canSession',
          6 => 'personalInfo',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'RoleApplication' => 
      array (
        'allOf' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/schemas/RoleRequestItem',
          ),
          1 => 
          array (
            '$ref' => '#/components/schemas/InsertInfo',
          ),
          2 => 
          array (
            '$ref' => '#/components/schemas/UpdateInfo',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleFragment' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'name' => 
          array (
            'description' => 'Name of the role.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'roleID' => 
          array (
            'description' => 'ID of the role.',
            'type' => 'integer',
          ),
        ),
        'required' => 
        array (
          0 => 'roleID',
          1 => 'name',
        ),
        'x-addon' => 'dashboard',
      ),
      'RolePost' => 
      array (
        'properties' => 
        array (
          'canSession' => 
          array (
            'description' => 'Can users in this role start a session?',
            'type' => 'boolean',
          ),
          'deletable' => 
          array (
            'description' => 'Is the role deletable?',
            'type' => 'boolean',
          ),
          'description' => 
          array (
            'description' => 'Description of the role.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
          'name' => 
          array (
            'description' => 'Name of the role.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'permissions' => 
          array (
            'items' => 
            array (
              '$ref' => '#/components/schemas/PermissionFragment',
            ),
            'type' => 'array',
          ),
          'personalInfo' => 
          array (
            'description' => 'Is membership in this role personal information?',
            'type' => 'boolean',
          ),
          'type' => 
          array (
            'description' => 'Default type of this role.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'name',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'RoleRequestExpansions' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'role' => 
          array (
            '$ref' => '#/components/schemas/RoleFragment',
          ),
          'user' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'statusUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestFragment' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'status' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestStatus',
          ),
          'dateInserted' => 
          array (
            'type' => 'string',
            'format' => 'date-time',
          ),
        ),
        'required' => 
        array (
          0 => 'status',
          1 => 'dateInserted',
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestItem' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'roleRequestID' => 
          array (
            'type' => 'integer',
            'description' => 'The unique ID of the entry.',
          ),
          'type' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestType',
          ),
          'roleID' => 
          array (
            'type' => 'integer',
            'description' => 'The role being applied/invited to.',
          ),
          'userID' => 
          array (
            'type' => 'integer',
            'description' => 'The user applying/being invited.',
          ),
          'status' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestStatus',
          ),
          'dateOfStatus' => 
          array (
            'type' => 'integer',
            'description' => 'The date the status was changed.  Only community managers see this.',
          ),
          'statusUserID' => 
          array (
            'type' => 'integer',
            'description' => 'The user that last set the status. Only community managers see this.',
          ),
          'dateExpires' => 
          array (
            'type' => 'string',
            'format' => 'date-time',
            'nullable' => true,
            'description' => 'The date the application/invitation expires.',
          ),
          'attributes' => 
          array (
            'type' => 'object',
          ),
        ),
        'required' => 
        array (
          0 => 'roleRequestID',
          1 => 'roleID',
          2 => 'userID',
          3 => 'status',
          4 => 'dateExpires',
          5 => 'attributes',
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestMeta' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'roleID' => 
          array (
            'type' => 'integer',
          ),
          'type' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestType',
          ),
          'name' => 
          array (
            'type' => 'string',
            'description' => 'The title displayed to to the user.',
          ),
          'body' => 
          array (
            'type' => 'string',
            'description' => 'The default text to display to the user.',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
          'attributesSchema' => 
          array (
            '$ref' => '#/components/schemas/BasicSchema',
          ),
          'attributes' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestMetaAttributes',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'The URL users visit to apply/accept the role request.',
            'format' => 'uri',
          ),
          'hasRole' => 
          array (
            'type' => 'boolean',
            'description' => 'Whether or not the user has this role.',
          ),
          'roleRequest' => 
          array (
            '$ref' => '#/components/schemas/RoleRequestFragment',
          ),
        ),
        'required' => 
        array (
          0 => 'roleID',
          1 => 'type',
          2 => 'name',
          3 => 'body',
          4 => 'format',
          5 => 'attributesSchema',
          6 => 'attributes',
          7 => 'url',
        ),
        'example' => 
        array (
          'roleID' => 1,
          'type' => 'application',
          'name' => 'Community Manager Application',
          'body' => 'Fill out the fields below to apply to become a community manager.',
          'format' => 'markdown',
          'attributesSchema' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'body' => 
              array (
                'type' => 'string',
                'description' => 'Tell us why you want to be a community manager.',
                'x-label' => 'Details',
              ),
              'isExperienced' => 
              array (
                'type' => 'boolean',
                'x-label' => 'I have previous experience as a community manager.',
              ),
            ),
          ),
          'attributes' => 
          array (
          ),
          'url' => 'https://example.com/requests/role-applications?role=1',
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestMetaAttributes' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'notification' => 
          array (
            'description' => 'Customize the notification sent when a role is approved.',
            'type' => 'object',
            'properties' => 
            array (
              'approved' => 
              array (
                '$ref' => '#/components/schemas/RoleRequestMetaNotification',
              ),
              'denied' => 
              array (
                '$ref' => '#/components/schemas/RoleRequestMetaNotification',
              ),
              'communityManager' => 
              array (
                '$ref' => '#/components/schemas/RoleRequestMetaNotification',
              ),
            ),
          ),
          'link' => 
          array (
            'description' => 'Customize the request link display.',
            'type' => 'object',
            'properties' => 
            array (
              'name' => 
              array (
                'description' => 'Override the link text.',
                'type' => 'string',
              ),
              'description' => 
              array (
                'description' => 'Give a link description.',
                'type' => 'string',
              ),
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestMetaNotification' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'name' => 
          array (
            'description' => 'Override the notification subject.',
            'type' => 'string',
            'minLength' => 1,
          ),
          'body' => 
          array (
            'description' => 'Override the notification body.',
            'type' => 'string',
          ),
          'format' => 
          array (
            '$ref' => '#/components/schemas/Format',
          ),
          'url' => 
          array (
            'description' => 'Overide the notification URL.',
            'type' => 'string',
            'format' => 'uri',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestStatus' => 
      array (
        'description' => 'The current status of the request.',
        'type' => 'string',
        'enum' => 
        array (
          0 => 'pending',
          1 => 'approved',
          2 => 'denied',
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestType' => 
      array (
        'description' => 'The type of role request.',
        'type' => 'string',
        'enum' => 
        array (
          0 => 'application',
          1 => 'invitation',
        ),
        'x-addon' => 'dashboard',
      ),
      'SearchResult' => 
      array (
        'properties' => 
        array (
          'url' => 
          array (
            'description' => 'The URL of the record.',
            'type' => 'string',
          ),
          'body' => 
          array (
            'description' => 'The content of the record.',
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
          'excerpt' => 
          array (
            'description' => 'An excerpt of the record.',
            'type' => 'string',
          ),
          'categoryID' => 
          array (
            'description' => 'The category containing the record.',
            'type' => 'integer',
          ),
          'commentID' => 
          array (
            'description' => 'The id of the comment.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the record was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'dateUpdated' => 
          array (
            'description' => 'When the user was updated.',
            'format' => 'date-time',
            'nullable' => true,
            'type' => 'string',
          ),
          'discussionID' => 
          array (
            'description' => 'The id of the discussion.',
            'type' => 'integer',
          ),
          'groupID' => 
          array (
            'description' => 'The id of the group or the id of the group containing the record.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'insertUserID' => 
          array (
            'description' => 'The user that created the record.',
            'type' => 'integer',
          ),
          'insertUser' => 
          array (
            '$ref' => '#/components/schemas/UserFragment',
          ),
          'name' => 
          array (
            'description' => 'The title of the record. A comment would be "RE: {DiscussionTitle}".',
            'minLength' => 1,
            'type' => 'string',
          ),
          'recordID' => 
          array (
            'description' => 'The identifier of the record.',
            'type' => 'integer',
          ),
          'recordType' => 
          array (
            'description' => 'The main type of record.',
            'enum' => 
            array (
              0 => 'discussion',
              1 => 'comment',
              2 => 'group',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
          'score' => 
          array (
            'description' => 'Score of the record.',
            'type' => 'integer',
          ),
          'type' => 
          array (
            'description' => 'Sub-type of the discussion.',
            'enum' => 
            array (
              0 => 'discussion',
              1 => 'comment',
              2 => 'question',
              3 => 'group',
            ),
            'minLength' => 1,
            'type' => 'string',
          ),
          'updateUserID' => 
          array (
            'description' => 'The user that updated the record.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'image' => 
          array (
            'description' => 'The url of the first image in the record.',
            'type' => 'string',
          ),
          'breadcrumbs' => 
          array (
            'description' => 'List of breadcrumbs objects. Only returned if: expand[] = breadcrumbs.',
            'items' => 
            array (
              'properties' => 
              array (
                'name' => 
                array (
                  'description' => 'Breadcrumb element name.',
                  'minLength' => 1,
                  'type' => 'string',
                  'example' => 'User Feedback',
                ),
                'url' => 
                array (
                  'description' => 'Breadcrumb element url.',
                  'minLength' => 1,
                  'type' => 'string',
                  'example' => 'https://vanilla.com/forum/user-feedback/',
                ),
              ),
              'type' => 'object',
            ),
            'type' => 'array',
          ),
        ),
        'required' => 
        array (
          0 => 'recordID',
          1 => 'recordType',
          2 => 'type',
          3 => 'name',
          4 => 'body',
          5 => 'score',
          6 => 'insertUserID',
          7 => 'dateInserted',
          8 => 'updateUserID',
          9 => 'dateUpdated',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'Tag' => 
      array (
        'description' => 'A full tag.',
        'allOf' => 
        array (
          0 => 
          array (
            '$ref' => '#/components/schemas/TagFragment',
          ),
          1 => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'parentTagID' => 
              array (
                'type' => 'integer',
                'description' => 'The parent ID of the tag.',
              ),
              'type' => 
              array (
                'type' => 'string',
                'description' => 'The tag type.',
              ),
            ),
          ),
          2 => 
          array (
            '$ref' => '#/components/schemas/InsertInfo',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'TagFragment' => 
      array (
        'description' => 'A tag that can be applied to content.',
        'type' => 'object',
        'properties' => 
        array (
          'tagID' => 
          array (
            'type' => 'integer',
            'description' => 'The ID of the tag.',
            'readOnly' => true,
          ),
          'urlcode' => 
          array (
            'type' => 'string',
            'description' => 'The URL slug of the tag.',
          ),
          'name' => 
          array (
            'type' => 'string',
            'description' => 'The full name of the tag.',
          ),
        ),
        'required' => 
        array (
          0 => 'tagID',
          1 => 'urlcode',
          2 => 'name',
        ),
        'x-addon' => 'vanilla',
      ),
      'TagReference' => 
      array (
        'description' => 'A reference to a tag for assignment to other items.',
        'type' => 'object',
        'properties' => 
        array (
          'tagIDs' => 
          array (
            'type' => 'array',
            'items' => 
            array (
              'type' => 'integer',
            ),
            'description' => 'You can specify the name of the tag.',
          ),
          'urlcodes' => 
          array (
            'type' => 'array',
            'items' => 
            array (
              'type' => 'string',
            ),
            'description' => 'You can also specify the url code of the tag.',
          ),
        ),
        'x-addon' => 'vanilla',
      ),
      'Theme' => 
      array (
        'properties' => 
        array (
          'themeID' => 
          array (
            'type' => 'string',
            'description' => 'Unique ID of the theme.',
          ),
          'version' => 
          array (
            'type' => 'string',
            'description' => 'Theme version.',
          ),
          'name' => 
          array (
            'description' => 'Theme name.',
            'type' => 'string',
          ),
          'parentTheme' => 
          array (
            'type' => 'string',
            'description' => 'Parent theme template',
          ),
          'parentVersion' => 
          array (
            'type' => 'string',
            'description' => 'Parent theme template version',
          ),
          'revisionID' => 
          array (
            'type' => 'integer',
            'description' => 'Theme revision ID',
          ),
          'revisionName' => 
          array (
            'type' => 'string',
            'description' => 'Theme revision name',
          ),
          'assets' => 
          array (
            '$ref' => '#/components/schemas/ThemeAssets',
          ),
          'preview' => 
          array (
            '$ref' => '#/components/schemas/PreviewAssets',
          ),
        ),
        'required' => 
        array (
          0 => 'assets',
          1 => 'type',
          2 => 'themeID',
          3 => 'version',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeAssets' => 
      array (
        'description' => 'Assets to include in a page as part of the theme.',
        'properties' => 
        array (
          'header' => 
          array (
            '$ref' => '#/components/schemas/ThemeHeaderAsset',
          ),
          'footer' => 
          array (
            '$ref' => '#/components/schemas/ThemeFooterAsset',
          ),
          'javascript' => 
          array (
            '$ref' => '#/components/schemas/ThemeJavascriptAsset',
          ),
          'scripts' => 
          array (
            '$ref' => '#/components/schemas/ThemeScriptsAsset',
          ),
          'styles' => 
          array (
            '$ref' => '#/components/schemas/ThemeCssAsset',
          ),
          'variables' => 
          array (
            '$ref' => '#/components/schemas/ThemeVariablesAsset',
          ),
          'logo' => 
          array (
            '$ref' => '#/components/schemas/ThemeLogoAsset',
          ),
          'mobileLogo' => 
          array (
            '$ref' => '#/components/schemas/ThemeMobileLogoAsset',
          ),
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeCssAsset' => 
      array (
        'description' => 'CSS for the theme.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'string',
            'description' => 'Contents of the asset. May require an expand parameter to retreive.',
          ),
        ),
        'required' => 
        array (
          0 => 'data',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeFooterAsset' => 
      array (
        'description' => 'Custom footer HTML.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'string',
            'description' => 'Contents of the asset. May require an expand parameter to retreive.',
          ),
        ),
        'required' => 
        array (
          0 => 'url',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeHeaderAsset' => 
      array (
        'description' => 'Custom header HTML.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'string',
            'description' => 'Contents of the asset. May require an expand parameter to retreive.',
          ),
        ),
        'required' => 
        array (
          0 => 'url',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeJavascriptAsset' => 
      array (
        'description' => 'Javascript for the theme.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'string',
            'description' => 'Contents of the asset. May require an expand parameter to retreive.',
          ),
        ),
        'required' => 
        array (
          0 => 'url',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeLogoAsset' => 
      array (
        'description' => 'Site logo to be displayed in the theme.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeMobileLogoAsset' => 
      array (
        'description' => 'Site logo to be displayed in the theme when viewed on a mobile device.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeScript' => 
      array (
        'properties' => 
        array (
          'url' => 
          array (
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'url',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeScriptsAsset' => 
      array (
        'description' => 'External script files to be included.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'array',
            'items' => 
            array (
              '$ref' => '#/components/schemas/ThemeScript',
            ),
          ),
        ),
        'required' => 
        array (
          0 => 'url',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'ThemeVariablesAsset' => 
      array (
        'description' => 'A collection of variables intended to be used by theme.',
        'properties' => 
        array (
          'type' => 
          array (
            'description' => 'The type of the asset.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'type' => 'string',
            'description' => 'Absolute URL of the asset.',
          ),
          'content-type' => 
          array (
            'description' => 'The content-type of the asset.',
            'type' => 'string',
            'example' => 'application/json',
          ),
          'data' => 
          array (
            'type' => 'object',
            'description' => 'JSON contents of the asset.',
          ),
        ),
        'required' => 
        array (
          0 => 'url',
          1 => 'type',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'Themes' => 
      array (
        'properties' => 
        array (
          'assets' => 
          array (
            '$ref' => '#/components/schemas/ThemeAssets',
          ),
          'preview' => 
          array (
            '$ref' => '#/components/schemas/PreviewAssets',
          ),
          'type' => 
          array (
            'type' => 'string',
            'enum' => 
            array (
              0 => 'themeFile',
            ),
          ),
          'themeID' => 
          array (
            'type' => 'integer',
          ),
          'version' => 
          array (
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'assets',
          1 => 'type',
          2 => 'themeID',
          3 => 'version',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'Token' => 
      array (
        'properties' => 
        array (
          'accessToken' => 
          array (
            'description' => 'A signed version of the token.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'accessTokenID' => 
          array (
            'description' => 'The unique numeric ID.',
            'type' => 'integer',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the token was generated.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'name' => 
          array (
            'description' => 'A user-specified label.',
            'minLength' => 1,
            'nullable' => true,
            'type' => 'string',
          ),
        ),
        'required' => 
        array (
          0 => 'accessTokenID',
          1 => 'name',
          2 => 'accessToken',
          3 => 'dateInserted',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'UpdateInfo' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'dateUpdated' => 
          array (
            'type' => 'string',
            'format' => 'date-time',
            'description' => 'The date the record was updateed.',
            'nullable' => true,
            'readOnly' => true,
          ),
          'updateUserID' => 
          array (
            'type' => 'integer',
            'description' => 'The user that updateed the record.',
            'nullable' => true,
            'readOnly' => true,
          ),
          'updateIPAddress' => 
          array (
            'type' => 'string',
            'format' => 'ipv4',
            'description' => 'The IP address the record was updateed from.',
            'nullable' => true,
            'readOnly' => true,
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'UploadedFile' => 
      array (
        'type' => 'string',
        'format' => 'binary',
        'x-addon' => 'dashboard',
      ),
      'User' => 
      array (
        'properties' => 
        array (
          'userID' => 
          array (
            'description' => 'ID of the user.',
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'Name of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'photoUrl' => 
          array (
            'description' => 'URL to the user photo.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
          'email' => 
          array (
            'description' => 'Email address of the user.',
            'minLength' => 0,
            'type' => 'string',
          ),
          'roles' => 
          array (
            'items' => 
            array (
              '$ref' => '#/components/schemas/RoleFragment',
            ),
            'type' => 'array',
          ),
          'dateInserted' => 
          array (
            'description' => 'When the user was created.',
            'format' => 'date-time',
            'type' => 'string',
          ),
          'dateLastActive' => 
          array (
            'description' => 'Time the user was last active.',
            'format' => 'date-time',
            'nullable' => true,
            'type' => 'string',
          ),
          'dateUpdated' => 
          array (
            'description' => 'When the user was last updated.',
            'format' => 'date-time',
            'nullable' => true,
            'type' => 'string',
          ),
          'points' => 
          array (
            'description' => 'The total number of points the user has accumulated.',
            'type' => 'integer',
            'default' => 0,
          ),
          'emailConfirmed' => 
          array (
            'description' => 'Has the email address for the user been confirmed?',
            'type' => 'boolean',
          ),
          'hidden' => 
          array (
            'description' => 'Is this user hiding their online status?',
            'type' => 'boolean',
          ),
          'bypassSpam' => 
          array (
            'description' => 'Should submissions from this user bypass SPAM checks?',
            'type' => 'boolean',
          ),
          'banned' => 
          array (
            'description' => 'Is the user banned?',
            'type' => 'integer',
          ),
          'rank' => 
          array (
            'x-addon' => 'ranks',
            'properties' => 
            array (
              'name' => 
              array (
                'description' => 'Name of the rank.',
                'minLength' => 1,
                'type' => 'string',
              ),
              'rankID' => 
              array (
                'description' => 'Rank ID.',
                'type' => 'integer',
              ),
              'userTitle' => 
              array (
                'description' => 'Label that will display beside the user.',
                'minLength' => 1,
                'type' => 'string',
              ),
            ),
            'required' => 
            array (
              0 => 'rankID',
              1 => 'name',
              2 => 'userTitle',
            ),
            'type' => 'object',
          ),
          'rankID' => 
          array (
            'x-addon' => 'ranks',
            'description' => 'ID of the user rank.',
            'nullable' => true,
            'type' => 'integer',
          ),
          'showEmail' => 
          array (
            'description' => 'Is the email address visible to other users?',
            'type' => 'boolean',
          ),
        ),
        'required' => 
        array (
          0 => 'userID',
          1 => 'name',
          2 => 'email',
          3 => 'photoUrl',
          4 => 'points',
          5 => 'emailConfirmed',
          6 => 'showEmail',
          7 => 'bypassSpam',
          8 => 'banned',
          9 => 'dateInserted',
          10 => 'dateLastActive',
          11 => 'dateUpdated',
        ),
        'type' => 'object',
        'x-addon' => 'dashboard',
      ),
      'UserBadges' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'badgeID' => 
          array (
            'description' => 'ID of the badge.',
            'type' => 'integer',
          ),
          'name' => 
          array (
            'description' => 'Name of the badge.',
            'type' => 'string',
          ),
          'photoUrl' => 
          array (
            'description' => 'Badge photo.',
            'type' => 'string',
          ),
          'url' => 
          array (
            'description' => 'URL to the badge photo.',
            'type' => 'string',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'UserFragment' => 
      array (
        'oneOf' => 
        array (
          0 => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'userID' => 
              array (
                'description' => 'The ID of the user.',
                'type' => 'integer',
              ),
              'name' => 
              array (
                'description' => 'The username of the user.',
                'minLength' => 1,
                'type' => 'string',
              ),
              'url' => 
              array (
                'description' => 'The URL of the user\'s profile.',
                'type' => 'string',
                'format' => 'uri',
              ),
              'photoUrl' => 
              array (
                'description' => 'The URL of the user\'s avatar picture.',
                'type' => 'string',
                'format' => 'uri',
              ),
              'dateLastActive' => 
              array (
                'description' => 'Time the user was last active.',
                'format' => 'date-time',
                'nullable' => true,
                'type' => 'string',
              ),
              'ssoID' => 
              array (
                'description' => 'The unique ID of the user from the source site, if using SSO.',
                'type' => 'string',
              ),
            ),
            'required' => 
            array (
              0 => 'userID',
              1 => 'name',
              2 => 'photoUrl',
              3 => 'dateLastActive',
            ),
          ),
          1 => 
          array (
            'type' => 'object',
            'description' => 'A user fragment when only expanding by ssoID.',
            'properties' => 
            array (
              'ssoID' => 
              array (
                'description' => 'The unique ID of the user from the source site, if using SSO.',
                'type' => 'string',
              ),
            ),
            'required' => 
            array (
              0 => 'ssoID',
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'UserPatch' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'bypassSpam' => 
          array (
            'description' => 'Should submissions from this user bypass SPAM checks?',
            'type' => 'boolean',
          ),
          'email' => 
          array (
            'description' => 'Email address of the user.',
            'minLength' => 0,
            'type' => 'string',
          ),
          'emailConfirmed' => 
          array (
            'description' => 'Has the email address for this user been confirmed?',
            'type' => 'boolean',
          ),
          'name' => 
          array (
            'description' => 'Name of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'password' => 
          array (
            'description' => 'Password of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'photo' => 
          array (
            'description' => 'Raw photo field value from the user record.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
          'roleID' => 
          array (
            'description' => 'Roles to set on the user.',
            'items' => 
            array (
              'type' => 'integer',
            ),
            'type' => 'array',
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'UserPost' => 
      array (
        'type' => 'object',
        'properties' => 
        array (
          'bypassSpam' => 
          array (
            'default' => false,
            'description' => 'Should submissions from this user bypass SPAM checks?',
            'type' => 'boolean',
          ),
          'email' => 
          array (
            'description' => 'Email address of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'emailConfirmed' => 
          array (
            'default' => true,
            'description' => 'Has the email address for this user been confirmed?',
            'type' => 'boolean',
          ),
          'name' => 
          array (
            'description' => 'Name of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'password' => 
          array (
            'description' => 'Password of the user.',
            'minLength' => 1,
            'type' => 'string',
          ),
          'photo' => 
          array (
            'description' => 'Raw photo field value from the user record.',
            'minLength' => 0,
            'nullable' => true,
            'type' => 'string',
          ),
          'roleID' => 
          array (
            'description' => 'Roles to set on the user.',
            'items' => 
            array (
              'type' => 'integer',
            ),
            'type' => 'array',
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'email',
          2 => 'password',
          3 => 'emailConfirmed',
          4 => 'bypassSpam',
        ),
        'x-addon' => 'dashboard',
      ),
      'Widget' => 
      array (
        'properties' => 
        array (
          'widgetID' => 
          array (
            'description' => 'The id of the widget.',
            'type' => 'string',
          ),
          'name' => 
          array (
            'description' => 'The name of the widget.',
            'type' => 'string',
          ),
          'widgetClass' => 
          array (
            'description' => 'The class of the widget.',
            'type' => 'string',
          ),
          'schema' => 
          array (
            'description' => 'The widget form schema.',
            'type' => 'object',
          ),
        ),
        'required' => 
        array (
          0 => 'name',
          1 => 'schema',
        ),
        'type' => 'object',
        'x-addon' => 'vanilla',
      ),
      'fullPocketSchema' => 
      array (
        'properties' => 
        array (
          'pocketID' => 
          array (
            'type' => 'integer',
          ),
          'widgetID' => 
          array (
            'type' => 'string',
            'description' => 'The widget type.',
          ),
          'name' => 
          array (
            'description' => 'The name of the pocket',
            'type' => 'string',
          ),
          'page' => 
          array (
            'description' => 'Which page to display the Pocket on.',
            'type' => 'string',
          ),
          'body' => 
          array (
            'description' => 'The body of the pocket.',
            'type' => 'string',
          ),
          'enabled' => 
          array (
            'type' => 'integer',
            'description' => 'Pocket enabled/disabled.',
          ),
          'sort' => 
          array (
            'type' => 'integer',
            'description' => 'The pocket sort order.',
          ),
          'repeatType' => 
          array (
            'enum' => 
            array (
              0 => 'once',
              1 => 'after',
              2 => 'before',
              3 => 'every',
              4 => 'index',
            ),
            'type' => 'string',
            'description' => 'Pocket repeat type.',
          ),
          'mobileType' => 
          array (
            'enum' => 
            array (
              0 => 'only',
              1 => 'never',
              2 => 'default',
            ),
            'type' => 'string',
            'description' => 'Whether or not pocket is active on mobile.',
          ),
          'isEmbeddable' => 
          array (
            'type' => 'boolean',
            'description' => 'Whether or not pocket is embeddable.',
          ),
          'isDashboard' => 
          array (
            'type' => 'boolean',
            'description' => 'Whether or not pocket is active in dashboard.',
          ),
          'testMode' => 
          array (
            'type' => 'integer',
            'description' => 'If the pocket is in test mode.',
          ),
          'format' => 
          array (
            'enum' => 
            array (
              0 => 'raw',
              1 => 'widget',
            ),
            'type' => 'string',
          ),
          'location' => 
          array (
            'type' => 'string',
            'description' => 'Location of the pocket on the page.',
          ),
          'isAd' => 
          array (
            'type' => 'boolean',
            'description' => 'Whether or not pocket is of type ad.',
          ),
        ),
        'required' => 
        array (
          0 => 'pocketID',
          1 => 'widgetID',
          2 => 'name',
          3 => 'repeatType',
        ),
        'x-addon' => 'pockets',
      ),
    ),
    'parameters' => 
    array (
      'CacheBusterParameter' => 
      array (
        'name' => 'etag',
        'in' => 'query',
        'description' => 'Whether or not output is cached.
',
        'schema' => 
        array (
          'type' => 'string',
        ),
        'x-addon' => 'dashboard',
      ),
      'DateInserted' => 
      array (
        'name' => 'dateInserted',
        'in' => 'query',
        'description' => 'Filter by insert date. See [date filters](https://docs.vanillaforums.com/help/apiv2/date-filters/).
',
        'schema' => 
        array (
          'format' => 'date-filter',
          'type' => 'string',
        ),
        'x-addon' => 'dashboard',
      ),
      'DateLastComment' => 
      array (
        'name' => 'dateLastComment',
        'in' => 'query',
        'description' => 'Filter by the date of the last comment or the original discussion date if it has no comments. See [date filters](https://docs.vanillaforums.com/help/apiv2/date-filters/).
',
        'schema' => 
        array (
          'format' => 'date-filter',
          'type' => 'string',
        ),
        'x-addon' => 'vanilla',
      ),
      'DateUpdated' => 
      array (
        'name' => 'dateUpdated',
        'in' => 'query',
        'description' => 'Filter by update date. See [date filters](https://docs.vanillaforums.com/help/apiv2/date-filters/).
',
        'schema' => 
        array (
          'format' => 'date-filter',
          'type' => 'string',
        ),
        'x-addon' => 'dashboard',
      ),
      'DiscussionExpand' => 
      array (
        'name' => 'expand',
        'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
        'in' => 'query',
        'schema' => 
        array (
          'type' => 'array',
          'items' => 
          array (
            'type' => 'string',
            'enum' => 
            array (
              0 => 'acceptedAnswers',
              1 => 'all',
              2 => 'category',
              3 => 'insertUser',
              4 => 'lastPost',
              5 => 'lastPost.body',
              6 => 'lastPost.insertUser',
              7 => 'lastUser',
              8 => 'reactions',
              9 => 'tags',
              10 => 'tagsIDs',
            ),
          ),
        ),
        'style' => 'form',
        'x-addon' => 'qna',
      ),
      'ExpandAssetsParam' => 
      array (
        'in' => 'query',
        'name' => 'expand',
        'description' => 'Expand associated records using one or more valid field names. A value of `all` will expand all expandable fields. style
',
        'schema' => 
        array (
          '$ref' => '#/components/schemas/ExpandAssetsValues',
        ),
        'style' => 'form',
        'x-addon' => 'dashboard',
      ),
      'LocaleCodeParameter' => 
      array (
        'name' => 'locale',
        'in' => 'path',
        'description' => 'The locale code requested.',
        'required' => true,
        'schema' => 
        array (
          'type' => 'string',
          'pattern' => '[a-zA-Z0-9-]+',
        ),
        'x-addon' => 'dashboard',
      ),
      'Offset' => 
      array (
        'name' => 'offset',
        'in' => 'query',
        'description' => 'Similar to a page parameter, but specifies a specific numeric record offset starting at zero.
',
        'schema' => 
        array (
          'type' => 'integer',
          'minimum' => 0,
        ),
        'x-addon' => 'dashboard',
      ),
      'Page' => 
      array (
        'name' => 'page',
        'in' => 'query',
        'description' => 'Page number. See [Pagination](https://docs.vanillaforums.com/apiv2/#pagination).
',
        'schema' => 
        array (
          'type' => 'integer',
          'default' => 1,
          'minimum' => 1,
        ),
        'x-addon' => 'dashboard',
      ),
      'RoleRequestExpand' => 
      array (
        'name' => 'expand',
        'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.

Note that only community managers can view and expand the `statusUser`.',
        'in' => 'query',
        'schema' => 
        array (
          'type' => 'array',
          'items' => 
          array (
            'type' => 'string',
            'enum' => 
            array (
              0 => 'all',
              1 => 'user',
              2 => 'role',
              3 => 'statusUser',
            ),
          ),
        ),
        'style' => 'form',
        'x-addon' => 'dashboard',
      ),
      'RoleRequestID' => 
      array (
        'name' => 'id',
        'in' => 'path',
        'description' => 'The ID of the request.',
        'required' => true,
        'schema' => 
        array (
          'type' => 'integer',
        ),
        'x-addon' => 'dashboard',
      ),
      'UserExpand' => 
      array (
        'description' => 'Expand associated records using one or more valid field names. A value of "all" will expand all expandable fields.
',
        'in' => 'query',
        'name' => 'expand',
        'schema' => 
        array (
          'items' => 
          array (
            'enum' => 
            array (
              0 => 'all',
              1 => 'rank',
              2 => 'reactionsReceived',
            ),
            'type' => 'string',
          ),
          'type' => 'array',
        ),
        'style' => 'form',
        'x-addon' => 'reactions',
      ),
    ),
    'responses' => 
    array (
      'NotFound' => 
      array (
        'description' => 'The record does not exist or was not found.',
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              'type' => 'object',
              'properties' => 
              array (
                'message' => 
                array (
                  'type' => 'string',
                  'description' => 'More information about the error.',
                ),
                'status' => 
                array (
                  'type' => 'number',
                  'description' => 'The HTTP status code for the error.',
                  'format' => 'int32',
                ),
              ),
              'required' => 
              array (
                0 => 'message',
              ),
            ),
            'example' => 
            array (
              'status' => 404,
              'message' => 'Page Not Found',
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
      'PermissionError' => 
      array (
        'description' => 'You don\'t have adequate permissions to access this resource.',
        'content' => 
        array (
          'application/json' => 
          array (
            'schema' => 
            array (
              'type' => 'object',
              'properties' => 
              array (
                'message' => 
                array (
                  'type' => 'string',
                  'description' => 'A message that tells you the permissions you need.',
                ),
                'status' => 
                array (
                  'type' => 'number',
                  'description' => 'The HTTP status code for the error.',
                  'format' => 'int32',
                ),
              ),
            ),
            'example' => 
            array (
              'status' => 401,
              'message' => 'Permission Problem',
            ),
          ),
        ),
        'x-addon' => 'dashboard',
      ),
    ),
  ),
  'servers' => 
  array (
    0 => 
    array (
      'url' => 'http://www.vanilla.com/api/v2',
    ),
  ),
  'x-resourceEvents' => 
  array (
    'notification' => 
    array (
      'x-addon' => 'dashboard',
      'name' => 'Notification',
      'type' => 'notification',
    ),
    'user' => 
    array (
      'x-addon' => 'dashboard',
      'name' => 'User',
      'type' => 'user',
    ),
    'comment' => 
    array (
      'x-addon' => 'vanilla',
      'name' => 'Comment',
      'type' => 'comment',
    ),
    'discussion' => 
    array (
      'x-addon' => 'vanilla',
      'name' => 'Discussion',
      'type' => 'discussion',
    ),
    'answer' => 
    array (
      'x-addon' => 'qna',
      'name' => 'Answer',
      'type' => 'answer',
    ),
    'reaction' => 
    array (
      'x-addon' => 'reactions',
      'name' => 'Reaction',
      'type' => 'reaction',
    ),
  ),
  'x-aliases' => 
  array (
    'AssetOut' => 
    array (
      'type' => 
      array (
        'description' => 'The type of the asset.',
        'type' => 'string',
      ),
      'url' => 
      array (
        'type' => 'string',
        'description' => 'Absolute URL of the asset.',
      ),
      'content-type' => 
      array (
        'description' => 'The content-type of the asset.',
        'type' => 'string',
        'example' => 'application/json',
      ),
    ),
    'StringAssetOut' => 
    array (
      'type' => 
      array (
        'description' => 'The type of the asset.',
        'type' => 'string',
      ),
      'url' => 
      array (
        'type' => 'string',
        'description' => 'Absolute URL of the asset.',
      ),
      'content-type' => 
      array (
        'description' => 'The content-type of the asset.',
        'type' => 'string',
        'example' => 'application/json',
      ),
      'data' => 
      array (
        'type' => 'string',
        'description' => 'Contents of the asset. May require an expand parameter to retreive.',
      ),
    ),
    'ThemeSlug' => 
    array (
      'description' => 'Unique theme slug.',
      'in' => 'path',
      'name' => 'themeID',
      'required' => true,
      'schema' => 
      array (
        'type' => 'string',
      ),
    ),
    'AssetNotFound' => 
    array (
      'description' => 'JavaScript could not be found.',
      'content' => 
      array (
        'application/json' => 
        array (
          'schema' => 
          array (
            'type' => 'object',
            'properties' => 
            array (
              'description' => 
              array (
                'description' => 'Verbose description of the error.',
                'nullable' => true,
                'type' => 'string',
              ),
              'message' => 
              array (
                'description' => 'Short description of the error.',
                'type' => 'string',
              ),
              'status' => 
              array (
                'description' => 'Status code of the error response.',
                'type' => 'integer',
              ),
            ),
            'required' => 
            array (
              0 => 'description',
              1 => 'message',
              2 => 'status',
            ),
          ),
        ),
      ),
    ),
  ),
);
