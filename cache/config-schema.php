<?php return array (
  'type' => 'object',
  'properties' => 
  array (
    'garden.description' => 
    array (
      'description' => 'The site description usually appears in search engines. You should try having a description that is 100–150
characters long.
',
      'type' => 'string',
      'default' => '',
      'maxLength' => 350,
      'x-key' => 'Garden.Description',
      'x-read' => 'public',
      'x-write' => 'community.manage',
    ),
    'garden.externalUrlFormat' => 
    array (
      'description' => 'The format used to generate URLs to pages from external sources, typically emails. Set this config setting
your site is embedded or uses a reverse proxy. Place a "%s" in the URL and it will be replaced with the path
being generated.
',
      'type' => 'string',
      'default' => '',
      'example' => 'https://example.com/community/%s',
      'pattern' => '(^$)|(^https?://.+%s)',
      'x-key' => 'Garden.ExternalUrlFormat',
      'x-read' => 'community.manage',
    ),
    'garden.homepageTitle' => 
    array (
      'description' => 'The homepage title is displayed on your home page. Pick a title that you would want to see appear in search
engines.
',
      'type' => 'string',
      'default' => '',
      'example' => 'Welcome To Our Support Community',
      'maxLength' => 100,
      'x-key' => 'Garden.HomepageTitle',
      'x-read' => 'public',
      'x-write' => 'community.manage',
    ),
    'garden.orgName' => 
    array (
      'description' => 'Your organization name is used for SEO microdata and JSON+LD.
',
      'type' => 'string',
      'default' => '',
      'maxLength' => 50,
      'x-key' => 'Garden.OrgName',
      'x-read' => 'public',
      'x-write' => 'community.manage',
    ),
    'garden.privacy.ips' => 
    array (
      'description' => 'Anonymize IP addresses on users so they aren\'t tracked. You can specify "partial" to remove the last octet or full to anonymize the entire IP address.',
      'type' => 'string',
      'default' => '',
      'enum' => 
      array (
        0 => '',
        1 => 'partial',
        2 => 'full',
      ),
      'x-key' => 'Garden.Privacy.IPs',
      'x-read' => 'community.manage',
    ),
    'garden.title' => 
    array (
      'description' => 'The banner title appears on your site\'s banner and in your browser\'s title bar.
',
      'type' => 'string',
      'default' => '',
      'example' => 'Support Community',
      'maxLength' => 50,
      'x-key' => 'Garden.Title',
      'x-read' => 'public',
      'x-write' => 'community.manage',
    ),
    'labs.newQuickLinks' => 
    array (
      'description' => 'Enable the new quick links UI on older themes.
',
      'type' => 'boolean',
      'default' => false,
      'example' => true,
      'x-key' => 'Feature.NewQuickLinks.Enabled',
      'x-read' => 'public',
      'x-write' => 'site.manage',
    ),
    'labs.newSearchPage' => 
    array (
      'description' => 'Enable the new search UI on older themes.
',
      'type' => 'boolean',
      'default' => false,
      'example' => true,
      'x-key' => 'Feature.useNewSearchPage.Enabled',
      'x-read' => 'public',
      'x-write' => 'site.manage',
    ),
    'labs.userCards' => 
    array (
      'description' => 'Enable user cards on older themes.
',
      'type' => 'boolean',
      'default' => false,
      'example' => true,
      'x-key' => 'Feature.UserCards.Enabled',
      'x-read' => 'public',
      'x-write' => 'site.manage',
    ),
  ),
  'x-addon' => 'dashboard',
);
