<?php return array (
  'keystone' => '/addons/themes/keystone',
  'lavendermoon' => '/addons/themes/lavendermoon',
  'lavendersun' => '/addons/themes/lavendersun',
  'theme-boilerplate' => '/addons/themes/theme-boilerplate',
  'theme-foundation' => '/addons/themes/theme-foundation',
  '2011Compatibility' => '/themes/2011Compatibility',
  'bittersweet' => '/themes/bittersweet',
  'default' => '/themes/default',
  'EmbedFriendly' => '/themes/EmbedFriendly',
  'mobile' => '/themes/mobile',
);
